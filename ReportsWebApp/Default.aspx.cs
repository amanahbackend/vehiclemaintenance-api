﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using ReportsWebApp.Models;
using ReportsWebApp.Utils;

namespace ReportsWebApp
{
    public partial class Default : Page
    {
        private string ReportsJsonFilePath => Server.MapPath("App_Data/Reports.json");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
                url = url.Replace("default.aspx", "report.aspx");

                lblServiceUrl.Text = url;

                // SaveJsonFile();
            }
        }

        #region Methods

        private void SaveJsonFile()
        {
            // 1 = JobCardsListReport.rdl
            var jobCardsListReportMetaData = GetJobCardsListReportMetaData(1);

            // 2 = JobCardReport.rdl
            var jobCardReportMetaData = GetJobCardReportMetaData(2);

            // 3 = DailyServiceReport.rdl
            var dailyServiceReportMetaData = GetDailyServiceReportMetaData(3);

            // 4 = EmptyJobCardReport.rdl
            var jobCardEmptyReportMetaData = GetJobCardEmptyReportMetaData(4);

            // 5 and 9 are the same report with 1 column different.
            // 5 = SparePartsUsageReport.rdl
            var sparePartsReportMetaData = GetSparePartsUsageReportMetaData(5, "Reports/SparePartsUsageReport.rdl");

            // 9 = GarageExpensesReport.rdl
            var garageExpensesReportMetaData = GetSparePartsUsageReportMetaData(9, "Reports/GarageExpensesReport.rdl");

            // 6 = MaterialRequisitionReport.rdl
            var materialRequisitionReportMetaData = GetMaterialRequisitionReportMetaData(6);

            // 7 = TechniciansWorkReport.rdl
            var technicianWorkReportMetaData = GetTechniciansWorkReportMetaData(7);

            // 8 = OSPWorkOrderReport.rdl
            var ospWorkOrderReportMetaData = GetOspWorkOrderReportMetaData(8);

            // 10 = CannibalizationReport.rdl
            var cannibalizationReportMetaData = GetCannibalizationReportMetaData(10);

            // 11 = DamageMemoReport.rdl
            var damageMemoReportMetaData = GetDamageMemoReportMetaData(11);

            List<ReportMetaData> lstReporstMetaData = new List<ReportMetaData>
            {
                jobCardsListReportMetaData, jobCardReportMetaData,
                dailyServiceReportMetaData, jobCardEmptyReportMetaData,
                sparePartsReportMetaData, materialRequisitionReportMetaData,
                technicianWorkReportMetaData, ospWorkOrderReportMetaData,
                garageExpensesReportMetaData, cannibalizationReportMetaData,
                damageMemoReportMetaData
            };

            FileUtil.SaveJsonFile(ReportsJsonFilePath, lstReporstMetaData);
        }

        // Id = 1; JobCardsListReport.rdl
        private static ReportMetaData GetJobCardsListReportMetaData(int id)
        {
            ReportMetaData reportMetaData = new ReportMetaData
            {
                Id = id,
                ReportTitle = "Job cards list report",
                RelativePath = "Reports/JobCardsListReport.rdl"
            };

            List<ReportParameterData> lstDataSource1Parameters = new List<ReportParameterData>
            {
                new ReportParameterData
                {
                    ParameterName = "@customerName",
                    QueryStringParameterName = "customerName"
                },
                new ReportParameterData
                {
                    ParameterName = "@startDate",
                    QueryStringParameterName = "startDate"
                },
                new ReportParameterData
                {
                    ParameterName = "@endDate",
                    QueryStringParameterName = "endDate"
                },
                new ReportParameterData
                {
                    ParameterName = "@serviceTypeId",
                    QueryStringParameterName = "serviceTypeId"
                },
                new ReportParameterData
                {
                    ParameterName = "@jobCardStatusId",
                    QueryStringParameterName = "jobCardStatusId"
                },
                new ReportParameterData
                {
                    ParameterName = "@vehiclePlateNo",
                    QueryStringParameterName = "vehiclePlateNo"
                },
                new ReportParameterData
                {
                    ParameterName = "@vehicleFleetNo",
                    QueryStringParameterName = "vehicleFleetNo"
                },
                new ReportParameterData
                {
                    ParameterName = "@jobCardId",
                    QueryStringParameterName = "jobCardId"
                }
            };

            ReportDataSourceData reportDataSource1 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_JobCard_GetListReport",
                DataSetName = "JobCardsDataSet",
                ReportParameters = lstDataSource1Parameters
            };

            reportMetaData.DataSources.Add(reportDataSource1);

            return reportMetaData;
        }

        // Id = 2; JobCardReport.rdl
        private static ReportMetaData GetJobCardReportMetaData(int id)
        {
            ReportMetaData reportMetaData = new ReportMetaData
            {
                Id = id,
                ReportTitle = "Job card report",
                RelativePath = "Reports/JobCardReport.rdl"
            };

            List<ReportParameterData> lstDataSource1Parameters = new List<ReportParameterData>
            {
                new ReportParameterData
                {
                    ParameterName = "@jobCardId",
                    QueryStringParameterName = "jobCardId"
                }
            };

            ReportDataSourceData reportDataSource1 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_JobCard_Get1Report",
                DataSetName = "JobCardDataSet",
                ReportParameters = lstDataSource1Parameters
            };

            reportMetaData.DataSources.Add(reportDataSource1);


            ReportDataSourceData reportDataSource2 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_JobCard_GetSpareParts",
                DataSetName = "SparePartsDataSet",

                ReportParameters = new List<ReportParameterData>
                {
                    new ReportParameterData
                    {
                        ParameterName = "@jobCardId",
                        QueryStringParameterName = "jobCardId"
                    }
                }
            };

            reportMetaData.DataSources.Add(reportDataSource2);

            ReportDataSourceData reportDataSource3 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_JobCards_GetRepairRequests",
                DataSetName = "RepairRequestsDataSet",

                ReportParameters = new List<ReportParameterData>
                {
                    new ReportParameterData
                    {
                        ParameterName = "@jobCardId",
                        QueryStringParameterName = "jobCardId"
                    }
                }
            };

            reportMetaData.DataSources.Add(reportDataSource3);

            return reportMetaData;
        }

        // Id = 3; DailyServiceReport.rdl
        private static ReportMetaData GetDailyServiceReportMetaData(int id)
        {
            ReportMetaData reportMetaData = new ReportMetaData
            {
                Id = id,
                ReportTitle = "Daily Service Report",
                RelativePath = "Reports/DailyServiceReport.rdl"
            };

            List<ReportParameterData> lstDataSource1Parameters = new List<ReportParameterData>
            {
                new ReportParameterData
                {
                    ParameterName = "@vehiclePlateNo",
                    QueryStringParameterName = "vehiclePlateNo"
                },
                new ReportParameterData
                {
                    ParameterName = "@startDate",
                    QueryStringParameterName = "startDate"
                },
                new ReportParameterData
                {
                    ParameterName = "@endDate",
                    QueryStringParameterName = "endDate"
                }
            };

            ReportDataSourceData reportDataSource1 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_JobCard_GetDailyServiceReport",
                DataSetName = "DailyServiceDataSet",
                ReportParameters = lstDataSource1Parameters
            };

            reportMetaData.DataSources.Add(reportDataSource1);

            return reportMetaData;
        }

        // Id = 4; JobCardReport.rdl (Empty)
        private static ReportMetaData GetJobCardEmptyReportMetaData(int id)
        {
            ReportMetaData reportMetaData = new ReportMetaData
            {
                Id = id,
                ReportTitle = "Job card empty report",
                RelativePath = "Reports/JobCardReport.rdl"
            };

            List<ReportParameterData> lstDataSource1Parameters = new List<ReportParameterData>
            {
                new ReportParameterData
                {
                    ParameterName = "@jobCardId",
                    ParameterValue = "0"
                },
                new ReportParameterData
                {
                    ParameterName = "@vehicleId",
                    QueryStringParameterName = "vehicleId"
                }
            };

            ReportDataSourceData reportDataSource1 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_JobCard_Get1Report",
                DataSetName = "JobCardDataSet",
                ReportParameters = lstDataSource1Parameters
            };

            reportMetaData.DataSources.Add(reportDataSource1);


            ReportDataSourceData reportDataSource2 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_JobCard_GetSpareParts",
                DataSetName = "SparePartsDataSet",

                ReportParameters = new List<ReportParameterData>
                {
                    new ReportParameterData
                    {
                        ParameterName = "@jobCardId",
                        ParameterValue = "0"
                    }
                }
            };

            reportMetaData.DataSources.Add(reportDataSource2);

            ReportDataSourceData reportDataSource3 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_JobCards_GetRepairRequests",
                DataSetName = "RepairRequestsDataSet",

                ReportParameters = new List<ReportParameterData>
                {
                    new ReportParameterData
                    {
                        ParameterName = "@jobCardId",
                        ParameterValue = "0"
                    }
                }
            };

            reportMetaData.DataSources.Add(reportDataSource3);

            return reportMetaData;
        }

        // Id = 5; SparePartsUsageReport.rdl
        private static ReportMetaData GetSparePartsUsageReportMetaData(int id, string relativePath)
        {
            ReportMetaData reportMetaData = new ReportMetaData
            {
                Id = id,
                ReportTitle = "Spare parts usage report",
                RelativePath = relativePath
            };

            List<ReportParameterData> lstDataSource1Parameters = new List<ReportParameterData>
            {
                new ReportParameterData
                {
                    ParameterName = "@sparePartId",
                    QueryStringParameterName = "sparePartId"
                },
                new ReportParameterData
                {
                    ParameterName = "@startDate",
                    QueryStringParameterName = "startDate"
                },
                new ReportParameterData
                {
                    ParameterName = "@endDate",
                    QueryStringParameterName = "endDate"
                },
                new ReportParameterData
                {
                    ParameterName = "@vehiclePlateNo",
                    QueryStringParameterName = "vehiclePlateNo"
                },
                new ReportParameterData
                {
                    ParameterName = "@vehicleFleetNo",
                    QueryStringParameterName = "vehicleFleetNo"
                },
                new ReportParameterData
                {
                    ParameterName = "@jobCardId",
                    QueryStringParameterName = "jobCardId"
                },
                new ReportParameterData
                {
                    ParameterName = "@jobCardStatusId",
                    QueryStringParameterName = "jobCardStatusId"
                },
            };

            ReportDataSourceData reportDataSource1 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_RepairRequestSparePart_GetSparePartsUsageReport",
                DataSetName = "SparePartsUsageDataSet",
                ReportParameters = lstDataSource1Parameters
            };

            reportMetaData.DataSources.Add(reportDataSource1);

            return reportMetaData;
        }

        // Id = 6; MaterialRequisitionReport.rdl
        private static ReportMetaData GetMaterialRequisitionReportMetaData(int id)
        {
            ReportMetaData reportMetaData = new ReportMetaData
            {
                Id = id,
                ReportTitle = "Material requisition report",
                RelativePath = "Reports/MaterialRequisitionReport.rdl"
            };

            List<ReportParameterData> lstDataSource1Parameters = new List<ReportParameterData>
            {
                new ReportParameterData
                {
                    ParameterName = "@deliveryOrderId",
                    QueryStringParameterName = "deliveryOrderId"
                }
            };

            ReportDataSourceData reportDataSource1 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_SparePartDeliveryOrder_GetMaterialRequisitionReport",
                DataSetName = "MaterialRequisitionDataSet",
                ReportParameters = lstDataSource1Parameters
            };

            reportMetaData.DataSources.Add(reportDataSource1);

            return reportMetaData;
        }

        // Id = 7; TechniciansWorkReport.rdl
        private static ReportMetaData GetTechniciansWorkReportMetaData(int id)
        {
            ReportMetaData reportMetaData = new ReportMetaData
            {
                Id = id,
                ReportTitle = "Technicians work report",
                RelativePath = "Reports/TechniciansWorkReport.rdl"
            };

            List<ReportParameterData> lstDataSource1Parameters = new List<ReportParameterData>
            {
                new ReportParameterData
                {
                    ParameterName = "@technicianId",
                    QueryStringParameterName = "technicianId"
                },
                new ReportParameterData
                {
                    ParameterName = "@startDate",
                    QueryStringParameterName = "startDate"
                },
                new ReportParameterData
                {
                    ParameterName = "@endDate",
                    QueryStringParameterName = "endDate"
                }
            };

            ReportDataSourceData reportDataSource1 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_RepairRequestSparePart_GetTechnicianWorkReport",
                DataSetName = "TechniciansWorkDataSet",
                ReportParameters = lstDataSource1Parameters
            };

            reportMetaData.DataSources.Add(reportDataSource1);

            return reportMetaData;
        }

        // Id = 8; OSPWorkOrderReport.rdl
        private static ReportMetaData GetOspWorkOrderReportMetaData(int id)
        {
            ReportMetaData reportMetaData = new ReportMetaData
            {
                Id = id,
                ReportTitle = "OSP work order report",
                RelativePath = "Reports/OSPWorkOrderReport.rdl"
            };

            List<ReportParameterData> lstDataSource1Parameters = new List<ReportParameterData>
            {
                new ReportParameterData
                {
                    ParameterName = "@repairRequestId",
                    QueryStringParameterName = "repairRequestId"
                }
            };

            ReportDataSourceData reportDataSource1 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_RepairRequestSparePart_GetOSPWorkOrderReport",
                DataSetName = "OSPWorkOrderDataSet",
                ReportParameters = lstDataSource1Parameters
            };

            reportMetaData.DataSources.Add(reportDataSource1);

            return reportMetaData;
        }

        // Id = 10; CannibalizationReport.rdl
        private static ReportMetaData GetCannibalizationReportMetaData(int id)
        {
            ReportMetaData reportMetaData = new ReportMetaData
            {
                Id = id,
                ReportTitle = "Cannibalization report",
                RelativePath = "Reports/CannibalizationReport.rdl"
            };

            List<ReportParameterData> lstDataSource1Parameters = new List<ReportParameterData>
            {
                new ReportParameterData
                {
                    ParameterName = "@cannId",
                    QueryStringParameterName = "cannId"
                }
            };

            ReportDataSourceData reportDataSource1 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_Cannibalization_GetReport",
                DataSetName = "CannibalizationDataSet",
                ReportParameters = lstDataSource1Parameters
            };

            reportMetaData.DataSources.Add(reportDataSource1);

            ReportDataSourceData reportDataSource2 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_CannibalizationSparePart_GetReport",
                DataSetName = "CannSparePartsDataSet",
                ReportParameters = lstDataSource1Parameters
            };

            reportMetaData.DataSources.Add(reportDataSource2);

            return reportMetaData;
        }

        // Id = 11; DamageMemoReport.rdl
        private static ReportMetaData GetDamageMemoReportMetaData(int id)
        {
            ReportMetaData reportMetaData = new ReportMetaData
            {
                Id = id,
                ReportTitle = "Damage memo report",
                RelativePath = "Reports/DamageMemoReport.rdl"
            };

            List<ReportParameterData> lstDataSource1Parameters = new List<ReportParameterData>
            {
                new ReportParameterData
                {
                    ParameterName = "@damageMemoId",
                    QueryStringParameterName = "damageMemoId"
                }
            };

            ReportDataSourceData reportDataSource1 = new ReportDataSourceData
            {
                CommandText = "dbo.stp_DamageMemo_GetReport",
                DataSetName = "DamageMemoDataSet",
                ReportParameters = lstDataSource1Parameters
            };

            reportMetaData.DataSources.Add(reportDataSource1);

            return reportMetaData;
        }

        #endregion Methods
    }
}