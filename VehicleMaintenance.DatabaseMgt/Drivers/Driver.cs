﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.Drivers
{
    public class Driver : BaseEntity
    {
        [StringLength(20)]
        public string CivilIdNo { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        public string FullName => Convert.ToString($"{FirstName} {LastName}");

        [StringLength(20)]
        public string PhoneNumber { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        public DateTime? BirthDate { get; set; }

        [StringLength(50)]
        public string Ssn { get; set; }

        public DateTime? LicenseIssuedDate { get; set; }

        [StringLength(50)]
        public string LicenseIssuedState { get; set; }

        [StringLength(20)]
        public string LicenseNumber { get; set; }

        [StringLength(10)]
        public string Gender { get; set; }

        [StringLength(20)]
        public string MaritalStatus { get; set; }

        [StringLength(36)]
        public string EmployeeId { get; set; }
    }
}
