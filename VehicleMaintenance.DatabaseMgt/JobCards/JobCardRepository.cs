﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Remotion.Linq.Parsing.Structure.IntermediateModel;
using VehicleMaintenance.DatabaseMgt.Common.Shifts;

namespace VehicleMaintenance.DatabaseMgt.JobCards
{
    public class JobCardRepository : Repository<JobCard>, IJobCardRepository
    {
        public JobCardRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public List<JobCard> SearchJobCards(string customerName,
            DateTime? dateIn, DateTime? dateOut, int? serviceTypeId, int? jobCardStatusId,
            string vehiclePlateNo, string vehicleFleetNo, int? jobCardId)
        {
            GetJobCardSearchParams(ref serviceTypeId, ref jobCardStatusId, ref jobCardId);

            IQueryable<JobCard> iqJobCards = GetQueryable().Include("Vehicle")
                .Include("Driver").Include("JobCardStatus");

            if (!string.IsNullOrWhiteSpace(customerName))
            {
                iqJobCards = iqJobCards.Where(x => x.Vehicle.CustomerName == customerName);
            }

            if (dateIn != null)
            {
                iqJobCards = iqJobCards.Where(x => x.DateIn.Value.Date >= dateIn);
            }

            if (dateOut != null)
            {
                iqJobCards = iqJobCards.Where(x => x.DateOut.Value.Date <= dateOut);
            }

            //if (serviceTypeId  != null)
            //{
            //    iqJobCards = iqJobCards.Where(x => x.ServiceTypeId == serviceTypeId);
            //}

            if (jobCardStatusId != null)
            {
                iqJobCards = iqJobCards.Where(x => x.JobCardStatusId == jobCardStatusId);
            }

            if (!string.IsNullOrWhiteSpace(vehiclePlateNo))
            {
                iqJobCards = iqJobCards.Where(x => x.Vehicle.PlateNumber == vehiclePlateNo);
            }

            if (!string.IsNullOrWhiteSpace(vehicleFleetNo))
            {
                iqJobCards = iqJobCards.Where(x => x.Vehicle.FleetNumber == vehicleFleetNo);
            }

            if (jobCardId != null)
            {
                iqJobCards = iqJobCards.Where(x => x.Id == jobCardId);
            }

            List<JobCard> lstJobCards = iqJobCards.OrderByDescending(obj => obj.Id).ToList();

            return lstJobCards;
        }

        public JobCard GetJobCard(int jobCardId)
        {
            JobCard jobCard = GetQueryable().Include("Vehicle").Include("CreatedByUser")
                .Include("Driver").Include("JobCardStatus")
                .FirstOrDefault(obj => obj.Id == jobCardId);

            return jobCard;
        }

        private void GetJobCardSearchParams(ref int? serviceTypeId, ref int? jobCardStatusId, ref int? jobCardId)
        {
            if (serviceTypeId <= 0)
            {
                serviceTypeId = null;
            }

            if (jobCardStatusId <= 0)
            {
                jobCardStatusId = null;
            }

            if (jobCardId <= 0)
            {
                jobCardId = null;
            }
        }

        public List<JobCard> GetJobCards()
        {
            List<JobCard> lstJobCards = _context.JobCard.Include("Vehicle")
                .Include("Driver").Include("JobCardStatus")
                .Where(obj => obj.RowStatusId != -1)
                .ToList();

            return lstJobCards;
        }

        /// <summary>
        /// Get all open job cards for all vehicles.
        /// </summary>
        /// <returns></returns>
        public List<JobCard> GetByOpenStatus()
        {
            IQueryable<JobCard> iqOpenJobCards = _context.JobCard.Include("Vehicle")
                .Include("Driver").Include("JobCardStatus")
                .Where(obj =>
                    obj.JobCardStatusId == (int) EnumJobCardStatus.InProgress
                    && obj.RowStatusId != -1);

            List<JobCard> lstJobCards = iqOpenJobCards.OrderByDescending(obj => obj.Id).ToList();

            return lstJobCards;
        }

        /// <summary>
        /// Get open job cards of a vehicle.
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public List<JobCard> GetByOpenStatus(int vehicleId)
        {
            List<JobCard> lstJobCards = Find(obj => obj.VehicleId == vehicleId &&
                                                    obj.JobCardStatusId == (int)EnumJobCardStatus.InProgress
                                                    && obj.RowStatusId != -1).ToList();

            return lstJobCards;
        }

        public List<JobCard> GetCancelledOrClosedStatuses(int vehicleId)
        {
            List<JobCard> lstJobCards = _context.JobCard.Include("Vehicle")
                .Include("Driver").Include("JobCardStatus")
                .Where(obj => obj.VehicleId == vehicleId
                              && obj.JobCardStatusId != (int)EnumJobCardStatus.InProgress
                              && obj.RowStatusId != -1)
                .ToList();

            return lstJobCards;
        }

        /// <summary>
        /// Get not closed not cancelled job cards (In progress ones) of a vehicle.
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public List<JobCard> GetNonCancelledNonClosedStatuses(int vehicleId)
        {
            List<JobCard> lstJobCards = _context.JobCard.Include("Vehicle")
                .Include("Driver").Include("JobCardStatus")
                .Where(obj => obj.VehicleId == vehicleId
                              && obj.JobCardStatusId != (int)EnumJobCardStatus.Closed
                              && obj.JobCardStatusId != (int)EnumJobCardStatus.Cancelled
                              && obj.RowStatusId != -1)
                .ToList();

            return lstJobCards;
        }

        /// <summary>
        /// Get all jobs cards of a vehicle.
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public List<JobCard> GetByVehicleId(int vehicleId)
        {
            List<JobCard> lstJobCards = Find(obj => obj.VehicleId == vehicleId && obj.RowStatusId != -1).ToList();

            return lstJobCards;
        }

        public bool CanAddJobCard(int vehicleId)
        {
            bool isOpenJobCardsOnVehicle = Find(obj => obj.VehicleId == vehicleId &&
                                                    obj.JobCardStatusId == (int)EnumJobCardStatus.InProgress
                                                    && obj.RowStatusId != -1).Any();

            return !isOpenJobCardsOnVehicle;
        }

        public int GetOpenJobCardsCount()
        {
            int rowsCount = GetQueryable().Count(obj =>
                obj.RowStatusId != (int)EnumRowStatus.Deleted &&
                obj.JobCardStatusId == (int)EnumJobCardStatus.InProgress);

            return rowsCount;
        }

        //public List<JobCard> GetDailyReceiptReport(string vehiclePlateNo, DateTime? startDate, DateTime? endDate)
        //{
        //    string connectionString = null;//_context.Database..ConnectionString;

        //    SqlConnection sqlConnection;

        //    const string commandText = "dbo.stp_JobCard_GetDailyServiceReport";

        //    List<SqlParameter> lstSqlParameters = new List<SqlParameter>();

        //    if (startDate.HasValue)
        //    {
        //        lstSqlParameters.Add(new SqlParameter("@startDate", startDate));
        //    }

        //    if (endDate.HasValue)
        //    {
        //        lstSqlParameters.Add(new SqlParameter("@endDate", endDate));
        //    }

        //    if (!string.IsNullOrWhiteSpace(vehiclePlateNo))
        //    {
        //        lstSqlParameters.Add(new SqlParameter("@vehiclePlateNo", vehiclePlateNo));
        //    }

        //    IDataReader dataReader = DbAccessManager.GetDataReader(connectionString, commandText, lstSqlParameters, out sqlConnection);

        //    List<JobCard> lstJobCards = new List<JobCard>();

        //    try
        //    {
        //        if (dataReader != null)
        //        {
        //            while (dataReader.Read())
        //            {
        //                JobCard jobCard = GetJobCardFromDataReader(dataReader);

        //                lstJobCards.Add(jobCard);
        //            }

        //            if (!dataReader.IsClosed)
        //            {
        //                dataReader.Close();
        //            }
        //        }
        //    }
        //    finally
        //    {
        //        sqlConnection.Close();
        //    }

        //    return lstJobCards;
        //}

        //private static JobCard GetJobCardFromDataReader(IDataRecord dataReader)
        //{
        //    JobCard jobCard = new JobCard();
        //    jobCard.Sr = Convert.ToInt32(dataReader["RowNumber"]);
        //    jobCard.DailyServiceDesc = Convert.ToString(dataReader["DailyServiceDesc"]);
        //    jobCard.Id = Convert.ToInt32(dataReader["JobCardId"]);
        //    jobCard.CreationDate = Convert.ToDateTime(dataReader["CreationDate"]);

        //    jobCard.DateIn = Convert.ToDateTime(dataReader["DateIn"]);
        //    jobCard.DateOut = Convert.ToDateTime(dataReader["DateOut"]);

        //    jobCard.DateInShiftId = dataReader["DateInShiftId"] != DBNull.Value
        //        ? Convert.ToInt32(dataReader["DateInShiftId"])
        //        : (int?)null;

        //    jobCard.DateOutShiftId = dataReader["DateOutShiftId"] != DBNull.Value
        //        ? Convert.ToInt32(dataReader["DateOutShiftId"])
        //        : (int?)null;

        //    jobCard.Description = Convert.ToString(dataReader["JobCardDescription"]);
        //    jobCard.Comment = Convert.ToString(dataReader["Comment"]);

        //    jobCard.Vehicle = new Vehicle();
        //    jobCard.Vehicle.PlateNumber = Convert.ToString(dataReader["VehiclePlateNo"]);
        //    jobCard.Vehicle.FleetNumber = Convert.ToString(dataReader["VehicleFleetNo"]);
        //    jobCard.Vehicle.CustomerName = Convert.ToString(dataReader["CustomerName"]);

        //    //jobCard.ServiceType = new ServiceType();
        //    //jobCard.ServiceType.NameEn = Convert.ToString(dataReader["ServiceType"]);

        //    return jobCard;
        //}

        public new void Add(JobCard jobCard)
        {
            // Set DateInShiftId and DateOutShiftId
            jobCard.DateInShiftId = GetShiftIdOfDateTime(jobCard.DateIn);
            jobCard.DateOutShiftId = GetShiftIdOfDateTime(jobCard.DateOut);
            jobCard.JobCardStatusId = (int)EnumJobCardStatus.InProgress;

            base.Add(jobCard);
        }

        public new void Update(JobCard jobCard)
        {
            // Set DateInShiftId and DateOutShiftId
            jobCard.DateInShiftId = GetShiftIdOfDateTime(jobCard.DateIn);
            jobCard.DateOutShiftId = GetShiftIdOfDateTime(jobCard.DateOut);

            base.Update(jobCard);
        }

        private int? GetShiftIdOfDateTime(DateTime? dateTime)
        {
            if (dateTime == null)
            {
                return null;
            }

            TimeSpan timeSpan = new TimeSpan(dateTime.Value.Hour, dateTime.Value.Minute, 0);

            Shift shift = _context.Shift.AsQueryable().FirstOrDefault(
                obj => timeSpan >= obj.StartTime && timeSpan <= obj.EndTime && obj.RowStatusId != -1);

            int shiftId = shift?.Id ?? 3; // Shift 3 has some exceptions. Start time=23:00:00; EndTime=06:59

            return shiftId;
        }
    }
}
