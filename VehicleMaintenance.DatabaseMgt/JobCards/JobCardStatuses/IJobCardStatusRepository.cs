﻿namespace VehicleMaintenance.DatabaseMgt.JobCards.JobCardStatuses
{
    public interface IJobCardStatusRepository : IRepository<JobCardStatus>
    {
    }
}
