﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VehicleMaintenance.DatabaseMgt.Drivers;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.Vehicles;

namespace VehicleMaintenance.DatabaseMgt.DamageMemos
{
    public class DamageMemo : BaseEntity
    {
        [StringLength(50)]
        public string DamageMemoRef { get; set; }

        public int? TechnicianId { get; set; }
        public virtual Technician Technician { get; set; }

        public int? DriverId { get; set; }
        public virtual Driver Driver { get; set; }

        public int? VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }
        
        [StringLength(50)]
        public string ForemanName { get; set; }

        [StringLength(100)]
        public string SparePart1Name { get; set; }

        [StringLength(100)]
        public string SparePart2Name { get; set; }

        [StringLength(100)]
        public string SparePart3Name { get; set; }
    }
}
