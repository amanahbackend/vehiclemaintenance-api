﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VehicleMaintenance.DatabaseMgt.DamageMemos
{
    public class DamageMemoRepository : Repository<DamageMemo>, IDamageMemoRepository
    {
        public DamageMemoRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public List<DamageMemo> Get()
        {
            var iqDamageMemos = GetQueryable().Include("Technician").Include("Driver").Include("Vehicle").
                Where(obj => obj.RowStatusId != -1).OrderByDescending(obj => obj.Id);

            return iqDamageMemos.ToList();
        }
    }
}
