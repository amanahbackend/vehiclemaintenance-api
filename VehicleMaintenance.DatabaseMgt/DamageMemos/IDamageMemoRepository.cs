﻿using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.DamageMemos
{
    public interface IDamageMemoRepository : IRepository<DamageMemo>
    {
        List<DamageMemo> Get();
    }
}
