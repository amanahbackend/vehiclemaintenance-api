﻿using System.Collections.Generic;
using System.Linq;

namespace VehicleMaintenance.DatabaseMgt.UploadedFiles
{
    public class UploadedFileRepository : Repository<UploadedFile>, IUploadedFileRepository
    {
        public UploadedFileRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public List<UploadedFile> GetUploadedFilesByJobCard(int jobCardId)
        {
            List<UploadedFile> lstUploadedFiles = Find(obj =>
                    obj.JobCardId == jobCardId
                    && obj.RowStatusId != (int)EnumRowStatus.Deleted).ToList();

            return lstUploadedFiles;
        }
    }
}
