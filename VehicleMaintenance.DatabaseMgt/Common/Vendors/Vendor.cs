﻿using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.Common.Vendors
{
   public class Vendor : BaseEntity
    {
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Address { get; set; }
    }
}
