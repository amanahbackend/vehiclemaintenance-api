﻿namespace VehicleMaintenance.DatabaseMgt.Common.Settings
{
    public class SettingRepository : Repository<Setting>, ISettingRepository
    {
        public SettingRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public string WebsiteUrl
        {
            get { return GetSingleOrDefault(obj => obj.Code == "WebsiteUrl").Value; }
        }

        public string WebsitePhysicalPath
        {
            get { return GetSingleOrDefault(obj => obj.Code == "WebsitePhysicalPath").Value; }
        }

        public string ApiServiceUrl
        {
            get { return GetSingleOrDefault(obj => obj.Code == "ApiServiceUrl").Value; }
        }

        public string ApiServiceRootPath
        {
            get { return GetSingleOrDefault(obj => obj.Code == "ApiServiceRootPath").Value; }
        }
    }
}