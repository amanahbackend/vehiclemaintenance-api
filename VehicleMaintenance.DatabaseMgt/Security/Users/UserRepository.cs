﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Identity;
using Utilites.ProcessingResult;
using Utilities;
using VehicleMaintenance.DatabaseMgt.Security.Roles;

namespace VehicleMaintenance.DatabaseMgt.Security.Users
{
    public class UserRepository : IUserRepository
    {
        private UserManager<ApplicationUser> _identityUserManager;
        private RoleManager<ApplicationRole> _identityRoleManager;
        //private IPasswordTokenPinManager _passwordTokenPinManager;
        //private IJunkUserManager _junkUserManager;
        private string _loginProvider = "JWT";
        private string _tokenName = "JwtToken";

        //public UserRepository(VehicleMaintenanceDbContext context) : base(context)
        //{
        //}

        public UserRepository(UserManager<ApplicationUser> identityUserManager
            ,RoleManager<ApplicationRole> identityRoleManager
            /*,IPasswordTokenPinManager passwordTokenPinManager, IJunkUserManager junkUserManager*/)
        {
            //_junkUserManager = junkUserManager;
            //_passwordTokenPinManager = passwordTokenPinManager;
            _identityUserManager = identityUserManager;
            _identityRoleManager = identityRoleManager;
        }

        public async Task<IList<Claim>> GetClaimsAsync(ApplicationUser user)
        {
            return await _identityUserManager.GetClaimsAsync(user);
        }

        public async Task<bool> SetAuthentiacationToken(ApplicationUser user, string token)
        {
            var result =
                await _identityUserManager.SetAuthenticationTokenAsync(user, _loginProvider, _tokenName, token);
            return result.Succeeded;
        }

        public async Task<bool> RemoveAuthenticationToken(ApplicationUser user)
        {
            var result = await _identityUserManager.RemoveAuthenticationTokenAsync(user, _loginProvider, _tokenName);
            return result.Succeeded;
        }

        public async Task<string> GetAuthenticationToken(ApplicationUser user)
        {
            var result = await _identityUserManager.GetAuthenticationTokenAsync(user, _loginProvider, _tokenName);
            return result;
        }

        public async Task<ApplicationUser> AddUserAsync(ApplicationUser user, string password)
        {
            ApplicationUser result = null;
            user.SecurityStamp = Guid.NewGuid().ToString("D");
            //user.Deactivated = false;
            user.PhoneNumberConfirmed = false;
            var identityResult = await _identityUserManager.CreateAsync(user, password);

            if (identityResult.Succeeded)
            {
                if (user.RoleNames != null && user.RoleNames.Count > 0)
                {
                    await AddUserToRolesAsync(user);
                }

                result = user;
            }

            return result;
        }

        public async Task<bool> AddUserToRolesAsync(ApplicationUser user)
        {
            if (user != null && user.RoleNames != null)
            {
                var roles = await _identityUserManager.GetRolesAsync(user);
                var result1 = await _identityUserManager.RemoveFromRolesAsync(user, roles.ToArray());

                var result2 = await _identityUserManager.AddToRolesAsync(user, user.RoleNames);

                return result1.Succeeded && result2.Succeeded;
            }

            return false;
        }

        public async Task AddUsersAsync(List<Tuple<ApplicationUser, string>> users)
        {
            foreach (var usr in users)
            {
                await AddUserAsync(usr.Item1, usr.Item2);
            }
        }

        public async Task<IList<string>> GetRolesAsync(ApplicationUser user)
        {
            IList<string> rolesNames = await _identityUserManager.GetRolesAsync(user);
            return rolesNames;
        }

        public async Task<ApplicationUser> GetBy(string username)
        {
            ApplicationUser user = await _identityUserManager.FindByNameAsync(username);
            if (user != null)
            {
                user.RoleNames = (List<string>)await GetRolesAsync(user);
            }
            return user;
        }

        public ProcessResult<int> Count()
        {
            try
            {
                var count = _identityUserManager.Users.Count();
                return ProcessResultHelper.Succeeded(count);
            }
            catch (Exception ex)
            {
                return ProcessResultHelper.Failed(0, ex);
            }

        }

        public async Task<ApplicationUser> Get(string id)
        {
            ApplicationUser user = await _identityUserManager.FindByIdAsync(id);
            user.RoleNames = (List<string>)await GetRolesAsync(user);
            return user;
        }

        public async Task<bool> IsUserNameExistAsync(string userName)
        {
            var user = await _identityUserManager.FindByNameAsync(userName);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> IsEmailExistAsync(string email)
        {
            var user = await _identityUserManager.FindByEmailAsync(email);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<IList<ApplicationUser>> GetUsersInRole(string roleName)
        {
            var result = await _identityUserManager.GetUsersInRoleAsync(roleName);
            return result;
        }

        public async Task<bool> IsUserInRole(string userName, string roleName)
        {
            var user = await _identityUserManager.FindByNameAsync(userName);
            var result = await _identityUserManager.IsInRoleAsync(user, roleName);
            return result;
        }

        public async Task<List<ApplicationUser>> GetAll()
        {
            var users = _identityUserManager.Users.ToList();
            foreach (var user in users)
            {
                user.RoleNames = (await GetRolesAsync(user)).ToList();
            }
            return users;
        }

        public async Task<bool> DeleteAsync(string username)
        {
            var user = await GetBy(username);
            if (user != null)
            {
                return await DeleteAsync(user);
            }
            return false;
        }

        public async Task<bool> DeleteByIdAsunc(string id)
        {
            var user = await Get(id);
            if (user != null)
            {
                return await DeleteAsync(user);
            }
            return false;
        }

        public async Task<bool> DeleteAsync(ApplicationUser user)
        {
            bool result = false;
            var callBack = await _identityUserManager.DeleteAsync(user);
            if (callBack.Succeeded)
            {
                //var junkUser = _junkUserManager.GetJunkUser(user);
                //if (junkUser.IsSucceeded)
                {
                    //_junkUserManager.Add(junkUser.Data);
                    result = true;
                }
            }
            return result;
        }

        public async Task<ApplicationUser> UpdateUserAsync(ApplicationUser user)
        {
            var dbUser = await _identityUserManager.FindByIdAsync(user.Id);
            CopyEditingProperties(user, dbUser);
            await AddUserToRolesAsync(dbUser);
            IdentityResult identityResult = await _identityUserManager.UpdateAsync(dbUser);

            return identityResult.Succeeded ? dbUser : null;
        }

        public async Task<bool> UpdateUserPassword(string userId, string newPassword)
        {
            var user = await _identityUserManager.FindByIdAsync(userId);

            if (user == null)
            {
                return false;
            }

            user.PasswordHash = _identityUserManager.PasswordHasher.HashPassword(user, newPassword);

            IdentityResult identityResult = await _identityUserManager.UpdateAsync(user);

            return identityResult.Succeeded;
        }

        private void CopyEditingProperties(ApplicationUser source, ApplicationUser destination)
        {
            // User name shouldn't be modified.
            destination.Email = source.Email;
            destination.FirstName = source.FirstName;
            destination.LastName = source.LastName;
            destination.PhoneNumber = source.PhoneNumber;
            
            destination.RoleNames = source.RoleNames;
        }

        public bool IsPhoneExist(string phone)
        {
            var count = 0;// _identityUserManager.Users.Count(u => u.Phone1.Equals(phone) ||
                         //                                     u.Phone2.Equals(phone));
            return count > 0;
        }

        public async Task<List<ApplicationUser>> Search(string searchToken, string[] searchFields)
        {
            var predicate = PredicateBuilder.False<ApplicationUser>();
            foreach (var searchField in searchFields)
            {
                predicate = predicate.Or(PredicateBuilder.CreateEqualSingleExpression<ApplicationUser>(searchField, searchToken));
            }
            var users = _identityUserManager.Users.Where(predicate).ToList();
            foreach (var user in users)
            {
                user.RoleNames = (await GetRolesAsync(user)).ToList();
            }
            return users;
        }

        public async Task<bool> Deactivate(string username)
        {
            var user = await GetBy(username);
            //user.Deactivated = true;
            user = await UpdateUserAsync(user);
            return user != null;
        }

        public async Task<bool> Activate(string username)
        {
            var user = await GetBy(username);
            //user.Deactivated = false;
            user = await UpdateUserAsync(user);
            return user != null;
        }

        public async Task<bool> IsUserDeactivated(string username)
        {
            var user = await GetBy(username);
            //return user.Deactivated.Value;

            return false;
        }

        public async Task<string> GeneratePhoneNumberToken(string username, string phone)
        {
            string token = null;
            var user = await GetBy(username);

            if (user.PhoneNumber.Equals(phone) /*|| user.Phone2.Equals(phone)*/)
            {
                token = await _identityUserManager.GenerateChangePhoneNumberTokenAsync(user, phone);
            }

            return token;
        }

        public async Task<bool> CheckPhoneValidationToken(string username, string phone, string token)
        {
            var user = await GetBy(username);
            return await _identityUserManager.VerifyChangePhoneNumberTokenAsync(user, token, phone);
        }

        public async Task<bool> IsPhoneConfirmed(string username)
        {
            var user = await GetBy(username);
            return await _identityUserManager.IsPhoneNumberConfirmedAsync(user);
        }

        public async Task<bool> ConfirmPhone(string username, string phone, string token)
        {
            var user = await GetBy(username);
            if (await CheckPhoneValidationToken(username, phone, token))
            {
                user.PhoneNumberConfirmed = true;
                return (await UpdateUserAsync(user)) != null;
            }

            return false;
        }

        public async Task<string> GenerateForgetPasswordToken(string username)
        {
            var user = await GetBy(username);
            var forgetPasswordToken = await _identityUserManager.GeneratePasswordResetTokenAsync(user);
            PasswordTokenPin passwordTokenPin = new PasswordTokenPin()
            {
                Token = forgetPasswordToken,
                Pin = $"{StringUtilities.GetBase10(4)}-{StringUtilities.GetBase10(4)}"
            };
            //_passwordTokenPinManager.Add(passwordTokenPin);
            return passwordTokenPin.Pin;
        }

        public async Task<bool> ChangePassword(string userName, string newPassword, string changePasswordPin)
        {
            //var user = await GetBy(userName);
            //PasswordTokenPin passwordTokenPin = _passwordTokenPinManager.GetByPin(changePasswordPin);
            //var identityResult =
            //    await _identityUserManager.ResetPasswordAsync(user, passwordTokenPin.Token, newPassword);
            //if (identityResult.Succeeded)
            //{
            //    _passwordTokenPinManager.Delete(passwordTokenPin);
            //}
            //return identityResult.Succeeded;

            return false;
        }

        public async Task<bool> ResetPassword(string username, string newPassword)
        {
            var user = await GetBy(username);
            // var currentPassword = user.PasswordHash;
            var token = await _identityUserManager.GeneratePasswordResetTokenAsync(user);
            var identityResult = await _identityUserManager.ResetPasswordAsync(user, token, newPassword);
            return identityResult.Succeeded;
        }


    }
}
