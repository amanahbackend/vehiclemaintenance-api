﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace VehicleMaintenance.DatabaseMgt.Security.Users
{
    public class ApplicationUser : IdentityUser
    {
        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        public string FullName => Convert.ToString($"{FirstName} {LastName}");
        
        [StringLength(36)]
        public string CreatedByUserId { get; set; }

        public DateTime? CreationDate { get; set; }

        [StringLength(36)]
        public string ModifiedByUserId { get; set; }

        public DateTime? ModificationDate { get; set; }

        [NotMapped]
        public List<string> RoleNames { get; set; }
    }
}
