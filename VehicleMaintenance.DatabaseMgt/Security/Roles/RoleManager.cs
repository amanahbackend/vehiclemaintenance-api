﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace VehicleMaintenance.DatabaseMgt.Security.Roles
{
    public class RoleManager : IRoleManager
    {
        RoleManager<ApplicationRole> _identityRoleManager;

        public RoleManager(RoleManager<ApplicationRole> identityRoleManager)
        {
            _identityRoleManager = identityRoleManager;
        }

        public async Task<ApplicationRole> GetByName(string roleName)
        {
            var role = await _identityRoleManager.FindByNameAsync(roleName);
            return role;
        }

        public async Task<ApplicationRole> AddRoleAsync(ApplicationRole Role)
        {
            IdentityResult result = await _identityRoleManager.CreateAsync(Role);
            if (result.Succeeded)
            {
                return Role;
            }
            else
            {
                return null;
            }
        }

        public async Task AddRolesAsync(List<ApplicationRole> Roles)
        {
            foreach (var role in Roles)
            {
                await AddRoleAsync(role);
            }
        }

        public async Task<bool> Delete(string roleName)
        {
            var role = await _identityRoleManager.FindByNameAsync(roleName);
            IdentityResult identityResult = await _identityRoleManager.DeleteAsync(role);
            return identityResult.Succeeded;
        }

        public async Task<ApplicationRole> Update(ApplicationRole role)
        {
            var dbRole = await _identityRoleManager.FindByIdAsync(role.Id);
            dbRole.Name = role.Name;
            IdentityResult identityResult = await _identityRoleManager.UpdateAsync(dbRole);
            return identityResult.Succeeded ? dbRole : null;
        }

        public List<ApplicationRole> GetAll()
        {
            var roles = _identityRoleManager.Roles.ToList();
            return roles;
        }
    }
}
