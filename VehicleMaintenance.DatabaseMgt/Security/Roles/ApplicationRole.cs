﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace VehicleMaintenance.DatabaseMgt.Security.Roles
{
    public class ApplicationRole : IdentityRole
    {
        [StringLength(36)]
        public string CreatedBy { get; set; }

        public DateTime? CreationDate { get; set; }

        [StringLength(36)]
        public string ModifiedBy { get; set; }

        public DateTime? ModificationDate { get; set; }
    }
}
