﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace VehicleMaintenance.DatabaseMgt.Util
{
    internal class DbAccessManager
    {
        public static IDataReader GetDataReader(string connectionString, string commandText,
            List<SqlParameter> lstSqlParameters, out SqlConnection sqlConnection)
        {
            sqlConnection = new SqlConnection(connectionString);

            IDataReader sqlDataReader;

            try
            {
                sqlConnection.Open();

                SqlCommand selectCommand = new SqlCommand
                {
                    Connection = sqlConnection,
                    CommandText = commandText,
                    CommandType = CommandType.StoredProcedure
                };

                if (lstSqlParameters != null && lstSqlParameters.Count > 0)
                {
                    selectCommand.Parameters.AddRange(lstSqlParameters.ToArray());
                }

                sqlDataReader = selectCommand.ExecuteReader();
            }
            catch (SqlException)
            {
                sqlDataReader = null;
            }

            return sqlDataReader;
        }
    }
}
