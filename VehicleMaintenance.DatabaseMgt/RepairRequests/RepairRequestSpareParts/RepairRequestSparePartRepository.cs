﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VehicleMaintenance.DatabaseMgt.JobCards;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestSpareParts
{
    public class RepairRequestSparePartRepository : Repository<RepairRequestSparePart>, IRepairRequestSparePartRepository
    {
        public RepairRequestSparePartRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public RepairRequestSparePart GetRepairRequestSparePart(int id)
        {
            RepairRequestSparePart repairRequestSparePart =
                GetQueryable().Include("SparePart").Include("Technician")
                    .FirstOrDefault(obj => obj.Id == id);

            return repairRequestSparePart;
        }

        /// <summary>
        /// Get repair request spare parts by repair request Id.
        /// Get active and pending ones.
        /// </summary>
        /// <param name="repairRequestId"></param>
        /// <param name="rowStatusId">1 = Active; 0 = Pending</param>
        /// <returns></returns>
        public List<RepairRequestSparePart> GetByRepairRequest(int repairRequestId, int rowStatusId)
        {
            List<RepairRequestSparePart> lstRepairRequestSpareParts =
                GetQueryable().Include("SparePart").Include("Technician")
                    .Where(obj => obj.RepairRequestId == repairRequestId &&
                                  obj.RowStatusId == rowStatusId).ToList();

            return lstRepairRequestSpareParts;
        }

        public List<RepairRequestSparePart> GetPendingRepairRequestSpareParts()
        {
            List<RepairRequestSparePart> lstRepairRequestSpareParts = GetQueryable()
                .Include("SparePart").Include("Technician")
                .Where(obj => obj.RowStatusId == 0).ToList();
            
            return lstRepairRequestSpareParts;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sparePartId"></param>
        /// <param name="startDate">Technician Start Date</param>
        /// <param name="endDate">Technician End Date</param>
        /// <param name="vehiclePlateNo"></param>
        /// <param name="vehicleFleetNo">Vehicle Fleet No</param>
        /// <param name="jobCardId"></param>
        /// <param name="jobCardStatusId"></param>
        /// <returns></returns>
        public List<RepairRequestSparePart> Search(int? sparePartId, DateTime? startDate, DateTime? endDate,
            string vehiclePlateNo, string vehicleFleetNo, int? jobCardId, int? jobCardStatusId)
        {
            var iqRepairRequestSpareParts = GetQueryable().Include("SparePart")
                .Include("RepairRequest").Include("Vehicle").Include("JobCard")
                .Where(obj => obj.RowStatusId == (int)EnumRowStatus.Active &&
                              obj.RepairRequest.RowStatusId == (int)EnumRowStatus.Active &&
                              (obj.JobCard.JobCardStatusId == (int)EnumJobCardStatus.InProgress
                               || obj.JobCard.JobCardStatusId == (int)EnumJobCardStatus.Closed));

            if (sparePartId != null && sparePartId.Value > 0)
            {
                iqRepairRequestSpareParts = iqRepairRequestSpareParts.Where(obj => obj.SparePartId == sparePartId);
            }

            if (startDate != null)
            {
                iqRepairRequestSpareParts = iqRepairRequestSpareParts.Where(obj => obj.JobCard.DateIn.Value.Date >= startDate);
            }

            if (endDate != null)
            {
                iqRepairRequestSpareParts = iqRepairRequestSpareParts.Where(obj => obj.JobCard.DateIn.Value.Date <= endDate);
            }

            if (!string.IsNullOrWhiteSpace(vehiclePlateNo))
            {
                int? vehicleId = _context.Vehicle.FirstOrDefault(obj => obj.PlateNumber == vehiclePlateNo)?.Id;

                iqRepairRequestSpareParts = iqRepairRequestSpareParts.Where(obj => obj.VehicleId == vehicleId);
            }
            else if (!string.IsNullOrWhiteSpace(vehicleFleetNo))
            {
                int? vehicleId = _context.Vehicle.FirstOrDefault(obj => obj.FleetNumber == vehicleFleetNo)?.Id;

                iqRepairRequestSpareParts = iqRepairRequestSpareParts.Where(obj => obj.VehicleId == vehicleId);
            }

            if (jobCardId != null && jobCardId.Value > 0)
            {
                iqRepairRequestSpareParts = iqRepairRequestSpareParts.Where(obj => obj.JobCardId == jobCardId);
            }

            if (jobCardStatusId != null && jobCardStatusId.Value > 0)
            {
                iqRepairRequestSpareParts =
                    iqRepairRequestSpareParts.Where(obj => obj.JobCard.JobCardStatusId == jobCardStatusId);
            }

            List<RepairRequestSparePart> lstRepairRequestSpareParts = iqRepairRequestSpareParts.OrderByDescending(obj => obj.Id).ToList();

            // Remove the nested objects.
            foreach (RepairRequestSparePart item in lstRepairRequestSpareParts)
            {
                if (item.RepairRequest != null)
                {
                    item.RepairRequest.JobCard = null;
                }

                if (item.JobCard != null)
                {
                    item.JobCard.RepairRequests = null;
                    item.JobCard.Vehicle = null;
                }
            }

            return lstRepairRequestSpareParts;
        }

        public List<RepairRequestSparePart> GetSparePartsOfJobCard(int jobCardId)
        {
            List<RepairRequestSparePart> lstRepairRequestSpareParts = GetQueryable()
                .Include("RepairRequest").Include("SparePart").Include("Technician")
                .Where(obj => obj.RepairRequest.JobCardId == jobCardId
                              && obj.RowStatusId != (int)EnumRowStatus.Deleted).ToList();

            return lstRepairRequestSpareParts;
        }

        /// <summary>
        /// Get undelivered spareParts of jobCard.
        /// </summary>
        /// <param name="jobCardId">Optional</param>
        /// <param name="endDate">Optional</param>
        /// <param name="startDate">Optional</param>
        /// <returns></returns>
        public List<RepairRequestSparePart> GetUndeliveredSpareParts(int? jobCardId, DateTime? startDate, DateTime? endDate)
        {
            // Get either DeliveredQuantity is null or its value is less than Quantity.
            var iqRepairRequestSpareParts = GetQueryable().Include("RepairRequest")
                .Include("SparePart").Where(obj =>
                    obj.RepairRequest.RepairRequestStatusId == (int)EnumRepairRequestStatus.InProgress &&
                    Convert.ToInt32(obj.DeliveredQuantity) < obj.Quantity &&
                    obj.RowStatusId != (int)EnumRowStatus.Active);

            if (startDate.HasValue)
            {
                iqRepairRequestSpareParts = iqRepairRequestSpareParts.Where(obj => obj.CreationDate >= startDate);
            }

            if (endDate.HasValue)
            {
                iqRepairRequestSpareParts = iqRepairRequestSpareParts.Where(obj => obj.CreationDate <= endDate);
            }

            if (jobCardId.HasValue)
            {
                iqRepairRequestSpareParts =
                    iqRepairRequestSpareParts.Where(obj => obj.RepairRequest.JobCardId == jobCardId);
            }

            List<RepairRequestSparePart> lstRepairRequestSpareParts = iqRepairRequestSpareParts.OrderByDescending(obj => obj.Id).ToList();

            return lstRepairRequestSpareParts;
        }

        public List<RepairRequestSparePart> GetTechniciansWorkReport(int? technicianId, DateTime? startDate, DateTime? endDate)
        {
            var iqRepairRequestSpareParts = GetQueryable().Include("Technician")
                .Where(obj => obj.RowStatusId == (int)EnumRowStatus.Active);

            if (technicianId != null && technicianId.Value > 0)
            {
                iqRepairRequestSpareParts = iqRepairRequestSpareParts.Where(obj => obj.TechnicianId == technicianId);
            }

            if (startDate != null)
            {
                iqRepairRequestSpareParts = iqRepairRequestSpareParts.Where(obj => obj.TechnicianStartDate.Value.Date >= startDate);
            }

            if (endDate != null)
            {
                iqRepairRequestSpareParts = iqRepairRequestSpareParts.Where(obj => obj.TechnicianEndDate.Value.Date <= endDate);
            }

            List<RepairRequestSparePart> result = iqRepairRequestSpareParts.OrderByDescending(obj => obj.Id).ToList();

            return result;
        }
    }
}

