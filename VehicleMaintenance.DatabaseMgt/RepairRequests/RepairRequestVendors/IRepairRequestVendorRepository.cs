﻿using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestVendors
{
    public interface IRepairRequestVendorRepository : IRepository<RepairRequestVendor>
    {
        List<RepairRequestVendor> GetByRepairRequest(int repairRequestId);
    }
}
