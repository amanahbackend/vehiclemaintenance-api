﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestMaterials
{
    public class RepairRequestMaterialRepository : Repository<RepairRequestMaterial>, IRepairRequestMaterialRepository
    {
        public RepairRequestMaterialRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public List<RepairRequestMaterial> GetByRepairRequestId(int repairRequestId)
        {
            List<RepairRequestMaterial> lstRepairRequestMaterials =
                GetQueryable().Include("Technician").Include("SparePart").Include("SparePartStatus")
                .Where(obj => obj.RepairRequestId == repairRequestId && obj.RowStatusId != -1).ToList();

            return lstRepairRequestMaterials;
        }
    }
}
