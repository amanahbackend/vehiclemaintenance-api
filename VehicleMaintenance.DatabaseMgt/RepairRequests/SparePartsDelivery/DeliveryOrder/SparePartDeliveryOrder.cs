﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrderDetails;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrder
{
    public class SparePartDeliveryOrder : BaseEntity
    {
        [StringLength(50)]
        public string RequestedBy { get; set; }

        public List<SparePartDeliveryOrderDetails> SparePartDeliveryOrderDetails { get; set; }
    }
}
