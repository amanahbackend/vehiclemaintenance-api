﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VehicleMaintenance.DatabaseMgt.JobCards;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestStatuses;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestTypes;
using VehicleMaintenance.DatabaseMgt.ServiceTypes;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests
{
    public class RepairRequest : BaseEntity
    {
        public int? JobCardId { get; set; }
        public virtual JobCard JobCard { get; set; }

        public int? RepairRequestTypeId { get; set; }
        public virtual RepairRequestType RepairRequestType { get; set; }

        public int? RepairRequestStatusId { get; set; }
        public virtual RepairRequestStatus RepairRequestStatus { get; set; }
        
        public int? ServiceTypeId { get; set; }
        public virtual ServiceType ServiceType { get; set; }

        public double Duration { get; set; }
        public double Cost { get; set; }
        public bool? External { get; set; }

        [StringLength(200)]
        public string Comment { get; set; }
    }

    public enum EnumRepairRequestStatus
    {
        Pending = 1,
        InProgress = 5,
        Closed = 4
    }
}
