﻿namespace VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestStatuses
{
    public interface IRepairRequestStatusRepository: IRepository<RepairRequestStatus>
    {
    }
}
