﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VehicleMaintenance.DatabaseMgt.RepairRequests
{
    public class RepairRequestRepository : Repository<RepairRequest>, IRepairRequestRepository
    {
        public RepairRequestRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public RepairRequest GetRepairRequest(int repairRequestId)
        {
            RepairRequest repairRequest = GetQueryable().
                Include("RepairRequestType").Include("RepairRequestStatus")
                .Include("ServiceType").Include("CreatedByUser")
                .FirstOrDefault(obj => obj.Id == repairRequestId && obj.RowStatusId != -1);

            return repairRequest;
        }

        public List<RepairRequest> GetByJobCardId(int jobCardId)
        {
            List<RepairRequest> lstRepairRequests = GetQueryable().Include("RepairRequestType")
                .Include("RepairRequestStatus")
                .Include("ServiceType").Include("CreatedByUser")
                .Where(obj =>
                    obj.JobCardId == jobCardId &&
                    obj.RowStatusId != (int)EnumRowStatus.Deleted).ToList();

            return lstRepairRequests;
        }

        public bool AllRepairRequestsAreClosedForJobCard(int jobCardId)
        {
            // Get all non-deleted repaire requests for this job card.
            int repairRequestsCount = GetRepairRequestsCountOfJobCard(jobCardId);

            // Get all non-deleted & completed repaire requests for this job card.
            int completedRepairRequestsCount = GetClosedRepairRequestsCountOfJobCard(jobCardId);

            return repairRequestsCount == completedRepairRequestsCount;
        }

        private int GetRepairRequestsCountOfJobCard(int jobCardId)
        {
            int repairRequestsCount = GetQueryable()
                .Count(obj => obj.JobCardId == jobCardId && obj.RowStatusId != -1);

            return repairRequestsCount;
        }

        private int GetClosedRepairRequestsCountOfJobCard(int jobCardId)
        {
            int closedRepairRequestsCount = GetQueryable()
                .Count(obj => obj.JobCardId == jobCardId
                              && obj.RepairRequestStatusId == (int)EnumRepairRequestStatus.Closed
                              && obj.RowStatusId != -1);

            return closedRepairRequestsCount;
        }
        
        public List<RepairRequest> GetPendingRepairRequests()
        {
            List<RepairRequest> lstRepairRequests = GetQueryable()
                .Include("CreatedByUser").Include("ServiceType")
                .Include("RepairRequestType").Include("RepairRequestStatus").Where(
                    obj =>
                        obj.RepairRequestStatusId == (int)EnumRepairRequestStatus.Pending &&
                        obj.RowStatusId != -1).ToList();

            return lstRepairRequests;
        }

        public int GetOpenRepairRequestsCount()
        {
            int rowsCount = GetQueryable().Count(obj =>
                obj.RowStatusId != (int)EnumRowStatus.Deleted &&
                obj.RepairRequestStatusId == (int)EnumRepairRequestStatus.InProgress);

            return rowsCount;
        }
    }
}
