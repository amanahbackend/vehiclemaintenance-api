﻿using System;
using System.ComponentModel.DataAnnotations;
using VehicleMaintenance.DatabaseMgt.Vehicles.VehicleTypes;

namespace VehicleMaintenance.DatabaseMgt.Vehicles
{
    public class Vehicle : BaseEntity
    {
        [StringLength(20)]
        public string PlateNumber { get; set; }

        [StringLength(20)]
        public string FleetNumber { get; set; }

        [StringLength(30)]
        public string ChassisNumber { get; set; }

        [StringLength(50)]
        public string CustomerName { get; set; }

        public int? Year { get; set; }

        [StringLength(50)]
        public string Make { get; set; }

        [StringLength(50)]
        public string Model { get; set; }

        [StringLength(20)]
        public string Color { get; set; }

        public int? KiloMeters { get; set; }

        [StringLength(20)]
        public string Vin { get; set; }

        public int? VehicleTypeId { get; set; }
        public virtual VehicleType VehicleType { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(500)]
        public string Remarks { get; set; }

        public double? Odometer { get; set; }

        public DateTime? PurchaseDate { get; set; }

        public int? FuelConsumption { get; set; }

        [StringLength(20)]
        public string WorkIdleTime { get; set; }
    }
}
