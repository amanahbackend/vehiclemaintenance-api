﻿namespace VehicleMaintenance.DatabaseMgt.Vehicles.VehicleTypes
{
    public interface IVehicleTypeRepository : IRepository<VehicleType>
    {
    }
}
