﻿using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.Vehicles.VehicleTypes
{
    public class VehicleType : BaseEntity
    {
        [StringLength(50)]
        public string Name { get; set; }
    }
}
