﻿using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.Vehicles
{
    public interface IVehicleRepository : IRepository<Vehicle>
    {
        int SearchVehiclesRowsCount(string plateNo, string fleetNo, string model, string vehicleStatus);

        List<Vehicle> SearchVehicles(string plateNo, string fleetNo, string model, string vehicleStatus, int pageNo, int pageSize);

        Vehicle GetVehicle(int vehicleId);

        bool IsPlateNoExists(string plateNo, int? vehicleIdToSkipCheck);

        bool IsFleetNoExists(string fleetNo, int? vehicleIdToSkipCheck);

        Vehicle GetByVehicleFleetNo(string fleetNo);
    }
}
