﻿using VehicleMaintenance.DatabaseMgt.SpareParts;

namespace VehicleMaintenance.DatabaseMgt.Vehicles.Cannibalizations
{
    public class CannibalizationSparePart : BaseEntity
    {
        public int? CannibalizationId { get; set; }

        public int? SparePartId { get; set; }
        public virtual SparePart SparePart { get; set; }
    }
}
