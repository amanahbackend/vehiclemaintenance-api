﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace VehicleMaintenance.DatabaseMgt.Vehicles.Cannibalizations
{
    public class CannibalizationRepository : Repository<Cannibalization>, ICannibalizationRepository
    {
        public CannibalizationRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public List<Cannibalization> Get()
        {
            var iqCannibalizationList = GetQueryable().Include("VehicleFrom").
                Include("VehicleTo").Include("VehicleTo").Include("TechnicianRemovedBy").
                Include("TechnicianFixedBy").Include("TechnicianRefixedBy").
                Where(obj => obj.RowStatusId != -1).OrderByDescending(obj => obj.Id);

            return iqCannibalizationList.ToList();
        }
    }
}
