﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VehicleMaintenance.DatabaseMgt.Technicians;

namespace VehicleMaintenance.DatabaseMgt.Vehicles.Cannibalizations
{
    public class Cannibalization : BaseEntity
    {
        public DateTime? FixingDate { get; set; }

        public int? VehicleFromId { get; set; }
        public virtual Vehicle VehicleFrom { get; set; }

        [StringLength(50)]
        public string VehicleFromRegnNo { get; set; }

        public int? TechnicianRemovedById { get; set; }
        public virtual Technician TechnicianRemovedBy { get; set; }

        public int? VehicleToId { get; set; }
        public virtual Vehicle VehicleTo { get; set; }

        [StringLength(50)]
        public string VehicleToRegnNo { get; set; }

        public int? TechnicianFixedById { get; set; }
        public virtual Technician TechnicianFixedBy { get; set; }

        [StringLength(250)]
        public string SupervisorComments { get; set; }

        public List<CannibalizationSparePart> CannibalizationSpareParts { get; set; }

        public DateTime? RefixingDate { get; set; }

        public int? TechnicianRefixedById { get; set; }
        public virtual Technician TechnicianRefixedBy { get; set; }
    }
}
