﻿using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.Vehicles.Cannibalizations
{
    public interface ICannibalizationRepository : IRepository<Cannibalization>
    {
        List<Cannibalization> Get();
    }
}
