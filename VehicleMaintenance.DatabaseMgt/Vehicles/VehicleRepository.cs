﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VehicleMaintenance.DatabaseMgt.Util;

namespace VehicleMaintenance.DatabaseMgt.Vehicles
{
    public class VehicleRepository : Repository<Vehicle>, IVehicleRepository
    {
        public VehicleRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public int SearchVehiclesRowsCount(string plateNo, string fleetNo, string model, string vehicleStatus)
        {
            int count;

            try
            {
                IQueryable<Vehicle> querySearchVehiclesRowCount = GetQueryableSearchVehicles(plateNo, fleetNo, model, vehicleStatus);

                count = querySearchVehiclesRowCount.Count();
            }
            catch (Exception)
            {
                count = 0;
            }

            return count;
        }

        public List<Vehicle> SearchVehicles(string plateNo, string fleetNo, string model, string vehicleStatus, int pageNo, int pageSize)
        {
            List<Vehicle> lstVehicles;

            int rowsCountToSkip = Pagination.GetRowsCountToSkip(pageNo, ref pageSize);

            try
            {
                IQueryable<Vehicle> querySearchVehicles = GetQueryableSearchVehicles(plateNo, fleetNo, model, vehicleStatus);

                lstVehicles = querySearchVehicles.OrderByDescending(obj => obj.Id).Skip(rowsCountToSkip).Take(pageSize).ToList();
            }
            catch (Exception)
            {
                lstVehicles = new List<Vehicle>();
            }

            return lstVehicles;
        }

        private IQueryable<Vehicle> GetQueryableSearchVehicles(string plateNo, string fleetNo, string model,
            string vehicleStatus)
        {
            IQueryable<Vehicle> querySearchVehicles = _context.Vehicle
                .Where(obj => obj.RowStatusId != (int)EnumRowStatus.Deleted).Include("VehicleType");

            if (!string.IsNullOrWhiteSpace(plateNo))
            {
                querySearchVehicles = querySearchVehicles.Where(obj => obj.PlateNumber.StartsWith(plateNo));
            }

            if (!string.IsNullOrWhiteSpace(fleetNo))
            {
                querySearchVehicles = querySearchVehicles.Where(obj => obj.FleetNumber.StartsWith(fleetNo));
            }

            if (!string.IsNullOrWhiteSpace(model))
            {
                querySearchVehicles = querySearchVehicles.Where(obj => obj.Model.StartsWith(model));
            }

            if (!string.IsNullOrWhiteSpace(vehicleStatus))
            {
                querySearchVehicles = querySearchVehicles.Where(obj => obj.Status == vehicleStatus);
            }

            return querySearchVehicles;
        }

        /// <summary>
        /// Get vehicle with vehicle type.
        /// </summary>
        /// <param name="vehicleId">Vehicle Id</param>
        /// <returns></returns>
        public Vehicle GetVehicle(int vehicleId)
        {
            Vehicle vehicle = GetQueryable().Include("VehicleType").FirstOrDefault(
                obj => obj.Id == vehicleId && obj.RowStatusId != -1);

            return vehicle;
        }

        public bool IsPlateNoExists(string plateNo, int? vehicleIdToSkipCheck)
        {
            var vehicleExists = _context.Vehicle.AsQueryable().Any(obj =>
                obj.PlateNumber == plateNo && (obj.Id != vehicleIdToSkipCheck ||
                                               vehicleIdToSkipCheck == null));

            return vehicleExists;
        }

        public bool IsFleetNoExists(string fleetNo, int? vehicleIdToSkipCheck)
        {
            bool vehicleExists = _context.Vehicle.AsQueryable().Any(obj =>
                obj.FleetNumber == fleetNo && (obj.Id != vehicleIdToSkipCheck ||
                                               vehicleIdToSkipCheck == null));

            return vehicleExists;
        }

        public Vehicle GetByVehicleFleetNo(string fleetNo)
        {
            return GetSingleOrDefault(obj => obj.FleetNumber == fleetNo);
        }
    }
}
