﻿using VehicleMaintenance.DatabaseMgt.Common.Settings;
using VehicleMaintenance.DatabaseMgt.Common.Shifts;
using VehicleMaintenance.DatabaseMgt.Common.Vendors;
using VehicleMaintenance.DatabaseMgt.DamageMemos;
using VehicleMaintenance.DatabaseMgt.Drivers;
using VehicleMaintenance.DatabaseMgt.JobCards;
using VehicleMaintenance.DatabaseMgt.JobCards.JobCardStatuses;
using VehicleMaintenance.DatabaseMgt.RepairRequests;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestMaterials;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestSpareParts;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestStatuses;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestTypes;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestVendors;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrder;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrderDetails;
using VehicleMaintenance.DatabaseMgt.ServiceTypes;
using VehicleMaintenance.DatabaseMgt.SpareParts;
using VehicleMaintenance.DatabaseMgt.SpareParts.SparePartStatuses;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.UploadedFiles;
using VehicleMaintenance.DatabaseMgt.Vehicles;
using VehicleMaintenance.DatabaseMgt.Vehicles.Cannibalizations;
using VehicleMaintenance.DatabaseMgt.Vehicles.VehicleTypes;

namespace VehicleMaintenance.DatabaseMgt
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly VehicleMaintenanceDbContext _context;

        private IDriverRepository _driversRepos;

        private IVehicleRepository _vehiclesRepos;
        private IVehicleTypeRepository _vehicleTypesRepos;

        private IJobCardRepository _jobCardsRepos;
        private IJobCardStatusRepository _jobCardStatusesRepos;

        private IRepairRequestRepository _repairRequestsRepos;
        private IRepairRequestTypeRepository _repairRequestTypesRepos;
        private IRepairRequestStatusRepository _repairRequestStatusesRepos;
        private IRepairRequestSparePartRepository _repairRequestSparePartsRepos;
        private IRepairRequestMaterialRepository _repairRequestMaterialsRepos;
        private IRepairRequestVendorRepository _repairRequestVendorsRepos;

        private ISparePartDeliveryOrderRepository _sparePartDeliveryOrdersRepos;
        private ISparePartDeliveryOrderDetailsRepository _sparePartDeliveryOrderDetailsRepos;

        private ITechnicianRepository _techniciansRepos;
        private IServiceTypeRepository _serviceTypesRepos;

        private ISparePartRepository _sparePartsRepos;
        private ISparePartStatusRepository _sparePartStatusesRepos;

        private IShiftRepository _shiftsRepos;
        private IVendorRepository _vendorsRepos;
        private ISettingRepository _settingsRepos;
        private IUploadedFileRepository _uploadedFilesRepos;

        private ICannibalizationRepository _cannibalizationsRepos;
        private IDamageMemoRepository _damageMemosRepos;

        public UnitOfWork(VehicleMaintenanceDbContext context)
        {
            _context = context;
        }

        public IDriverRepository Drivers => _driversRepos ?? (_driversRepos = new DriverRepository(_context));

        public IVehicleRepository Vehicles => _vehiclesRepos ?? (_vehiclesRepos = new VehicleRepository(_context));
        public IVehicleTypeRepository VehicleTypes => _vehicleTypesRepos ?? (_vehicleTypesRepos = new VehicleTypeRepository(_context));

        public IJobCardRepository JobCards => _jobCardsRepos ?? (_jobCardsRepos = new JobCardRepository(_context));
        public IJobCardStatusRepository JobCardStatuses => _jobCardStatusesRepos ?? (_jobCardStatusesRepos = new JobCardStatusRepository(_context));

        public IRepairRequestRepository RepairRequests =>
            _repairRequestsRepos ?? (_repairRequestsRepos = new RepairRequestRepository(_context));

        public IRepairRequestTypeRepository RepairRequestTypes =>
            _repairRequestTypesRepos ?? (_repairRequestTypesRepos = new RepairRequestTypeRepository(_context));

        public IRepairRequestStatusRepository RepairRequestStatuses =>
            _repairRequestStatusesRepos ?? (_repairRequestStatusesRepos = new RepairRequestStatusRepository(_context));

        public IRepairRequestSparePartRepository RepairRequestSpareParts =>
            _repairRequestSparePartsRepos ?? (_repairRequestSparePartsRepos = new RepairRequestSparePartRepository(_context));

        public IRepairRequestMaterialRepository RepairRequestMaterials =>
            _repairRequestMaterialsRepos ?? (_repairRequestMaterialsRepos = new RepairRequestMaterialRepository(_context));

        public IRepairRequestVendorRepository RepairRequestVendors =>
            _repairRequestVendorsRepos ?? (_repairRequestVendorsRepos = new RepairRequestVendorRepository(_context));

        public ISparePartDeliveryOrderRepository SparePartDeliveryOrders =>
            _sparePartDeliveryOrdersRepos ?? (_sparePartDeliveryOrdersRepos = new SparePartDeliveryOrderRepository(_context));

        public ISparePartDeliveryOrderDetailsRepository SparePartDeliveryOrderDetails =>
            _sparePartDeliveryOrderDetailsRepos ?? (_sparePartDeliveryOrderDetailsRepos = new SparePartDeliveryOrderDetailsRepository(_context));

        public ITechnicianRepository Technicians => _techniciansRepos ?? (_techniciansRepos = new TechnicianRepository(_context));

        public IServiceTypeRepository ServiceTypes => _serviceTypesRepos ?? (_serviceTypesRepos = new ServiceTypeRepository(_context));

        public ISparePartRepository SpareParts => _sparePartsRepos ?? (_sparePartsRepos = new SparePartRepository(_context));
        public ISparePartStatusRepository SparePartStatuses => _sparePartStatusesRepos ?? (_sparePartStatusesRepos = new SparePartStatusRepository(_context));

        public IShiftRepository Shifts => _shiftsRepos ?? (_shiftsRepos = new ShiftRepository(_context));
        public IVendorRepository Vendors => _vendorsRepos ?? (_vendorsRepos = new VendorRepository(_context));
        public ISettingRepository Settings => _settingsRepos ?? (_settingsRepos = new SettingRepository(_context));
        public IUploadedFileRepository UploadedFiles => _uploadedFilesRepos ?? (_uploadedFilesRepos = new UploadedFileRepository(_context));

        public ICannibalizationRepository Cannibalizations => _cannibalizationsRepos ?? (_cannibalizationsRepos = new CannibalizationRepository(_context));
        public IDamageMemoRepository DamageMemos => _damageMemosRepos ?? (_damageMemosRepos = new DamageMemoRepository(_context));
    }
}
