﻿using System.Collections.Generic;
using System.Linq;

namespace VehicleMaintenance.DatabaseMgt.Technicians
{
    public class TechnicianRepository : Repository<Technician>, ITechnicianRepository
    {
        public TechnicianRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Search technicians by first name or last name or employee id.
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public List<Technician> SearchByWord(string keyword)
        {
            List<Technician> lstTechnicians = Find(
                obj => obj.RowStatusId != -1 &&
                       (obj.FirstName.StartsWith(keyword) ||
                        obj.LastName.StartsWith(keyword) ||
                        obj.EmployeeId.StartsWith(keyword)))
                        .OrderBy(obj => obj.FirstName)
                        .ToList();

            return lstTechnicians;
        }
    }
}
