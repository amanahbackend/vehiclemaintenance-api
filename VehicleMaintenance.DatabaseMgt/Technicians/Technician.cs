﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VehicleMaintenance.DatabaseMgt.Technicians
{
   public class Technician : BaseEntity
    {
        [StringLength(20)]
        public string CivilIdNo { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        public string FullName => Convert.ToString($"{FirstName} {LastName}");

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(20)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string Specialty { get; set; }

        [StringLength(36)]
        public string EmployeeId { get; set; }

        public double? HourRating { get; set; }

        public bool? Available { get; set; }
    }
}
