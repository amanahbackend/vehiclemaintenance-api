﻿using VehicleMaintenance.DatabaseMgt.Common.Settings;
using VehicleMaintenance.DatabaseMgt.Common.Shifts;
using VehicleMaintenance.DatabaseMgt.Common.Vendors;
using VehicleMaintenance.DatabaseMgt.Drivers;
using VehicleMaintenance.DatabaseMgt.JobCards;
using VehicleMaintenance.DatabaseMgt.JobCards.JobCardStatuses;
using VehicleMaintenance.DatabaseMgt.RepairRequests;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestMaterials;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestSpareParts;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestStatuses;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestTypes;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestVendors;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrder;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrderDetails;
using VehicleMaintenance.DatabaseMgt.ServiceTypes;
using VehicleMaintenance.DatabaseMgt.SpareParts;
using VehicleMaintenance.DatabaseMgt.SpareParts.SparePartStatuses;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.UploadedFiles;
using VehicleMaintenance.DatabaseMgt.Vehicles;
using VehicleMaintenance.DatabaseMgt.Vehicles.VehicleTypes;

namespace VehicleMaintenance.DatabaseMgt
{
    public interface IUnitOfWork
    {
        IDriverRepository Drivers { get; }

        IVehicleRepository Vehicles { get; }
        IVehicleTypeRepository VehicleTypes { get; }

        IJobCardRepository JobCards { get; }
        IJobCardStatusRepository JobCardStatuses { get; }

        IRepairRequestRepository RepairRequests { get; }
        IRepairRequestTypeRepository RepairRequestTypes { get; }
        IRepairRequestStatusRepository RepairRequestStatuses { get; }
        IRepairRequestSparePartRepository RepairRequestSpareParts { get; }
        IRepairRequestMaterialRepository RepairRequestMaterials { get; }
        IRepairRequestVendorRepository RepairRequestVendors { get; }

        ISparePartDeliveryOrderRepository SparePartDeliveryOrders { get; }
        ISparePartDeliveryOrderDetailsRepository SparePartDeliveryOrderDetails { get; }

        ITechnicianRepository Technicians { get; }
        IServiceTypeRepository ServiceTypes { get; }
        
        ISparePartRepository SpareParts { get; }
        ISparePartStatusRepository SparePartStatuses { get; }

        IShiftRepository Shifts { get; }
        IVendorRepository Vendors { get; }
        ISettingRepository Settings { get; }
        IUploadedFileRepository UploadedFiles { get; }
    }
}
