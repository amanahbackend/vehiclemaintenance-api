﻿using System.Collections.Generic;

namespace VehicleMaintenance.DatabaseMgt.SpareParts
{
    public interface ISparePartRepository : IRepository<SparePart>
    {
        List<SparePart> Search(string name, string serialNo, string category);
    }
}
