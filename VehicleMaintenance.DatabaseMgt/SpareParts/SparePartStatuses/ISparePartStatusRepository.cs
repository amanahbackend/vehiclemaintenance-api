﻿namespace VehicleMaintenance.DatabaseMgt.SpareParts.SparePartStatuses
{
    public interface ISparePartStatusRepository : IRepository<SparePartStatus>
    {
    }
}
