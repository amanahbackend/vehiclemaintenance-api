﻿using System.Collections.Generic;
using System.Linq;

namespace VehicleMaintenance.DatabaseMgt.SpareParts
{
    public class SparePartRepository : Repository<SparePart>, ISparePartRepository
    {
        public SparePartRepository(VehicleMaintenanceDbContext context) : base(context)
        {
        }

        public List<SparePart> Search(string name, string serialNo, string category)
        {
            var iQueryable = GetQueryable().Where(obj => obj.RowStatusId != (int)EnumRowStatus.Deleted);

            if (!string.IsNullOrWhiteSpace(name))
            {
                iQueryable = iQueryable.Where(obj => obj.NameEn.Contains(name) ||
                                                     obj.NameAr.Contains(name));
            }

            if (!string.IsNullOrWhiteSpace(serialNo))
            {
                iQueryable = iQueryable.Where(obj => obj.SerialNumber.Contains(serialNo));
            }

            if (!string.IsNullOrWhiteSpace(category))
            {
                iQueryable = iQueryable.Where(obj => obj.Category.Contains(category));
            }

            List<SparePart> lstSpareParts = iQueryable.ToList();

            return lstSpareParts;
        }
    }
}
