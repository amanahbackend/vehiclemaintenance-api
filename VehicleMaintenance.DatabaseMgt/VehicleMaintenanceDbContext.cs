﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VehicleMaintenance.DatabaseMgt.Common.Settings;
using VehicleMaintenance.DatabaseMgt.Common.Shifts;
using VehicleMaintenance.DatabaseMgt.Common.Vendors;
using VehicleMaintenance.DatabaseMgt.DamageMemos;
using VehicleMaintenance.DatabaseMgt.Drivers;
using VehicleMaintenance.DatabaseMgt.JobCards;
using VehicleMaintenance.DatabaseMgt.JobCards.JobCardStatuses;
using VehicleMaintenance.DatabaseMgt.RepairRequests;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestMaterials;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestSpareParts;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestVendors;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrder;
using VehicleMaintenance.DatabaseMgt.RepairRequests.SparePartsDelivery.DeliveryOrderDetails;
using VehicleMaintenance.DatabaseMgt.Security.Roles;
using VehicleMaintenance.DatabaseMgt.Security.Users;
using VehicleMaintenance.DatabaseMgt.ServiceTypes;
using VehicleMaintenance.DatabaseMgt.SpareParts;
using VehicleMaintenance.DatabaseMgt.SpareParts.SparePartStatuses;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.UploadedFiles;
using VehicleMaintenance.DatabaseMgt.Vehicles;
using VehicleMaintenance.DatabaseMgt.Vehicles.Cannibalizations;
using VehicleMaintenance.DatabaseMgt.Vehicles.VehicleTypes;

namespace VehicleMaintenance.DatabaseMgt
{
    public class VehicleMaintenanceDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public VehicleMaintenanceDbContext(DbContextOptions<VehicleMaintenanceDbContext> options)
            : base(options)
        {

        }

        public DbSet<Vehicle> Vehicle { get; set; }
        public DbSet<VehicleType> VehicleType { get; set; }

        public DbSet<JobCard> JobCard { get; set; }
        public DbSet<JobCardStatus> JobCardStatus { get; set; }

        public DbSet<RepairRequest> RepairRequest { get; set; }
        public DbSet<RepairRequestSparePart> RepairRequestSparePart { get; set; }
        public DbSet<RepairRequestMaterial> RepairRequestMaterial { get; set; }
        public DbSet<RepairRequestVendor> RepairRequestVendor { get; set; }

        public DbSet<SparePartDeliveryOrder> SparePartDeliveryOrder { get; set; }
        public DbSet<SparePartDeliveryOrderDetails> SparePartDeliveryOrderDetails { get; set; }

        public DbSet<SparePart> SparePart { get; set; }
        public DbSet<SparePartStatus> SparePartStatus { get; set; }

        public DbSet<ServiceType> ServiceType { get; set; }
        public DbSet<Driver> Driver { get; set; }
        public DbSet<Technician> Technician { get; set; }
        public DbSet<Vendor> Vendor { get; set; }

        public DbSet<Shift> Shift { get; set; }
        public DbSet<UploadedFile> UploadedFile { get; set; }
        public DbSet<Setting> Setting { get; set; }

        public DbSet<Cannibalization> Cannibalization { get; set; }
        public DbSet<CannibalizationSparePart> CannibalizationSparePart { get; set; }

        public DbSet<DamageMemo> DamageMemo { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>().Property(obj => obj.Id).HasMaxLength(36);
            builder.Entity<ApplicationRole>().Property(obj => obj.Id).HasMaxLength(36);
        }
    }
}
