﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.Common.Settings;
using VehicleMaintenance.DatabaseMgt.UploadedFiles;
using VehicleMaintenance.WebApi.Models.Common;
using VehicleMaintenance.WebApi.Models.UploadedFiles;

namespace VehicleMaintenance.WebApi.Utilities
{
    public static class CommonMethods
    {
        /// <summary>
        /// Prepare uploaded files data.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="dirRelativePath">"/Data/JobCards"</param>
        /// <param name="files"></param>
        /// <param name="createdByUserId"></param>
        public static void PrepareUploadedFilesData(ISettingRepository settings,
            string dirRelativePath, ICollection<UploadedFile> files, string createdByUserId)
        {
            if (files == null || files.Count == 0)
            {
                return;
            }

            // "D:\AmanahWork\KCRM\VehicleMaintenanceApi\VehicleMaintenance.WebApi\wwwroot"
            string apiServiceRootPath = settings.ApiServiceRootPath;

            // Loop for the files to save.
            foreach (UploadedFile uploadedFile in files)
            {
                string fileExtension = Path.GetExtension(uploadedFile.FileName);
                string fileName = $"{Guid.NewGuid()}{fileExtension}";

                // "/Data/JobCards/82c19b61-ef64-41dc-b3f1-fd1b63be6cba.jpg"
                string fileRelativePath = $"{dirRelativePath}/{fileName}";

                // Update uploadedFile.FilePath with fileRelativePath.
                uploadedFile.FileRelativePath = fileRelativePath;

                string fileFullPath = $"{apiServiceRootPath}/{fileRelativePath}";
                uploadedFile.FileFullPath = fileFullPath;

                // Limit the file name to 50 chars.
                if (uploadedFile.FileName.Length > 50)
                {
                    uploadedFile.FileName = uploadedFile.FileName.Substring(0, 45) + fileExtension;
                }

                uploadedFile.PrepareEntityForAdding();
                uploadedFile.CreatedByUserId = createdByUserId;
                uploadedFile.RowStatusId = (int)EnumRowStatus.Active;
            }
        }
        
        public static EntityAddWzFilesViewModel GetEntityAddWzFilesViewModel(int id, ICollection<UploadedFile> files)
        {
            FileListViewModel fileListViewModel = new FileListViewModel
            {
                FilePaths = new List<string>()
            };

            if (files != null)
            {
                foreach (UploadedFile file in files.ToList())
                {
                    fileListViewModel.FilePaths.Add(file.FileFullPath);
                }
            }

            EntityAddWzFilesViewModel entityAddWzFilesViewModel = new EntityAddWzFilesViewModel
            {
                Id = id,
                FileList = fileListViewModel
            };

            return entityAddWzFilesViewModel;
        }

        public static void DeleteFile(ISettingRepository settings, string fileRelativePath)
        {
            try
            {
                string baseUrl = settings.ApiServiceRootPath;
                string filePath = $"{baseUrl}{fileRelativePath}";

                System.IO.File.Delete(filePath);
            }
            catch (Exception)
            {
            }
        }
    }
}
