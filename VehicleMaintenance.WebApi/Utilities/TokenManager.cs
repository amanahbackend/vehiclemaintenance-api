﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using VehicleMaintenance.DatabaseMgt.Security.Users;
using VehicleMaintenance.WebApi.Models.Security;

namespace VehicleMaintenance.WebApi.Utilities
{
    public static class TokenManager
    {
        //public static async Task<Tuple<UserViewModel, TokenResponse>> GetToken
        //    (LoginViewModel model, IUserManager UserManager, string authHeader = null)
        //{
        //    bool deactivated = await UserManager.IsUserDeactivated(model.Username);
        //    if (!deactivated)
        //    {
        //        bool phoneCofirmed = await UserManager.IsPhoneConfirmed(model.Username);
        //        if (phoneCofirmed)
        //        {
        //            TokenResponse tokenResponse = null;
        //            UserViewModel userModel = null;
        //            var discoveryClient = new DiscoveryClient("http://localhost");
        //            discoveryClient.Policy.RequireHttps = false;
        //            discoveryClient.Policy.ValidateIssuerName = false;
        //            var discoveryResponse = await discoveryClient.GetAsync();
        //            if (discoveryResponse.TokenEndpoint != null)
        //            {
        //                var tokenClient = new TokenClient(discoveryResponse.TokenEndpoint, model.ClientId, model.Secret);

        //                tokenResponse = await tokenClient.RequestCustomGrantAsync(model.Username, model.Password, "Mawashi");
        //                if (tokenResponse.AccessToken != null)
        //                {
        //                    User user = await UserManager.GetBy(model.Username);
        //                    userModel = Mapper.Map<User, UserViewModel>(user);
        //                }
        //            }
        //            return new Tuple<UserViewModel, TokenResponse>(userModel, tokenResponse);
        //        }
        //    }
        //    return null;
        //}


        /// this key is sha256 encoding of "mawashiSecret" 
        private static string _jwtSecurityKey = "2dfcdfebc9f0037512964af4f0939bc715616e6609574bf43665e19f9cbc1ecd";

        public static async Task<Tuple<UserViewModel, JwtSecurityToken>> GetToken(
           LoginViewModel model, IUserRepository userManager,
           IPasswordHasher<ApplicationUser> passwordHasher)
        {
            ApplicationUser user = await userManager.GetBy(model.UserName);
            if (user != null)
            {
                bool deactivated = await userManager.IsUserDeactivated(model.UserName);
                if (!deactivated)
                {
                        var passwordCheckResult = passwordHasher.VerifyHashedPassword(user, user.PasswordHash, model.Password);
                        if (passwordCheckResult == PasswordVerificationResult.Success)
                        {
                            var userClaims = await userManager.GetClaimsAsync(user);

                            var claims = new[]
                                {
                                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                                new Claim(JwtRegisteredClaimNames.Jti, user.Id),
                                new Claim(JwtRegisteredClaimNames.Email, user.Email)
                            }.Union(userClaims);

                            byte[] jwtSecurityTokenBytes = Convert.FromBase64String(_jwtSecurityKey);
                            var symmetricSecurityKey = new SymmetricSecurityKey(jwtSecurityTokenBytes);
                            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

                            var token = new JwtSecurityToken(
                                    claims: claims,
                                    expires: DateTime.UtcNow.AddHours(24),
                                    signingCredentials: signingCredentials);

                            var userModel = Mapper.Map<ApplicationUser, UserViewModel>(user);
                            return new Tuple<UserViewModel, JwtSecurityToken>(userModel, token);
                        }
                    }
            }
            return null;
        }

        public static bool ValidateToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;
                
                var symmetricKey = Convert.FromBase64String(_jwtSecurityKey);

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                var principal = tokenHandler.ValidateToken(token, validationParameters, out SecurityToken securityToken);
                return principal.Identity.IsAuthenticated;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //public static async Task<List<string>> GetRolesFromToken(string token, IUserRepository UserManager)
        //{
        //    JwtSecurityToken jwtTokenObj = new JwtSecurityToken(token);
        //    string username = jwtTokenObj.Payload.Sub;
        //    // here i am fady :D :D
        //    var user = await UserManager.GetBy(username);
        //    return user?.RoleNames;
        //}
    }
}
