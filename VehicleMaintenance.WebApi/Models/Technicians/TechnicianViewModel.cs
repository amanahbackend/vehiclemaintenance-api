﻿using System;

namespace VehicleMaintenance.WebApi.Models.Technicians
{
    public class TechnicianViewModel : BaseViewModel
    {
        public string CivilIdNo { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => Convert.ToString($"{FirstName} {LastName}");

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Specialty { get; set; }

        public string EmployeeId { get; set; }

        public double? HourRating { get; set; }

        public bool? Available { get; set; }
    }
}
