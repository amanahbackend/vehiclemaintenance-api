﻿namespace VehicleMaintenance.WebApi.Models.JobCards
{
    public class JobCardStatusViewModel : BaseViewModel
    {
        public string NameAr { get; set; }

        public string NameEn { get; set; }
    }
}
