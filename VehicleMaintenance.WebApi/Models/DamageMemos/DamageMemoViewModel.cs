﻿using VehicleMaintenance.DatabaseMgt.Drivers;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.Vehicles;

namespace VehicleMaintenance.WebApi.Models.DamageMemos
{
    public class DamageMemoViewModel : BaseViewModel
    {
        public string DamageMemoRef { get; set; }

        public int? TechnicianId { get; set; }
        public virtual Technician Technician { get; set; }

        public int? DriverId { get; set; }
        public virtual Driver Driver { get; set; }

        public int? VehicleId { get; set; }

        /// <summary>
        /// This property in ViewModel only.
        /// </summary>
        public string VehicleFleetNo { get; set; }

        public virtual Vehicle Vehicle { get; set; }

        public string ForemanName { get; set; }

        public string SparePart1Name { get; set; }

        public string SparePart2Name { get; set; }

        public string SparePart3Name { get; set; }
    }
}
