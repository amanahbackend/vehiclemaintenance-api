﻿using System;
using VehicleMaintenance.DatabaseMgt.Vehicles.VehicleTypes;

namespace VehicleMaintenance.WebApi.Models.Vehicles
{
    public class VehicleViewModel : BaseViewModel
    {
        public string PlateNumber { get; set; }
        
        public string FleetNumber { get; set; }
        
        public string ChassisNumber { get; set; }

        public string CustomerName { get; set; }

        public int? Year { get; set; }
        
        public string Make { get; set; }
        
        public string Model { get; set; }
        
        public string Color { get; set; }

        public int? KiloMeters { get; set; }
        
        public string Vin { get; set; }

        public int? VehicleTypeId { get; set; }
        public virtual VehicleType VehicleType { get; set; }
        
        public string Status { get; set; }
        
        public string Description { get; set; }
        
        public string Remarks { get; set; }

        public double? Odometer { get; set; }

        public DateTime? PurchaseDate { get; set; }

        public int? FuelConsumption { get; set; }

        public string WorkIdleTime { get; set; }
    }
}
