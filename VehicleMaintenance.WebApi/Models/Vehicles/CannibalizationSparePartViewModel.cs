﻿using VehicleMaintenance.DatabaseMgt.SpareParts;

namespace VehicleMaintenance.WebApi.Models.Vehicles
{
    public class CannibalizationSparePartViewModel : BaseViewModel
    {
        public int? CannibalizationId { get; set; }

        public int? SparePartId { get; set; }
        public virtual SparePart SparePart { get; set; }
    }
}
