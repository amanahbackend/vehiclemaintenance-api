﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.Vehicles;

namespace VehicleMaintenance.WebApi.Models.Vehicles
{
    public class CannibalizationViewModel : BaseViewModel
    {
        public DateTime? FixingDate { get; set; }

        public int? VehicleFromId { get; set; }

        /// <summary>
        /// This property in ViewModel only.
        /// </summary>
        public string VehicleFromFleetNo { get; set; }
        public virtual Vehicle VehicleFrom { get; set; }

        [StringLength(50)]
        public string VehicleFromRegnNo { get; set; }

        public int? TechnicianRemovedById { get; set; }
        public virtual Technician TechnicianRemovedBy { get; set; }

        public int? VehicleToId { get; set; }
        
        /// <summary>
        /// This property in ViewModel only.
        /// </summary>
        public string VehicleToFleetNo { get; set; }
        public virtual Vehicle VehicleTo { get; set; }

        [StringLength(50)]
        public string VehicleToRegnNo { get; set; }

        public int? TechnicianFixedById { get; set; }
        public virtual Technician TechnicianFixedBy { get; set; }

        [StringLength(250)]
        public string SupervisorComments { get; set; }

        public List<CannibalizationSparePartViewModel> CannibalizationSpareParts { get; set; }

        public DateTime? RefixingDate { get; set; }

        public int? TechnicianRefixedById { get; set; }
        public virtual Technician TechnicianRefixedBy { get; set; }
    }
}
