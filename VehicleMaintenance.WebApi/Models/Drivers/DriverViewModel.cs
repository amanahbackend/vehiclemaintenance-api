﻿using System;

namespace VehicleMaintenance.WebApi.Models.Drivers
{
    public class DriverViewModel : BaseViewModel
    {
        public string CivilIdNo { get; set; }

        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => Convert.ToString($"{FirstName} {LastName}");

        public string PhoneNumber { get; set; }

        public string Address { get; set; }

        public DateTime BirthDate { get; set; }

        public string Ssn { get; set; }

        public DateTime? LicenseIssuedDate { get; set; }

        public string LicenseIssuedState { get; set; }

        public string LicenseNumber { get; set; }

        public string Gender { get; set; }

        public string MaritalStatus { get; set; }

        public string EmployeeId { get; set; }
    }
}
