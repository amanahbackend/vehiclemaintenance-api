﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace VehicleMaintenance.WebApi.Models.UploadedFiles
{
    public class FileListViewModel
    {
        /// <summary>
        /// This property should be filled from API to UI to be sent again to API.
        /// </summary>
        public List<string> FilePaths { get; set; }

        /// <summary>
        /// This property should be filled from UI. FormFiles is the attached files list.
        /// </summary>
        public List<IFormFile> FormFiles { get; set; }
    }
}
