﻿namespace VehicleMaintenance.WebApi.Models.RepairRequests
{
    public class RepairRequestTypeViewModel : BaseViewModel
    {
        public string NameAr { get; set; }

        public string NameEn { get; set; }

        public bool? NeedApprove { get; set; }
    }
}
