﻿using System.Collections.Generic;
using VehicleMaintenance.DatabaseMgt.Common.Vendors;
using VehicleMaintenance.DatabaseMgt.RepairRequests;
using VehicleMaintenance.DatabaseMgt.UploadedFiles;

namespace VehicleMaintenance.WebApi.Models.RepairRequests
{
    public class RepairRequestVendorViewModel : BaseViewModel
    {
        public int? RepairRequestId { get; set; }
        public virtual RepairRequest RepairRequest { get; set; }

        public int? VendorId { get; set; }
        public virtual Vendor Vendor { get; set; }

        public virtual ICollection<UploadedFile> Files { get; set; }
    }
}
