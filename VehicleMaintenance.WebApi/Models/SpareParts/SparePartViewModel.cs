﻿namespace VehicleMaintenance.WebApi.Models.SpareParts
{
    public class SparePartViewModel : BaseViewModel
    {
        public string SerialNumber { get; set; }

        public string NameAr { get; set; }

        public string NameEn { get; set; }

        public bool? NeedApprove { get; set; }

        public string Category { get; set; }

        public string CaseAr { get; set; }

        public string CaseEn { get; set; }

        public string Model { get; set; }

        public string Make { get; set; }

        public string Description { get; set; }

        public decimal? Cost { get; set; }

        public string Unit { get; set; }
    }
}
