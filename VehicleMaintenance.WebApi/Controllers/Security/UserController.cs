﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt.Security.Users;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Security;

namespace VehicleMaintenance.WebApi.Controllers.Security
{
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;

        public UserController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signManager, IMapper mapper)
        {
            _userManager = userManager;
            _signManager = signManager;
            _mapper = mapper;
        }

        [HttpGet, Route("Get")]
        public async Task<ResponseResult<List<UserViewModel>>> Get()
        {
            return await Search(null);
        }

        [HttpGet, Route("Search")]
        public async Task<ResponseResult<List<UserViewModel>>> Search(string keyword)
        {
            ResponseResult<List<UserViewModel>> responseResult;

            try
            {
                IQueryable<ApplicationUser> iqUserManagerUsers = _userManager.Users;

                if (!string.IsNullOrWhiteSpace(keyword))
                {
                    iqUserManagerUsers = iqUserManagerUsers.Where(
                        obj => obj.FirstName.Contains(keyword) ||
                               obj.LastName.Contains(keyword));
                }

                List<ApplicationUser> lstApplicationUsers =
                    iqUserManagerUsers.OrderBy(obj => obj.FirstName).ToList();

                foreach (var user in lstApplicationUsers)
                {
                    user.RoleNames = (await _userManager.GetRolesAsync(user)).ToList();
                }

                List<UserViewModel> lstUserViewModels = _mapper.Map<List<UserViewModel>>(lstApplicationUsers);

                responseResult = new ResponseResult<List<UserViewModel>>
                {
                    Data = lstUserViewModels
                };
            }
            catch (Exception ex)
            {
                responseResult = new ResponseResult<List<UserViewModel>>
                {
                    Message = ex.Message,
                    StatusCode = 0
                };
            }

            return responseResult;
        }


        [HttpGet, Route("Get/{userId}")]
        public async Task<ResponseResult<UserViewModel>> Get(string userId)
        {
            ResponseResult<UserViewModel> responseResult;

            try
            {
                ApplicationUser applicationUser =
                    _userManager.Users.FirstOrDefault(obj => obj.Id == userId);

                if (applicationUser != null)
                {
                    applicationUser.RoleNames = (await _userManager.GetRolesAsync(applicationUser)).ToList();
                }

                UserViewModel userViewModel = _mapper.Map<UserViewModel>(applicationUser);

                responseResult = new ResponseResult<UserViewModel>
                {
                    Data = userViewModel
                };
            }
            catch (Exception ex)
            {
                responseResult = new ResponseResult<UserViewModel>
                {
                    Message = ex.Message,
                    StatusCode = 0
                };
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public async Task<ResponseResult<UserViewModel>> Add([FromBody] UserViewModel userViewModel)
        {
            ResponseResult<UserViewModel> responseResult;

            try
            {
                ApplicationUser applicationUser = _mapper.Map<ApplicationUser>(userViewModel);
                string password = userViewModel.Password;
                applicationUser.CreationDate = DateTime.Now;

                IdentityResult identityResult = await _userManager.CreateAsync(applicationUser, password);

                if (identityResult.Succeeded)
                {
                    if (applicationUser.RoleNames != null && applicationUser.RoleNames.Count > 0)
                    {
                        string userRoleName = applicationUser.RoleNames[0];

                        identityResult = await _userManager.AddToRoleAsync(applicationUser, userRoleName);
                    }

                    if (identityResult.Succeeded)
                    {
                        userViewModel = _mapper.Map<ApplicationUser, UserViewModel>(applicationUser);

                        responseResult = new ResponseResult<UserViewModel>
                        {
                            Data = userViewModel,
                            StatusCode = 1
                        };
                    }
                    else
                    {
                        List<IdentityError> identityErrors = identityResult.Errors.ToList();
                        string error = identityErrors.Count > 0 ? identityErrors[0].Description : string.Empty;

                        responseResult = new ResponseResult<UserViewModel>
                        {
                            Message = "An error occurred while adding user roles. " + error,
                            StatusCode = -2
                        };
                    }
                }
                else
                {
                    List<IdentityError> identityErrors = identityResult.Errors.ToList();
                    string error = identityErrors.Count > 0 ? identityErrors[0].Description : string.Empty;

                    responseResult = new ResponseResult<UserViewModel>
                    {
                        Message = "An error occurred while adding user. " + error,
                        StatusCode = -1
                    };
                }
            }
            catch (Exception ex)
            {
                responseResult = new ResponseResult<UserViewModel>
                {
                    Message = ex.Message,
                    StatusCode = 0
                };
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public async Task<ResponseResult<bool>> Update([FromBody] UserViewModel userViewModel)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                string userId = userViewModel.Id;

                IdentityResult identityResult = await UpdateUser(userId, userViewModel);

                if (identityResult.Succeeded)
                {
                    responseResult = new ResponseResult<bool>
                    {
                        Data = identityResult.Succeeded,
                        StatusCode = 1
                    };
                }
                else
                {
                    List<IdentityError> identityErrors = identityResult.Errors.ToList();
                    string error = identityErrors.Count > 0 ? identityErrors[0].Description : string.Empty;

                    responseResult = new ResponseResult<bool>
                    {
                        Message = "An error occurred while updating user. " + error,
                        StatusCode = -1
                    };
                }
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        private async Task<IdentityResult> UpdateUser(string userId, UserViewModel userViewModel)
        {
            IdentityResult identityResult;

            ApplicationUser applicationUser = await _userManager.FindByIdAsync(userId);

            if (applicationUser == null)
            {
                identityResult = new IdentityResult();

                identityResult.Errors.Append(new IdentityError
                {
                    Code = "-1",
                    Description = "User not found."
                });

                return identityResult;
            }

            applicationUser.FirstName = userViewModel.FirstName;
            applicationUser.LastName = userViewModel.LastName;
            applicationUser.PhoneNumber = userViewModel.PhoneNumber;
            applicationUser.Email = userViewModel.Email;
            applicationUser.ModifiedByUserId = userViewModel.ModifiedByUserId;
            applicationUser.ModificationDate = DateTime.Now;

            identityResult = await _userManager.UpdateAsync(applicationUser);

            if (!identityResult.Succeeded)
            {
                return identityResult;
            }

            if (userViewModel.RoleNames == null || userViewModel.RoleNames.Count == 0)
            {
                return identityResult;
            }

            // Remove user from previous role(s).
            IList<string> userRoles = await _userManager.GetRolesAsync(applicationUser);
            identityResult = await _userManager.RemoveFromRolesAsync(applicationUser, userRoles);

            if (!identityResult.Succeeded)
            {
                return identityResult;
            }

            // Add user to role.
            string userRoleName = userViewModel.RoleNames[0];

            identityResult = await _userManager.AddToRoleAsync(applicationUser, userRoleName);

            return identityResult;
        }


        [HttpPost, Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel loginViewModel)
        {
            IActionResult actionResult;

            try
            {
                if (loginViewModel == null)
                {
                    actionResult = Unauthorized("loginViewModel is null.");

                    return actionResult;
                }

                if (string.IsNullOrWhiteSpace(loginViewModel.UserName) ||
                    string.IsNullOrWhiteSpace(loginViewModel.Password))
                {
                    actionResult = Unauthorized("loginViewModel.UserName or loginViewModel.Password has null or empty value.");

                    return actionResult;
                }

                string userName = loginViewModel.UserName;
                string password = loginViewModel.Password;
                const bool rememberMe = false;

                var result = await _signManager.PasswordSignInAsync(userName, password, rememberMe, false);

                if (result.Succeeded)
                {
                    ApplicationUser applicationUser = await _userManager.FindByNameAsync(userName);
                    IList<string> userRoles = await _userManager.GetRolesAsync(applicationUser);

                    OkObjectResult okObjectResult = Ok(new
                    {
                        UserName = applicationUser.UserName,
                        Roles = userRoles,
                        userId = applicationUser.Id
                    });

                    actionResult = okObjectResult;
                }
                else
                {
                    actionResult = Unauthorized("User name and password aren't match. result.Succeeded = false.");
                }
            }
            catch (Exception ex)
            {
                actionResult = Unauthorized($"Exception occurred. {ex.Message}");
            }

            return actionResult;
        }


        [Route("Count")]
        public ResponseResult<int> Count()
        {
            var responseResult = new ResponseResult<int>();

            try
            {
                int usersCount = _userManager.Users.Count();
                responseResult.Data = usersCount;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("UpdateUserPassword")]
        public async Task<ResponseResult<bool>> UpdateUserPassword([FromBody] UserViewModel userViewModel)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                string userId = userViewModel.Id;
                string newPassword = userViewModel.Password;
                string modifiedByUserId = userViewModel.ModifiedByUserId;

                bool isUserPasswordUpdated = await UpdateUserPassword(userId, newPassword, modifiedByUserId);

                responseResult.Data = isUserPasswordUpdated;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        private async Task<bool> UpdateUserPassword(string userId, string newPassword, string modifiedByUserId)
        {
            ApplicationUser applicationUser = await _userManager.FindByIdAsync(userId);

            if (applicationUser == null)
            {
                return false;
            }

            applicationUser.PasswordHash = _userManager.PasswordHasher.HashPassword(applicationUser, newPassword);
            applicationUser.ModifiedByUserId = modifiedByUserId;
            applicationUser.ModificationDate = DateTime.Now;

            IdentityResult identityResult = await _userManager.UpdateAsync(applicationUser);

            return identityResult.Succeeded;
        }


        [HttpDelete, Route("Delete/{userName}")]
        public async Task<ResponseResult<bool>> Delete([FromRoute] string userName)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                bool isDeleted = await DeleteUserByNameAsync(userName);

                responseResult.Data = isDeleted;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        private async Task<bool> DeleteUserByNameAsync(string userName)
        {
            IdentityResult identityResult;

            var applicationUser = await _userManager.FindByNameAsync(userName);

            if (applicationUser != null)
            {
                identityResult = await _userManager.DeleteAsync(applicationUser);
            }
            else
            {
                identityResult = IdentityResult.Failed(new IdentityError
                {
                    Description = "User not found."
                });
            }

            return identityResult.Succeeded;
        }
    }
}

