﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.Vehicles.VehicleTypes;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Vehicles;

namespace VehicleMaintenance.WebApi.Controllers.Vehicles
{
    [Route("api/VehicleType")]
    [Produces("application/json")]
    public class VehicleTypeController : BaseController
    {
        public VehicleTypeController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        [HttpGet, Route("Get")]
        public ResponseResult<List<VehicleTypeViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<VehicleTypeViewModel>>();

            try
            {
                List<VehicleType> lstVehicleTypes = UnitOfWork.VehicleTypes.Find(obj => obj.RowStatusId != -1).ToList();
                var lstVehicleTypeVm = Mapper.Map<List<VehicleTypeViewModel>>(lstVehicleTypes);

                responseResult.Data = lstVehicleTypeVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("Get/{id}")]
        public ResponseResult<VehicleTypeViewModel> Get(int id)
        {
            var responseResult = new ResponseResult<VehicleTypeViewModel>();

            try
            {
                VehicleType vehicleType = UnitOfWork.VehicleTypes.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                VehicleTypeViewModel vehicleTypeVm = Mapper.Map<VehicleTypeViewModel>(vehicleType);
                responseResult.Data = vehicleTypeVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<VehicleTypeViewModel> Add([FromBody] VehicleTypeViewModel model)
        {
            var responseResult = new ResponseResult<VehicleTypeViewModel>();

            try
            {
                VehicleType vehicleType = Mapper.Map<VehicleType>(model);
                vehicleType.PrepareEntityForAdding();

                UnitOfWork.VehicleTypes.Add(vehicleType);

                // Re-map the added model again to update Id.
                var vehicleTypeVm = Mapper.Map<VehicleTypeViewModel>(vehicleType);
                responseResult.Data = vehicleTypeVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] VehicleTypeViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                VehicleType vehicleType = Mapper.Map<VehicleType>(model);
                vehicleType.PrepareEntityForEditing(model);

                UnitOfWork.VehicleTypes.Update(vehicleType);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                VehicleType vehicleType = UnitOfWork.VehicleTypes.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                vehicleType.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.VehicleTypes.Update(vehicleType);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
