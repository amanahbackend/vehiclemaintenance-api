﻿using System;
using System.Collections.Generic;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.Vehicles.Cannibalizations;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Vehicles;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace VehicleMaintenance.WebApi.Controllers.Vehicles
{
    [Route("api/Cannibalization")]
    [Produces("application/json")]
    public class CannibalizationController : BaseController
    {
        public CannibalizationController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        [HttpGet, Route("Get")]
        public ResponseResult<List<CannibalizationViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<CannibalizationViewModel>>();

            try
            {
                List<Cannibalization> lstCannibalizations = UnitOfWork.Cannibalizations.Get();

                var lstCannibalizationVm = Mapper.Map<List<CannibalizationViewModel>>(lstCannibalizations);

                responseResult.Data = lstCannibalizationVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("Get/{id}")]
        public ResponseResult<CannibalizationViewModel> Get(int id)
        {
            var responseResult = new ResponseResult<CannibalizationViewModel>();

            try
            {
                Cannibalization cannibalization = UnitOfWork.Cannibalizations.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                CannibalizationViewModel cannibalizationVm = Mapper.Map<CannibalizationViewModel>(cannibalization);
                responseResult.Data = cannibalizationVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<CannibalizationViewModel> Add([FromBody] CannibalizationViewModel model)
        {
            var responseResult = new ResponseResult<CannibalizationViewModel>();

            try
            {
                Cannibalization cannibalization = Mapper.Map<Cannibalization>(model);
                cannibalization.PrepareEntityForAdding();

                cannibalization.VehicleFromId = UnitOfWork.Vehicles.GetByVehicleFleetNo(model.VehicleFromFleetNo).Id;
                cannibalization.VehicleToId = UnitOfWork.Vehicles.GetByVehicleFleetNo(model.VehicleToFleetNo).Id;

                if (cannibalization.CannibalizationSpareParts != null)
                {
                    foreach (var canniSparePart in cannibalization.CannibalizationSpareParts)
                    {
                        canniSparePart.CreatedByUserId = cannibalization.CreatedByUserId;
                        canniSparePart.CreationDate = canniSparePart.CreationDate;
                        canniSparePart.RowStatusId = (int)EnumRowStatus.Active;
                    }
                }

                UnitOfWork.Cannibalizations.Add(cannibalization);

                // Re-map the added model again to update Id.
                var cannibalizationVm = Mapper.Map<CannibalizationViewModel>(cannibalization);
                responseResult.Data = cannibalizationVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] CannibalizationViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                Cannibalization cannibalization = Mapper.Map<Cannibalization>(model);
                cannibalization.PrepareEntityForEditing(model);

                UnitOfWork.Cannibalizations.Update(cannibalization);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                Cannibalization cannibalization = UnitOfWork.Cannibalizations.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                cannibalization.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.Cannibalizations.Update(cannibalization);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
