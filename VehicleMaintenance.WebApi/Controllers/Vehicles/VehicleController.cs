﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.Vehicles;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Vehicles;

namespace VehicleMaintenance.WebApi.Controllers.Vehicles
{
    [Route("api/Vehicle")]
    [Produces("application/json")]
    public class VehicleController : BaseController
    {
        private readonly bool _confirmUniquenessOfPlateNoAndFleetNo;

        public VehicleController(UnitOfWork context, IMapper mapper) : base(context, mapper)
        {
            _confirmUniquenessOfPlateNoAndFleetNo = false;
        }

        [HttpGet, Route("SearchVehiclesRowsCount")]
        public ResponseResult<int> SearchVehiclesRowsCount(
            [FromQuery] string plateNo, string fleetNo, string model, string vehicleStatus)
        {
            var responseResult = new ResponseResult<int>();

            try
            {
                var rowsCount = UnitOfWork.Vehicles.SearchVehiclesRowsCount(plateNo, fleetNo, model, vehicleStatus);

                responseResult.Data = rowsCount;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpGet, Route("SearchVehicles")]
        public ResponseResult<List<VehicleViewModel>> SearchVehicles(
            [FromQuery] string plateNo, string fleetNo, string model, string vehicleStatus, int pageNo, int pageSize)
        {
            var responseResult = new ResponseResult<List<VehicleViewModel>>();

            try
            {
                List<Vehicle> lstVehicles = UnitOfWork.Vehicles.SearchVehicles(plateNo, fleetNo, model, vehicleStatus, pageNo, pageSize);
                var lstVehiclesVm = Mapper.Map<List<VehicleViewModel>>(lstVehicles);

                responseResult.Data = lstVehiclesVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpGet, Route("getVehiclesPerPage")]
        public ResponseResult<List<VehicleViewModel>> GetVehiclesPerPage(
            [FromQuery] string plateNo, string fleetNo, string model, string vehicleStatus, int pageNo, int pageSize)
        {
            return SearchVehicles(null, null, null, null, pageNo, pageSize);
        }

        [HttpGet, Route("Get")]
        public ResponseResult<List<VehicleViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<VehicleViewModel>>();

            try
            {
                List<Vehicle> lstVehicles = UnitOfWork.Vehicles.Find(obj => obj.RowStatusId != -1).ToList();
                var lstVehicleVm = Mapper.Map<List<VehicleViewModel>>(lstVehicles);

                responseResult.Data = lstVehicleVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [Route("Get/{id}")]
        public ResponseResult<VehicleViewModel> Get(int id)
        {
            var responseResult = new ResponseResult<VehicleViewModel>();

            try
            {
                Vehicle vehicle = UnitOfWork.Vehicles.GetVehicle(id);
                VehicleViewModel vehicleVm = Mapper.Map<VehicleViewModel>(vehicle);
                responseResult.Data = vehicleVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpGet, Route("Count")]
        public ResponseResult<int> Count()
        {
            var responseResult = new ResponseResult<int>();

            try
            {
                int vehiclesCount = UnitOfWork.Vehicles.GetQueryable().Count(obj => obj.RowStatusId != -1);
                responseResult.Data = vehiclesCount;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpPost, Route("Add")]
        public ResponseResult<VehicleViewModel> Add([FromBody] VehicleViewModel model)
        {
            var responseResult = new ResponseResult<VehicleViewModel>();

            try
            {
                if (_confirmUniquenessOfPlateNoAndFleetNo)
                {
                    // Confirm that the vehicle plate number doesn't exist.
                    bool plateNoExists = UnitOfWork.Vehicles.IsPlateNoExists(model.PlateNumber, null);

                    if (plateNoExists)
                    {
                        responseResult.StatusCode = -1;
                        responseResult.Message = "Vehicle plate number already exists.";

                        return responseResult;
                    }

                    // Confirm that the vehicle fleet number doesn't exist.
                    bool fleetNoExists = UnitOfWork.Vehicles.IsFleetNoExists(model.FleetNumber, null);

                    if (fleetNoExists)
                    {
                        responseResult.StatusCode = -2;
                        responseResult.Message = "Vehicle fleet number already exists.";

                        return responseResult;
                    }
                }

                Vehicle vehicle = Mapper.Map<Vehicle>(model);
                vehicle.PrepareEntityForAdding();

                UnitOfWork.Vehicles.Add(vehicle);

                // Re-map the added Vehicle again to update Id.
                var vehicleVm = Mapper.Map<VehicleViewModel>(vehicle);
                responseResult.Data = vehicleVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] VehicleViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                if (_confirmUniquenessOfPlateNoAndFleetNo)
                {
                    // Confirm that the vehicle plate number doesn't exist.
                    bool plateNoExists = UnitOfWork.Vehicles.IsPlateNoExists(model.PlateNumber, model.Id);

                    if (plateNoExists)
                    {
                        responseResult.StatusCode = -1;
                        responseResult.Message = "Vehicle plate number already exists.";

                        return responseResult;
                    }

                    // Confirm that the vehicle fleet number doesn't exist.
                    bool fleetNoExists = UnitOfWork.Vehicles.IsFleetNoExists(model.FleetNumber, model.Id);

                    if (fleetNoExists)
                    {
                        responseResult.StatusCode = -2;
                        responseResult.Message = "Vehicle fleet number already exists.";

                        return responseResult;
                    }
                }

                Vehicle vehicle = Mapper.Map<Vehicle>(model);
                vehicle.PrepareEntityForEditing(model);

                UnitOfWork.Vehicles.Update(vehicle);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                Vehicle vehicleType = UnitOfWork.Vehicles.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                vehicleType.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.Vehicles.Update(vehicleType);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}