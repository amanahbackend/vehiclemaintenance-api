﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.Drivers;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Drivers;

namespace VehicleMaintenance.WebApi.Controllers.Drivers
{
    [Route("api/Driver")]
    [Produces("application/json")]
    public class DriverController : BaseController
    {
        public DriverController(UnitOfWork context, IMapper mapper) : base(context, mapper)
        {
        }


        [Route("Get")]
        public ResponseResult<List<DriverViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<DriverViewModel>>();

            try
            {
                List<Driver> lstDrivers = UnitOfWork.Drivers.Find(obj => obj.RowStatusId != -1).ToList();
                var lstDriverVm = Mapper.Map<List<DriverViewModel>>(lstDrivers);

                responseResult.Data = lstDriverVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [Route("Get/{id}")]
        public ResponseResult<DriverViewModel> Get(int id)
        {
            var responseResult = new ResponseResult<DriverViewModel>();

            try
            {
                Driver driver = UnitOfWork.Drivers.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                DriverViewModel driverVm = Mapper.Map<DriverViewModel>(driver);
                responseResult.Data = driverVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("SearchByWord")]
        public ResponseResult<List<DriverViewModel>> SearchByWord(string keyword)
        {
            var responseResult = new ResponseResult<List<DriverViewModel>>();

            try
            {
                List<Driver> lstDrivers = UnitOfWork.Drivers.SearchByWord(keyword);
                var lstDriversVm = Mapper.Map<List<DriverViewModel>>(lstDrivers);

                responseResult.Data = lstDriversVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<DriverViewModel> Add([FromBody] DriverViewModel model)
        {
            var responseResult = new ResponseResult<DriverViewModel>();

            try
            {
                Driver driver = Mapper.Map<Driver>(model);
                driver.PrepareEntityForAdding();
                
                UnitOfWork.Drivers.Add(driver);

                // Re-map the added driver again to update Id.
                var driverVm = Mapper.Map<DriverViewModel>(driver);
                responseResult.Data = driverVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<DriverViewModel> Update([FromBody] DriverViewModel model)
        {
            var responseResult = new ResponseResult<DriverViewModel>();

            try
            {
                Driver driver = Mapper.Map<Driver>(model);
                driver.PrepareEntityForEditing(model);
                
                UnitOfWork.Drivers.Update(driver);

                var driverVm = Mapper.Map<DriverViewModel>(driver);
                responseResult.Data = driverVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                Driver driver = UnitOfWork.Drivers.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                driver.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.Drivers.Update(driver);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}