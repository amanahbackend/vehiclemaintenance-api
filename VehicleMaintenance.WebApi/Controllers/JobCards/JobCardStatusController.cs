﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.JobCards.JobCardStatuses;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.JobCards;

namespace VehicleMaintenance.WebApi.Controllers.JobCards
{
    [Route("api/JobCardStatus")]
    [Produces("application/json")]
    public class JobCardStatusController : BaseController
    {
        public JobCardStatusController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        [HttpGet, Route("Get")]
        public ResponseResult<List<JobCardStatusViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<JobCardStatusViewModel>>();

            try
            {
                List<JobCardStatus> lstJobCardStatuses = UnitOfWork.JobCardStatuses.Find(obj => obj.RowStatusId != -1).ToList();
                var lstJobCardStatusVm = Mapper.Map<List<JobCardStatusViewModel>>(lstJobCardStatuses);

                responseResult.Data = lstJobCardStatusVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
