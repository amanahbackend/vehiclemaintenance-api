﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.JobCards;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Common;
using VehicleMaintenance.WebApi.Models.JobCards;
using VehicleMaintenance.WebApi.Utilities;

namespace VehicleMaintenance.WebApi.Controllers.JobCards
{
    [Route("api/JobCard")]
    [Produces("application/json")]
    public class JobCardController : BaseController
    {
        public JobCardController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        [HttpGet, Route("GetJobCard/{jobCardId}")]
        public ResponseResult<JobCardViewModel> GetJobCard([FromRoute] int jobCardId)
        {
            var responseResult = new ResponseResult<JobCardViewModel>();

            try
            {
                var jobCard = UnitOfWork.JobCards.GetJobCard(jobCardId);
                var lstJobCardsVm = Mapper.Map<JobCardViewModel>(jobCard);

                responseResult.Data = lstJobCardsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpGet, Route("SearchJobCards")]
        public ResponseResult<List<JobCardViewModel>> SearchJobCards(
            [FromQuery] string customerName, DateTime? dateIn, DateTime? dateOut,
            int? serviceTypeId, int? jobCardStatusId, string vehiclePlateNo,
            string vehicleFleetNo, int? jobCardId)
        {
            var responseResult = new ResponseResult<List<JobCardViewModel>>();

            try
            {
                var lstJobCards = UnitOfWork.JobCards.SearchJobCards(customerName, dateIn, dateOut,
                    serviceTypeId, jobCardStatusId, vehiclePlateNo, vehicleFleetNo, jobCardId);

                var lstJobCardsVm = Mapper.Map<List<JobCardViewModel>>(lstJobCards);

                responseResult.Data = lstJobCardsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpGet, Route("GetJobCards")]
        public ResponseResult<List<JobCardViewModel>> GetJobCards()
        {
            var responseResult = new ResponseResult<List<JobCardViewModel>>();

            try
            {
                var lstJobCards = UnitOfWork.JobCards.GetJobCards();
                var lstJobCardsVm = Mapper.Map<List<JobCardViewModel>>(lstJobCards);

                responseResult.Data = lstJobCardsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetByOpenStatus")]
        public ResponseResult<List<JobCardViewModel>> GetByOpenStatus()
        {
            var responseResult = new ResponseResult<List<JobCardViewModel>>();

            try
            {
                var lstJobCards = UnitOfWork.JobCards.GetByOpenStatus();
                var lstJobCardsVm = Mapper.Map<List<JobCardViewModel>>(lstJobCards);

                responseResult.Data = lstJobCardsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetByOpenStatusOfVehicle/{vehicleId}")]
        public ResponseResult<List<JobCardViewModel>> GetByOpenStatusOfVehicle([FromRoute] int vehicleId)
        {
            var responseResult = new ResponseResult<List<JobCardViewModel>>();

            try
            {
                var lstJobCards = UnitOfWork.JobCards.GetByOpenStatus(vehicleId);
                var lstJobCardsVm = Mapper.Map<List<JobCardViewModel>>(lstJobCards);

                responseResult.Data = lstJobCardsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetCancelledOrClosedStatuses/{vehicleId}")]
        public ResponseResult<List<JobCardViewModel>> GetCancelledOrClosedStatuses([FromRoute] int vehicleId)
        {
            var responseResult = new ResponseResult<List<JobCardViewModel>>();

            try
            {
                var lstJobCards = UnitOfWork.JobCards.GetCancelledOrClosedStatuses(vehicleId);
                var lstJobCardsVm = Mapper.Map<List<JobCardViewModel>>(lstJobCards);

                responseResult.Data = lstJobCardsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetNonCancelledNonClosedStatuses/{vehicleId}")]
        public ResponseResult<List<JobCardViewModel>> GetExceptClosedNorCancelledStatuses([FromRoute] int vehicleId)
        {
            var responseResult = new ResponseResult<List<JobCardViewModel>>();

            try
            {
                var lstJobCards = UnitOfWork.JobCards.GetNonCancelledNonClosedStatuses(vehicleId);
                var lstJobCardsVm = Mapper.Map<List<JobCardViewModel>>(lstJobCards);

                responseResult.Data = lstJobCardsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetByVehicleId/{vehicleId}")]
        public ResponseResult<List<JobCardViewModel>> GetByVehicleId([FromRoute] int vehicleId)
        {
            var responseResult = new ResponseResult<List<JobCardViewModel>>();

            try
            {
                var lstJobCards = UnitOfWork.JobCards.GetByVehicleId(vehicleId);
                var lstJobCardsVm = Mapper.Map<List<JobCardViewModel>>(lstJobCards);

                responseResult.Data = lstJobCardsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("CanAddJobCard/{VehicleId}")]
        public ResponseResult<bool> CanAddJobCard([FromRoute] int vehicleId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                bool canAddJobCard = UnitOfWork.JobCards.CanAddJobCard(vehicleId);

                responseResult.Data = canAddJobCard;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("OpenJobCardsCount")]
        public ResponseResult<int> OpenJobCardsCount()
        {
            var responseResult = new ResponseResult<int>();

            try
            {
                int repairRequestsCount = UnitOfWork.JobCards.GetOpenJobCardsCount();
                responseResult.Data = repairRequestsCount;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        //[HttpPost, Route("getDailyServiceReport")]
        //public ResponseResult<List<JobCardViewModel>> GetDailyServiceReport([FromBody] DailyServiceSearchParams model)
        //{
        //    var responseResult = new ResponseResult<List<JobCardViewModel>>();

        //    try
        //    {
        //        var lstJobCards = UnitOfWork.JobCards.GetDailyReceiptReport(model.VehiclePlateNo, model.StartDate, model.EndDate);
        //        var lstJobCardsVm = Mapper.Map<List<JobCardViewModel>>(lstJobCards);

        //        responseResult.Data = lstJobCardsVm;
        //    }
        //    catch (Exception ex)
        //    {
        //        responseResult.StatusCode = 0;
        //        responseResult.Message = ex.Message;
        //    }

        //    return responseResult;
        //}


        [HttpPost, Route("Add")]
        public ResponseResult<EntityAddWzFilesViewModel> Add([FromBody] JobCardViewModel model)
        {
            var responseResult = new ResponseResult<EntityAddWzFilesViewModel>();

            try
            {
                const string dirRelativePath = "/Data/JobCards";
                CommonMethods.PrepareUploadedFilesData(UnitOfWork.Settings, dirRelativePath, model.Files, model.CreatedByUserId);

                JobCard jobCard = Mapper.Map<JobCard>(model);
                jobCard.PrepareEntityForAdding();

                UnitOfWork.JobCards.Add(jobCard);

                EntityAddWzFilesViewModel entityAddWzFilesViewModel = CommonMethods.GetEntityAddWzFilesViewModel(jobCard.Id, model.Files);

                responseResult.Data = entityAddWzFilesViewModel;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] JobCardViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                int jobCardId = model.Id;

                // Not to close job card if it has some unclosed repair requests.
                if (model.JobCardStatusId == (int)EnumJobCardStatus.Closed) // Closed.
                {
                    bool allRepairRequestsClosed = UnitOfWork.RepairRequests.AllRepairRequestsAreClosedForJobCard(jobCardId);

                    if (!allRepairRequestsClosed)
                    {
                        responseResult.StatusCode = -1;
                        responseResult.Message = "You should close all repair requests before closing the job card.";

                        return responseResult;
                    }
                }

                JobCard jobCard = UnitOfWork.JobCards.Get(jobCardId);

                // Set fields to be updated.
                jobCard.PrepareEntityForEditing(model);
                jobCard.DriverId = model.DriverId;
                jobCard.Description = model.Description;
                jobCard.Odometer = model.Odometer;
                jobCard.Hourmeter = model.Hourmeter;
                jobCard.JobCardStatusId = model.JobCardStatusId;
                jobCard.Comment = model.Comment;

                jobCard.DateIn = model.DateIn;
                jobCard.DateOut = model.DateOut;

                UnitOfWork.JobCards.Update(jobCard);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                var entity = UnitOfWork.JobCards.Get(id);
                entity.PrepareEntityForDeleting(deletedByUserId);

                // Mark deleted cards as cancelled too.
                entity.JobCardStatusId = (int)EnumJobCardStatus.Cancelled;

                UnitOfWork.JobCards.Update(entity);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

    }
}
