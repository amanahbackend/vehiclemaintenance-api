﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.DamageMemos;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.DamageMemos;

namespace VehicleMaintenance.WebApi.Controllers.DamageMemos
{
    [Route("api/DamageMemo")]
    [Produces("application/json")]
    public class DamageMemoController : BaseController
    {
        public DamageMemoController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        [HttpGet, Route("Get")]
        public ResponseResult<List<DamageMemoViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<DamageMemoViewModel>>();

            try
            {
                List<DamageMemo> lstDamageMemos = UnitOfWork.DamageMemos.Get();

                var lstDamageMemoVm = Mapper.Map<List<DamageMemoViewModel>>(lstDamageMemos);

                responseResult.Data = lstDamageMemoVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("Get/{id}")]
        public ResponseResult<DamageMemoViewModel> Get(int id)
        {
            var responseResult = new ResponseResult<DamageMemoViewModel>();

            try
            {
                DamageMemo damageMemo = UnitOfWork.DamageMemos.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                DamageMemoViewModel damageMemoVm = Mapper.Map<DamageMemoViewModel>(damageMemo);
                responseResult.Data = damageMemoVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<DamageMemoViewModel> Add([FromBody] DamageMemoViewModel model)
        {
            var responseResult = new ResponseResult<DamageMemoViewModel>();

            try
            {
                DamageMemo damageMemo = Mapper.Map<DamageMemo>(model);
                damageMemo.PrepareEntityForAdding();

                damageMemo.VehicleId = UnitOfWork.Vehicles.GetByVehicleFleetNo(model.VehicleFleetNo).Id;

                UnitOfWork.DamageMemos.Add(damageMemo);

                // Re-map the added model again to update Id.
                var damageMemoVm = Mapper.Map<DamageMemoViewModel>(damageMemo);
                responseResult.Data = damageMemoVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] DamageMemoViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                DamageMemo damageMemo = Mapper.Map<DamageMemo>(model);
                damageMemo.PrepareEntityForEditing(model);

                UnitOfWork.DamageMemos.Update(damageMemo);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                DamageMemo damageMemo = UnitOfWork.DamageMemos.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                damageMemo.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.DamageMemos.Update(damageMemo);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
