﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.Common.Vendors;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Common;

namespace VehicleMaintenance.WebApi.Controllers.Common
{
    [Route("api/Vendor")]
    [Produces("application/json")]
    public class VendorController : BaseController
    {
        public VendorController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        [HttpGet, Route("Get")]
        public ResponseResult<List<VendorViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<VendorViewModel>>();

            try
            {
                List<Vendor> lstVendors = UnitOfWork.Vendors.Find(obj => obj.RowStatusId != -1).ToList();
                var lstVendorVm = Mapper.Map<List<VendorViewModel>>(lstVendors);

                responseResult.Data = lstVendorVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<VendorViewModel> Add([FromBody] VendorViewModel model)
        {
            var responseResult = new ResponseResult<VendorViewModel>();

            try
            {
                Vendor vendor = Mapper.Map<Vendor>(model);
                vendor.PrepareEntityForAdding();

                UnitOfWork.Vendors.Add(vendor);

                // Re-map the added model again to update Id.
                var vendorVm = Mapper.Map<VendorViewModel>(vendor);
                responseResult.Data = vendorVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] VendorViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                Vendor vendor = Mapper.Map<Vendor>(model);
                vendor.PrepareEntityForEditing(model);

                UnitOfWork.Vendors.Update(vendor);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                Vendor vendor = UnitOfWork.Vendors.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                vendor.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.Vendors.Update(vendor);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
