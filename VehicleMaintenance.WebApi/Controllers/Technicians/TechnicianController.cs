﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.Technicians;

namespace VehicleMaintenance.WebApi.Controllers.Technicians
{
    [Route("api/Technician")]
    [Produces("application/json")]
    public class TechnicianController : BaseController
    {
        public TechnicianController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }


        [HttpGet, Route("Get")]
        public ResponseResult<List<TechnicianViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<TechnicianViewModel>>();

            try
            {
                List<Technician> lstTechnicians = UnitOfWork.Technicians.Find(
                    obj => obj.RowStatusId != (int)EnumRowStatus.Deleted).OrderBy(obj => obj.FirstName).ToList();

                var lstTechnicianVm = Mapper.Map<List<TechnicianViewModel>>(lstTechnicians);

                responseResult.Data = lstTechnicianVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("SearchByWord")]
        public ResponseResult<List<TechnicianViewModel>> SearchByWord(string keyword)
        {
            var responseResult = new ResponseResult<List<TechnicianViewModel>>();

            try
            {
                List<Technician> lstTechnicians = UnitOfWork.Technicians.SearchByWord(keyword);
                var lstTechnicianVm = Mapper.Map<List<TechnicianViewModel>>(lstTechnicians);

                responseResult.Data = lstTechnicianVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPost, Route("Add")]
        public ResponseResult<TechnicianViewModel> Add([FromBody] TechnicianViewModel model)
        {
            var responseResult = new ResponseResult<TechnicianViewModel>();

            try
            {
                Technician technician = Mapper.Map<Technician>(model);
                technician.PrepareEntityForAdding();

                UnitOfWork.Technicians.Add(technician);

                // Re-map the added model again to update Id.
                TechnicianViewModel technicianVm = Mapper.Map<TechnicianViewModel>(technician);
                responseResult.Data = technicianVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] TechnicianViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                Technician technician = Mapper.Map<Technician>(model);
                technician.PrepareEntityForEditing(model);

                UnitOfWork.Technicians.Update(technician);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                Technician technician = UnitOfWork.Technicians.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                technician.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.Technicians.Update(technician);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
