﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestSpareParts;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.RepairRequests;

namespace VehicleMaintenance.WebApi.Controllers.RepairRequests
{
    [Route("api/RepairRequestSparePart")]
    [Produces("application/json")]
    public class RepairRequestSparePartController : BaseController
    {
        public RepairRequestSparePartController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }


        [HttpGet, Route("GetRepairRequestSparePart/{id}")]
        public ResponseResult<RepairRequestSparePartViewModel> GetRepairRequestSparePart([FromRoute] int id)
        {
            var responseResult = new ResponseResult<RepairRequestSparePartViewModel>();

            try
            {
                RepairRequestSparePart repairRequestSparePart = UnitOfWork.RepairRequestSpareParts.GetRepairRequestSparePart(id);
                var repairRequestSparePartsVm = Mapper.Map<RepairRequestSparePartViewModel>(repairRequestSparePart);

                responseResult.Data = repairRequestSparePartsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetByRepairRequest/{repairRequestId}/{rowStatusId}")]
        public ResponseResult<List<RepairRequestSparePartViewModel>> GetByRepairRequest([FromRoute] int repairRequestId, int rowStatusId)
        {
            var responseResult = new ResponseResult<List<RepairRequestSparePartViewModel>>();

            try
            {
                List<RepairRequestSparePart> lstRepairRequestSpareParts = UnitOfWork.RepairRequestSpareParts.GetByRepairRequest(repairRequestId, rowStatusId);
                var lstRepairRequestSparePartsVm = Mapper.Map<List<RepairRequestSparePartViewModel>>(lstRepairRequestSpareParts);

                responseResult.Data = lstRepairRequestSparePartsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetPendingRepairRequestSpareParts")]
        public ResponseResult<List<RepairRequestSparePartViewModel>> GetPendingRepairRequestSpareParts()
        {
            var responseResult = new ResponseResult<List<RepairRequestSparePartViewModel>>();

            try
            {
                List<RepairRequestSparePart> lstRepairRequests = UnitOfWork.RepairRequestSpareParts.GetPendingRepairRequestSpareParts();
                var lstRepairRequestVm = Mapper.Map<List<RepairRequestSparePartViewModel>>(lstRepairRequests);

                responseResult.Data = lstRepairRequestVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("SearchRepairRequestSpareParts")]
        public ResponseResult<List<RepairRequestSparePartViewModel>> SearchRepairRequestSpareParts(
            [FromQuery] DateTime? startDate, DateTime? endDate, string vehiclePlateNo, string vehicleFleetNo,
            int? jobCardId, int? jobCardStatusId, int? sparePartId)
        {
            var responseResult = new ResponseResult<List<RepairRequestSparePartViewModel>>();

            try
            {
                List<RepairRequestSparePart> lstRepairRequestSpareParts =
                    UnitOfWork.RepairRequestSpareParts.Search(sparePartId, startDate, endDate,
                        vehiclePlateNo, vehicleFleetNo, jobCardId, jobCardStatusId);

                var lstRepairRequestSparePartsVm = Mapper.Map<List<RepairRequestSparePartViewModel>>(lstRepairRequestSpareParts);

                responseResult.Data = lstRepairRequestSparePartsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetSparePartsOfJobCard/{jobCardId}")]
        public ResponseResult<List<RepairRequestSparePartViewModel>> GetSparePartsOfJobCard([FromRoute] int jobCardId)
        {
            var responseResult = new ResponseResult<List<RepairRequestSparePartViewModel>>();

            try
            {
                List<RepairRequestSparePart> lstRepairRequestSpareParts = UnitOfWork.RepairRequestSpareParts.GetSparePartsOfJobCard(jobCardId);
                var lstRepairRequestSparePartsVm = Mapper.Map<List<RepairRequestSparePartViewModel>>(lstRepairRequestSpareParts);

                responseResult.Data = lstRepairRequestSparePartsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetUndeliveredSpareParts")]
        public ResponseResult<List<RepairRequestSparePartViewModel>> GetUndeliveredSpareParts(int? jobCardId, DateTime? startDate, DateTime? endDate)
        {
            var responseResult = new ResponseResult<List<RepairRequestSparePartViewModel>>();

            try
            {
                List<RepairRequestSparePart> lstRepairRequestSpareParts = UnitOfWork.RepairRequestSpareParts.GetUndeliveredSpareParts(jobCardId, startDate, endDate);
                var lstRepairRequestSparePartsVm = Mapper.Map<List<RepairRequestSparePartViewModel>>(lstRepairRequestSpareParts);

                responseResult.Data = lstRepairRequestSparePartsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpGet, Route("GetTechniciansWorkReport")]
        public ResponseResult<List<RepairRequestSparePartViewModel>> GetTechniciansWorkReport(int? technicianId, DateTime? startDate, DateTime? endDate)
        {
            var responseResult = new ResponseResult<List<RepairRequestSparePartViewModel>>();

            try
            {
                List<RepairRequestSparePart> lstRepairRequestSpareParts = UnitOfWork.RepairRequestSpareParts.GetTechniciansWorkReport(technicianId, startDate, endDate);

                var lstRepairRequestSparePartsVm = Mapper.Map<List<RepairRequestSparePartViewModel>>(lstRepairRequestSpareParts);

                responseResult.Data = lstRepairRequestSparePartsVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpPost, Route("Add")]
        public ResponseResult<RepairRequestSparePartViewModel> Add([FromBody] RepairRequestSparePartViewModel model)
        {
            var responseResult = new ResponseResult<RepairRequestSparePartViewModel>();

            try
            {
                RepairRequestSparePart repairRequestSparePart = Mapper.Map<RepairRequestSparePart>(model);
                repairRequestSparePart.PrepareEntityForAdding();
                repairRequestSparePart.DeliveredQuantity = 0;
                
                bool isSparePartNeedsApprove = UnitOfWork.SpareParts.Get(model.SparePartId.Value).NeedApprove ?? false;

                if (isSparePartNeedsApprove)
                {
                    repairRequestSparePart.RowStatusId = (int)EnumRowStatus.Inactive;
                }

                UnitOfWork.RepairRequestSpareParts.Add(repairRequestSparePart);

                // Remove auto mapped objects. 
                repairRequestSparePart.JobCard = null;
                repairRequestSparePart.RepairRequest = null;

                // Re-map the added model again to update Id.
                var repairRequestSparePartVm = Mapper.Map<RepairRequestSparePartViewModel>(repairRequestSparePart);
                responseResult.Data = repairRequestSparePartVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] RepairRequestSparePartViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                RepairRequestSparePart repairRequestSparePart = UnitOfWork.RepairRequestSpareParts.Get(model.Id);

                // Validate model.Quantity
                if (model.Quantity.HasValue)
                {
                    if (repairRequestSparePart.DeliveredQuantity == null ||
                        repairRequestSparePart.DeliveredQuantity <= model.Quantity)
                    {
                        repairRequestSparePart.Quantity = model.Quantity;
                    }
                    else
                    {
                        responseResult.StatusCode = -1;
                        responseResult.Message = "Delivered Quantity is more than new quantity.";

                        return responseResult;
                    }
                }

                // Set fields to be updated.
                repairRequestSparePart.PrepareEntityForEditing(model);

                if (model.Amount.HasValue)
                {
                    repairRequestSparePart.Amount = model.Amount;
                }

                if (!string.IsNullOrWhiteSpace(model.Source))
                {
                    repairRequestSparePart.Source = model.Source;
                }

                // Technician.
                if (model.TechnicianId.HasValue)
                {
                    repairRequestSparePart.TechnicianId = model.TechnicianId;
                }

                if (model.TechnicianStartDate.HasValue)
                {
                    repairRequestSparePart.TechnicianStartDate = model.TechnicianStartDate;
                }

                if (model.TechnicianEndDate.HasValue)
                {
                    repairRequestSparePart.TechnicianEndDate = model.TechnicianEndDate;
                }

                if (model.KiloMetersChangeSparePart.HasValue)
                {
                    repairRequestSparePart.KiloMetersChangeSparePart = model.KiloMetersChangeSparePart;
                }

                if (model.Comments != null)
                {
                    repairRequestSparePart.Comments = model.Comments;
                }

                // Note: repairRequestSparePart.SparePartId shouldn't be modified.
                // Because it requires sometimes an approval.

                UnitOfWork.RepairRequestSpareParts.Update(repairRequestSparePart);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }


        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                var entity = UnitOfWork.RepairRequestSpareParts.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                entity.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.RepairRequestSpareParts.Update(entity);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
