﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using VehicleMaintenance.DatabaseMgt;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestStatuses;
using VehicleMaintenance.WebApi.Models;
using VehicleMaintenance.WebApi.Models.RepairRequests;

namespace VehicleMaintenance.WebApi.Controllers.RepairRequests
{
    [Route("api/RepairRequestStatus")]
    [Produces("application/json")]
    public class RepairRequestStatusController : BaseController
    {
        public RepairRequestStatusController(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        [HttpGet, Route("Get")]
        public ResponseResult<List<RepairRequestStatusViewModel>> Get()
        {
            var responseResult = new ResponseResult<List<RepairRequestStatusViewModel>>();

            try
            {
                List<RepairRequestStatus> lstRepairRequestStatuses = UnitOfWork.RepairRequestStatuses.Find(obj => obj.RowStatusId != -1).ToList();
                var lstRepairRequestStatusesVm = Mapper.Map<List<RepairRequestStatusViewModel>>(lstRepairRequestStatuses);

                responseResult.Data = lstRepairRequestStatusesVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpPost, Route("Add")]
        public ResponseResult<RepairRequestStatusViewModel> Add([FromBody] RepairRequestStatusViewModel model)
        {
            var responseResult = new ResponseResult<RepairRequestStatusViewModel>();

            try
            {
                RepairRequestStatus repairRequestStatus = Mapper.Map<RepairRequestStatus>(model);
                repairRequestStatus.PrepareEntityForAdding();

                UnitOfWork.RepairRequestStatuses.Add(repairRequestStatus);

                // Re-map the added model again to update Id.
                var repairRequestStatusVm = Mapper.Map<RepairRequestStatusViewModel>(repairRequestStatus);
                responseResult.Data = repairRequestStatusVm;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpPut, Route("Update")]
        public ResponseResult<bool> Update([FromBody] RepairRequestStatusViewModel model)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                RepairRequestStatus repairRequestStatus = Mapper.Map<RepairRequestStatus>(model);
                repairRequestStatus.PrepareEntityForEditing(model);

                UnitOfWork.RepairRequestStatuses.Update(repairRequestStatus);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        [HttpDelete, Route("Delete/{id}/{deletedByUserId}")]
        public ResponseResult<bool> Delete(int id, string deletedByUserId)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                RepairRequestStatus repairRequestStatus = UnitOfWork.RepairRequestStatuses.GetSingleOrDefault(obj => obj.Id == id && obj.RowStatusId != -1);
                repairRequestStatus.PrepareEntityForDeleting(deletedByUserId);

                UnitOfWork.RepairRequestStatuses.Update(repairRequestStatus);

                responseResult.Data = true;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }
    }
}
