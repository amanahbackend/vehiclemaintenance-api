﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class DamageMemoReportTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DamageCost",
                table: "DamageMemo");

            migrationBuilder.DropColumn(
                name: "EmployeeSalary",
                table: "DamageMemo");

            migrationBuilder.DropColumn(
                name: "SalaryDeductionMonths",
                table: "DamageMemo");

            migrationBuilder.DropColumn(
                name: "SalaryDeductionRatio",
                table: "DamageMemo");

            migrationBuilder.DropColumn(
                name: "SalaryDeductionValue",
                table: "DamageMemo");

            migrationBuilder.AddColumn<string>(
                name: "ForemanName",
                table: "DamageMemo",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SparePart1Name",
                table: "DamageMemo",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SparePart2Name",
                table: "DamageMemo",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SparePart3Name",
                table: "DamageMemo",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ForemanName",
                table: "DamageMemo");

            migrationBuilder.DropColumn(
                name: "SparePart1Name",
                table: "DamageMemo");

            migrationBuilder.DropColumn(
                name: "SparePart2Name",
                table: "DamageMemo");

            migrationBuilder.DropColumn(
                name: "SparePart3Name",
                table: "DamageMemo");

            migrationBuilder.AddColumn<decimal>(
                name: "DamageCost",
                table: "DamageMemo",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "EmployeeSalary",
                table: "DamageMemo",
                type: "decimal(18, 2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SalaryDeductionMonths",
                table: "DamageMemo",
                type: "decimal(18, 2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SalaryDeductionRatio",
                table: "DamageMemo",
                type: "decimal(18, 2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SalaryDeductionValue",
                table: "DamageMemo",
                type: "decimal(18, 2)",
                nullable: true);
        }
    }
}
