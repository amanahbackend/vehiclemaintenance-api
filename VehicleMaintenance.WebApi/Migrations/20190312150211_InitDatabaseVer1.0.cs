﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VehicleMaintenance.WebApi.Migrations
{
    public partial class InitDatabaseVer10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 36, nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 36, nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Driver",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    CivilIdNo = table.Column<string>(maxLength: 20, nullable: true),
                    Title = table.Column<string>(maxLength: 50, nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: true),
                    Address = table.Column<string>(maxLength: 200, nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: true),
                    Ssn = table.Column<string>(maxLength: 50, nullable: true),
                    LicenseIssuedDate = table.Column<DateTime>(nullable: true),
                    LicenseIssuedState = table.Column<string>(maxLength: 50, nullable: true),
                    LicenseNumber = table.Column<string>(maxLength: 20, nullable: true),
                    Gender = table.Column<string>(maxLength: 10, nullable: true),
                    MaritalStatus = table.Column<string>(maxLength: 20, nullable: true),
                    EmployeeId = table.Column<string>(maxLength: 36, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Driver", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Driver_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Driver_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JobCardStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    NameAr = table.Column<string>(maxLength: 50, nullable: true),
                    NameEn = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobCardStatus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobCardStatus_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobCardStatus_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RepairRequestStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    NameAr = table.Column<string>(maxLength: 100, nullable: true),
                    NameEn = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairRequestStatus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RepairRequestStatus_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestStatus_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RepairRequestType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    NameAr = table.Column<string>(maxLength: 100, nullable: true),
                    NameEn = table.Column<string>(maxLength: 100, nullable: true),
                    NeedApprove = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairRequestType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RepairRequestType_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestType_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    NameAr = table.Column<string>(maxLength: 100, nullable: true),
                    NameEn = table.Column<string>(maxLength: 100, nullable: true),
                    Period = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceType_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceType_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Setting",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    Code = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Value = table.Column<string>(maxLength: 100, nullable: true),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    Category = table.Column<string>(maxLength: 50, nullable: true),
                    DataType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Setting", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Setting_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Setting_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Shift",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    StartTime = table.Column<TimeSpan>(nullable: true),
                    EndTime = table.Column<TimeSpan>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shift", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Shift_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Shift_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SparePart",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    SerialNumber = table.Column<string>(maxLength: 20, nullable: true),
                    NameAr = table.Column<string>(maxLength: 100, nullable: true),
                    NameEn = table.Column<string>(maxLength: 100, nullable: true),
                    NeedApprove = table.Column<bool>(nullable: true),
                    Category = table.Column<string>(maxLength: 50, nullable: true),
                    CaseAr = table.Column<string>(maxLength: 50, nullable: true),
                    CaseEn = table.Column<string>(maxLength: 50, nullable: true),
                    Model = table.Column<string>(maxLength: 50, nullable: true),
                    Make = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    Cost = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    Unit = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SparePart", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SparePart_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SparePart_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SparePartDeliveryOrder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    RequestedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SparePartDeliveryOrder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrder_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrder_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SparePartStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    NameAr = table.Column<string>(maxLength: 50, nullable: true),
                    NameEn = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SparePartStatus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SparePartStatus_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SparePartStatus_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Technician",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    CivilIdNo = table.Column<string>(maxLength: 20, nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    Address = table.Column<string>(maxLength: 200, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: true),
                    Specialty = table.Column<string>(maxLength: 50, nullable: true),
                    EmployeeId = table.Column<string>(maxLength: 36, nullable: true),
                    HourRating = table.Column<double>(nullable: true),
                    Available = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Technician", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Technician_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Technician_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleType_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleType_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vendor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Address = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vendor_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vendor_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vehicle",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    PlateNumber = table.Column<string>(maxLength: 20, nullable: true),
                    FleetNumber = table.Column<string>(maxLength: 20, nullable: true),
                    ChassisNumber = table.Column<string>(maxLength: 20, nullable: true),
                    CustomerName = table.Column<string>(maxLength: 50, nullable: true),
                    Year = table.Column<int>(nullable: true),
                    Make = table.Column<string>(maxLength: 50, nullable: true),
                    Model = table.Column<string>(maxLength: 50, nullable: true),
                    Color = table.Column<string>(maxLength: 20, nullable: true),
                    KiloMeters = table.Column<int>(nullable: true),
                    Vin = table.Column<string>(maxLength: 20, nullable: true),
                    VehicleTypeId = table.Column<int>(nullable: true),
                    Status = table.Column<string>(maxLength: 20, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Remarks = table.Column<string>(maxLength: 500, nullable: true),
                    Odometer = table.Column<double>(nullable: true),
                    PurchaseDate = table.Column<DateTime>(nullable: true),
                    FuelConsumption = table.Column<int>(nullable: true),
                    WorkIdleTime = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicle", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vehicle_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicle_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicle_VehicleType_VehicleTypeId",
                        column: x => x.VehicleTypeId,
                        principalTable: "VehicleType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cannibalization",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    FixingDate = table.Column<DateTime>(nullable: true),
                    VehicleFromId = table.Column<int>(nullable: true),
                    VehicleFromRegnNo = table.Column<string>(maxLength: 50, nullable: true),
                    TechnicianRemovedById = table.Column<int>(nullable: true),
                    VehicleToId = table.Column<int>(nullable: true),
                    VehicleToRegnNo = table.Column<string>(maxLength: 50, nullable: true),
                    TechnicianFixedById = table.Column<int>(nullable: true),
                    SupervisorComments = table.Column<string>(maxLength: 250, nullable: true),
                    RefixingDate = table.Column<DateTime>(nullable: true),
                    TechnicianRefixedById = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cannibalization", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cannibalization_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cannibalization_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cannibalization_Technician_TechnicianFixedById",
                        column: x => x.TechnicianFixedById,
                        principalTable: "Technician",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cannibalization_Technician_TechnicianRefixedById",
                        column: x => x.TechnicianRefixedById,
                        principalTable: "Technician",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cannibalization_Technician_TechnicianRemovedById",
                        column: x => x.TechnicianRemovedById,
                        principalTable: "Technician",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cannibalization_Vehicle_VehicleFromId",
                        column: x => x.VehicleFromId,
                        principalTable: "Vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cannibalization_Vehicle_VehicleToId",
                        column: x => x.VehicleToId,
                        principalTable: "Vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DamageMemo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    DamageMemoRef = table.Column<string>(nullable: true),
                    TechnicianId = table.Column<int>(nullable: true),
                    DriverId = table.Column<int>(nullable: true),
                    VehicleId = table.Column<int>(nullable: true),
                    DamageCost = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    EmployeeSalary = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    SalaryDeductionRatio = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    SalaryDeductionMonths = table.Column<decimal>(type: "decimal(18, 2)", nullable: true),
                    SalaryDeductionValue = table.Column<decimal>(type: "decimal(18, 2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DamageMemo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DamageMemo_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DamageMemo_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DamageMemo_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DamageMemo_Technician_TechnicianId",
                        column: x => x.TechnicianId,
                        principalTable: "Technician",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DamageMemo_Vehicle_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JobCard",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    DateIn = table.Column<DateTime>(nullable: true),
                    DateOut = table.Column<DateTime>(nullable: true),
                    DateInShiftId = table.Column<int>(nullable: true),
                    DateOutShiftId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    VehicleId = table.Column<int>(nullable: false),
                    DriverId = table.Column<int>(nullable: true),
                    Comment = table.Column<string>(maxLength: 200, nullable: true),
                    JobCardStatusId = table.Column<int>(nullable: true),
                    Odometer = table.Column<double>(nullable: true),
                    Hourmeter = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobCard", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobCard_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobCard_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobCard_JobCardStatus_JobCardStatusId",
                        column: x => x.JobCardStatusId,
                        principalTable: "JobCardStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobCard_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobCard_Vehicle_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CannibalizationSparePart",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    CannibalizationId = table.Column<int>(nullable: true),
                    SparePartId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CannibalizationSparePart", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CannibalizationSparePart_Cannibalization_CannibalizationId",
                        column: x => x.CannibalizationId,
                        principalTable: "Cannibalization",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CannibalizationSparePart_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CannibalizationSparePart_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CannibalizationSparePart_SparePart_SparePartId",
                        column: x => x.SparePartId,
                        principalTable: "SparePart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RepairRequest",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    JobCardId = table.Column<int>(nullable: true),
                    RepairRequestTypeId = table.Column<int>(nullable: true),
                    RepairRequestStatusId = table.Column<int>(nullable: true),
                    ServiceTypeId = table.Column<int>(nullable: true),
                    Duration = table.Column<double>(nullable: false),
                    Cost = table.Column<double>(nullable: false),
                    External = table.Column<bool>(nullable: true),
                    Comment = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairRequest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RepairRequest_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequest_JobCard_JobCardId",
                        column: x => x.JobCardId,
                        principalTable: "JobCard",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequest_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequest_RepairRequestStatus_RepairRequestStatusId",
                        column: x => x.RepairRequestStatusId,
                        principalTable: "RepairRequestStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequest_RepairRequestType_RepairRequestTypeId",
                        column: x => x.RepairRequestTypeId,
                        principalTable: "RepairRequestType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequest_ServiceType_ServiceTypeId",
                        column: x => x.ServiceTypeId,
                        principalTable: "ServiceType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RepairRequestMaterial",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    RepairRequestId = table.Column<int>(nullable: true),
                    SparePartId = table.Column<int>(nullable: true),
                    SparePartStatusId = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: true),
                    Cost = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    TechnicianId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairRequestMaterial", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RepairRequestMaterial_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestMaterial_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestMaterial_RepairRequest_RepairRequestId",
                        column: x => x.RepairRequestId,
                        principalTable: "RepairRequest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestMaterial_SparePart_SparePartId",
                        column: x => x.SparePartId,
                        principalTable: "SparePart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestMaterial_SparePartStatus_SparePartStatusId",
                        column: x => x.SparePartStatusId,
                        principalTable: "SparePartStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestMaterial_Technician_TechnicianId",
                        column: x => x.TechnicianId,
                        principalTable: "Technician",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RepairRequestSparePart",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    RepairRequestId = table.Column<int>(nullable: true),
                    JobCardId = table.Column<int>(nullable: true),
                    VehicleId = table.Column<int>(nullable: true),
                    SparePartId = table.Column<int>(nullable: true),
                    DeliveredQuantity = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    Source = table.Column<string>(maxLength: 100, nullable: true),
                    KiloMetersChangeSparePart = table.Column<int>(nullable: true),
                    TechnicianId = table.Column<int>(nullable: true),
                    TechnicianStartDate = table.Column<DateTime>(nullable: true),
                    TechnicianEndDate = table.Column<DateTime>(nullable: true),
                    Comments = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairRequestSparePart", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RepairRequestSparePart_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestSparePart_JobCard_JobCardId",
                        column: x => x.JobCardId,
                        principalTable: "JobCard",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestSparePart_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestSparePart_RepairRequest_RepairRequestId",
                        column: x => x.RepairRequestId,
                        principalTable: "RepairRequest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestSparePart_SparePart_SparePartId",
                        column: x => x.SparePartId,
                        principalTable: "SparePart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestSparePart_Technician_TechnicianId",
                        column: x => x.TechnicianId,
                        principalTable: "Technician",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestSparePart_Vehicle_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RepairRequestVendor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    RepairRequestId = table.Column<int>(nullable: false),
                    VendorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairRequestVendor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RepairRequestVendor_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestVendor_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairRequestVendor_RepairRequest_RepairRequestId",
                        column: x => x.RepairRequestId,
                        principalTable: "RepairRequest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RepairRequestVendor_Vendor_VendorId",
                        column: x => x.VendorId,
                        principalTable: "Vendor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SparePartDeliveryOrderDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    SparePartDeliveryOrderId = table.Column<int>(nullable: true),
                    RepairRequestSparePartId = table.Column<int>(nullable: true),
                    SparePartId = table.Column<int>(nullable: true),
                    DeliveredQty = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SparePartDeliveryOrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrderDetails_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrderDetails_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrderDetails_RepairRequestSparePart_RepairRequestSparePartId",
                        column: x => x.RepairRequestSparePartId,
                        principalTable: "RepairRequestSparePart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrderDetails_SparePartDeliveryOrder_SparePartDeliveryOrderId",
                        column: x => x.SparePartDeliveryOrderId,
                        principalTable: "SparePartDeliveryOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SparePartDeliveryOrderDetails_SparePart_SparePartId",
                        column: x => x.SparePartId,
                        principalTable: "SparePart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UploadedFile",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowStatusId = table.Column<int>(nullable: true),
                    CreatedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ModifiedByUserId = table.Column<string>(maxLength: 36, nullable: true),
                    ModificationDate = table.Column<DateTime>(nullable: true),
                    FileName = table.Column<string>(maxLength: 50, nullable: true),
                    FileRelativePath = table.Column<string>(maxLength: 100, nullable: true),
                    FileType = table.Column<string>(maxLength: 20, nullable: true),
                    IsDefault = table.Column<bool>(nullable: true),
                    JobCardId = table.Column<int>(nullable: true),
                    RepairRequestVendorId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UploadedFile", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UploadedFile_AspNetUsers_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UploadedFile_JobCard_JobCardId",
                        column: x => x.JobCardId,
                        principalTable: "JobCard",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UploadedFile_AspNetUsers_ModifiedByUserId",
                        column: x => x.ModifiedByUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UploadedFile_RepairRequestVendor_RepairRequestVendorId",
                        column: x => x.RepairRequestVendorId,
                        principalTable: "RepairRequestVendor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Cannibalization_CreatedByUserId",
                table: "Cannibalization",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Cannibalization_ModifiedByUserId",
                table: "Cannibalization",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Cannibalization_TechnicianFixedById",
                table: "Cannibalization",
                column: "TechnicianFixedById");

            migrationBuilder.CreateIndex(
                name: "IX_Cannibalization_TechnicianRefixedById",
                table: "Cannibalization",
                column: "TechnicianRefixedById");

            migrationBuilder.CreateIndex(
                name: "IX_Cannibalization_TechnicianRemovedById",
                table: "Cannibalization",
                column: "TechnicianRemovedById");

            migrationBuilder.CreateIndex(
                name: "IX_Cannibalization_VehicleFromId",
                table: "Cannibalization",
                column: "VehicleFromId");

            migrationBuilder.CreateIndex(
                name: "IX_Cannibalization_VehicleToId",
                table: "Cannibalization",
                column: "VehicleToId");

            migrationBuilder.CreateIndex(
                name: "IX_CannibalizationSparePart_CannibalizationId",
                table: "CannibalizationSparePart",
                column: "CannibalizationId");

            migrationBuilder.CreateIndex(
                name: "IX_CannibalizationSparePart_CreatedByUserId",
                table: "CannibalizationSparePart",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_CannibalizationSparePart_ModifiedByUserId",
                table: "CannibalizationSparePart",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_CannibalizationSparePart_SparePartId",
                table: "CannibalizationSparePart",
                column: "SparePartId");

            migrationBuilder.CreateIndex(
                name: "IX_DamageMemo_CreatedByUserId",
                table: "DamageMemo",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_DamageMemo_DriverId",
                table: "DamageMemo",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_DamageMemo_ModifiedByUserId",
                table: "DamageMemo",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_DamageMemo_TechnicianId",
                table: "DamageMemo",
                column: "TechnicianId");

            migrationBuilder.CreateIndex(
                name: "IX_DamageMemo_VehicleId",
                table: "DamageMemo",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_CreatedByUserId",
                table: "Driver",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_ModifiedByUserId",
                table: "Driver",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCard_CreatedByUserId",
                table: "JobCard",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCard_DriverId",
                table: "JobCard",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCard_JobCardStatusId",
                table: "JobCard",
                column: "JobCardStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCard_ModifiedByUserId",
                table: "JobCard",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCard_VehicleId",
                table: "JobCard",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCardStatus_CreatedByUserId",
                table: "JobCardStatus",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCardStatus_ModifiedByUserId",
                table: "JobCardStatus",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequest_CreatedByUserId",
                table: "RepairRequest",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequest_JobCardId",
                table: "RepairRequest",
                column: "JobCardId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequest_ModifiedByUserId",
                table: "RepairRequest",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequest_RepairRequestStatusId",
                table: "RepairRequest",
                column: "RepairRequestStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequest_RepairRequestTypeId",
                table: "RepairRequest",
                column: "RepairRequestTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequest_ServiceTypeId",
                table: "RepairRequest",
                column: "ServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_CreatedByUserId",
                table: "RepairRequestMaterial",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_ModifiedByUserId",
                table: "RepairRequestMaterial",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_RepairRequestId",
                table: "RepairRequestMaterial",
                column: "RepairRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_SparePartId",
                table: "RepairRequestMaterial",
                column: "SparePartId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_SparePartStatusId",
                table: "RepairRequestMaterial",
                column: "SparePartStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestMaterial_TechnicianId",
                table: "RepairRequestMaterial",
                column: "TechnicianId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestSparePart_CreatedByUserId",
                table: "RepairRequestSparePart",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestSparePart_JobCardId",
                table: "RepairRequestSparePart",
                column: "JobCardId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestSparePart_ModifiedByUserId",
                table: "RepairRequestSparePart",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestSparePart_RepairRequestId",
                table: "RepairRequestSparePart",
                column: "RepairRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestSparePart_SparePartId",
                table: "RepairRequestSparePart",
                column: "SparePartId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestSparePart_TechnicianId",
                table: "RepairRequestSparePart",
                column: "TechnicianId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestSparePart_VehicleId",
                table: "RepairRequestSparePart",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestStatus_CreatedByUserId",
                table: "RepairRequestStatus",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestStatus_ModifiedByUserId",
                table: "RepairRequestStatus",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestType_CreatedByUserId",
                table: "RepairRequestType",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestType_ModifiedByUserId",
                table: "RepairRequestType",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestVendor_CreatedByUserId",
                table: "RepairRequestVendor",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestVendor_ModifiedByUserId",
                table: "RepairRequestVendor",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestVendor_RepairRequestId",
                table: "RepairRequestVendor",
                column: "RepairRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairRequestVendor_VendorId",
                table: "RepairRequestVendor",
                column: "VendorId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceType_CreatedByUserId",
                table: "ServiceType",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceType_ModifiedByUserId",
                table: "ServiceType",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Setting_CreatedByUserId",
                table: "Setting",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Setting_ModifiedByUserId",
                table: "Setting",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Shift_CreatedByUserId",
                table: "Shift",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Shift_ModifiedByUserId",
                table: "Shift",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePart_CreatedByUserId",
                table: "SparePart",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePart_ModifiedByUserId",
                table: "SparePart",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrder_CreatedByUserId",
                table: "SparePartDeliveryOrder",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrder_ModifiedByUserId",
                table: "SparePartDeliveryOrder",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrderDetails_CreatedByUserId",
                table: "SparePartDeliveryOrderDetails",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrderDetails_ModifiedByUserId",
                table: "SparePartDeliveryOrderDetails",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrderDetails_RepairRequestSparePartId",
                table: "SparePartDeliveryOrderDetails",
                column: "RepairRequestSparePartId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrderDetails_SparePartDeliveryOrderId",
                table: "SparePartDeliveryOrderDetails",
                column: "SparePartDeliveryOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartDeliveryOrderDetails_SparePartId",
                table: "SparePartDeliveryOrderDetails",
                column: "SparePartId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartStatus_CreatedByUserId",
                table: "SparePartStatus",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SparePartStatus_ModifiedByUserId",
                table: "SparePartStatus",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Technician_CreatedByUserId",
                table: "Technician",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Technician_ModifiedByUserId",
                table: "Technician",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFile_CreatedByUserId",
                table: "UploadedFile",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFile_JobCardId",
                table: "UploadedFile",
                column: "JobCardId");

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFile_ModifiedByUserId",
                table: "UploadedFile",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFile_RepairRequestVendorId",
                table: "UploadedFile",
                column: "RepairRequestVendorId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_CreatedByUserId",
                table: "Vehicle",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_ModifiedByUserId",
                table: "Vehicle",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_VehicleTypeId",
                table: "Vehicle",
                column: "VehicleTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleType_CreatedByUserId",
                table: "VehicleType",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleType_ModifiedByUserId",
                table: "VehicleType",
                column: "ModifiedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendor_CreatedByUserId",
                table: "Vendor",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendor_ModifiedByUserId",
                table: "Vendor",
                column: "ModifiedByUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CannibalizationSparePart");

            migrationBuilder.DropTable(
                name: "DamageMemo");

            migrationBuilder.DropTable(
                name: "RepairRequestMaterial");

            migrationBuilder.DropTable(
                name: "Setting");

            migrationBuilder.DropTable(
                name: "Shift");

            migrationBuilder.DropTable(
                name: "SparePartDeliveryOrderDetails");

            migrationBuilder.DropTable(
                name: "UploadedFile");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Cannibalization");

            migrationBuilder.DropTable(
                name: "SparePartStatus");

            migrationBuilder.DropTable(
                name: "RepairRequestSparePart");

            migrationBuilder.DropTable(
                name: "SparePartDeliveryOrder");

            migrationBuilder.DropTable(
                name: "RepairRequestVendor");

            migrationBuilder.DropTable(
                name: "SparePart");

            migrationBuilder.DropTable(
                name: "Technician");

            migrationBuilder.DropTable(
                name: "RepairRequest");

            migrationBuilder.DropTable(
                name: "Vendor");

            migrationBuilder.DropTable(
                name: "JobCard");

            migrationBuilder.DropTable(
                name: "RepairRequestStatus");

            migrationBuilder.DropTable(
                name: "RepairRequestType");

            migrationBuilder.DropTable(
                name: "ServiceType");

            migrationBuilder.DropTable(
                name: "Driver");

            migrationBuilder.DropTable(
                name: "JobCardStatus");

            migrationBuilder.DropTable(
                name: "Vehicle");

            migrationBuilder.DropTable(
                name: "VehicleType");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
