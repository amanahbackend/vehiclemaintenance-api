﻿using AutoMapper;
using VehicleMaintenance.DatabaseMgt.Common.Shifts;
using VehicleMaintenance.DatabaseMgt.Common.Vendors;
using VehicleMaintenance.DatabaseMgt.DamageMemos;
using VehicleMaintenance.DatabaseMgt.Drivers;
using VehicleMaintenance.DatabaseMgt.JobCards;
using VehicleMaintenance.DatabaseMgt.JobCards.JobCardStatuses;
using VehicleMaintenance.DatabaseMgt.RepairRequests;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestMaterials;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestSpareParts;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestStatuses;
using VehicleMaintenance.DatabaseMgt.RepairRequests.RepairRequestTypes;
using VehicleMaintenance.DatabaseMgt.Security.Roles;
using VehicleMaintenance.DatabaseMgt.Security.Users;
using VehicleMaintenance.DatabaseMgt.ServiceTypes;
using VehicleMaintenance.DatabaseMgt.SpareParts;
using VehicleMaintenance.DatabaseMgt.SpareParts.SparePartStatuses;
using VehicleMaintenance.DatabaseMgt.Technicians;
using VehicleMaintenance.DatabaseMgt.Vehicles;
using VehicleMaintenance.DatabaseMgt.Vehicles.Cannibalizations;
using VehicleMaintenance.DatabaseMgt.Vehicles.VehicleTypes;
using VehicleMaintenance.WebApi.Models.Common;
using VehicleMaintenance.WebApi.Models.DamageMemos;
using VehicleMaintenance.WebApi.Models.Drivers;
using VehicleMaintenance.WebApi.Models.JobCards;
using VehicleMaintenance.WebApi.Models.RepairRequests;
using VehicleMaintenance.WebApi.Models.Security;
using VehicleMaintenance.WebApi.Models.ServiceTypes;
using VehicleMaintenance.WebApi.Models.SpareParts;
using VehicleMaintenance.WebApi.Models.Technicians;
using VehicleMaintenance.WebApi.Models.Vehicles;

namespace VehicleMaintenance.WebApi.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ApplicationUser, UserViewModel>()
                .ForMember(dest => dest.Password, opt => opt.Ignore());

            CreateMap<UserViewModel, ApplicationUser>()
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore())
                .ForMember(dest => dest.EmailConfirmed, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumberConfirmed, opt => opt.Ignore())
                //.ForMember(dest => dest.PhoneNumber, opt => opt.Ignore())
                .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore());

            CreateMap<RoleViewModel, ApplicationRole>();

            CreateMap<DriverViewModel, Driver>();

            CreateMap<VehicleViewModel, Vehicle>();
            CreateMap<VehicleTypeViewModel, VehicleType>();

            CreateMap<JobCardViewModel, JobCard>();
            CreateMap<JobCardStatusViewModel, JobCardStatus>();

            CreateMap<RepairRequestViewModel, RepairRequest>();
            CreateMap<RepairRequestTypeViewModel, RepairRequestType>();
            CreateMap<RepairRequestStatusViewModel, RepairRequestStatus>();
            CreateMap<RepairRequestSparePartViewModel, RepairRequestSparePart>();
            CreateMap<RepairRequestMaterialViewModel, RepairRequestMaterial>();

            CreateMap<TechnicianViewModel, Technician>();

            CreateMap<ServiceTypeViewModel, ServiceType>();

            CreateMap<SparePartViewModel, SparePart>();
            CreateMap<SparePartStatusViewModel, SparePartStatus>();

            CreateMap<ShiftViewModel, Shift>();

            CreateMap<VendorViewModel, Vendor>();

            CreateMap<Cannibalization, CannibalizationViewModel>()
                .ForMember(dest => dest.VehicleFromId, opt => opt.Ignore())
                .ForMember(dest => dest.VehicleToId, opt => opt.Ignore());

            CreateMap<DamageMemo, DamageMemoViewModel>()
                .ForMember(dest => dest.VehicleFleetNo, opt => opt.Ignore());
        }
    }
}
