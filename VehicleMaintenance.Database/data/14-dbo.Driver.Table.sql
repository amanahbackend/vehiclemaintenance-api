﻿SET IDENTITY_INSERT [dbo].[Driver] ON 
GO
INSERT [dbo].[Driver] ([Id], [CivilIdNo], [Title], [FirstName], [LastName], [PhoneNumber], [Address], [BirthDate], [Ssn], [LicenseIssuedDate], [LicenseIssuedState], [LicenseNumber], [Gender], [MaritalStatus], [EmployeeId], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (1, N'', N'Mr.', N'MANSOUR AHMED', N'ALI  ISMAIEL', N'', N'Kuiwait', CAST(N'1990-01-01T00:00:00.0000000' AS DateTime2), N'', NULL, N'Kuiwait', N'', N'Male', N'Married', N'2195', 1, NULL, CAST(N'2019-04-09T10:36:51.8700000' AS DateTime2), NULL, NULL)
GO
INSERT [dbo].[Driver] ([Id], [CivilIdNo], [Title], [FirstName], [LastName], [PhoneNumber], [Address], [BirthDate], [Ssn], [LicenseIssuedDate], [LicenseIssuedState], [LicenseNumber], [Gender], [MaritalStatus], [EmployeeId], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (2, N'', N'Mr.', N'ABDELATY AHMED', N'AHMED HUSSEIN', N'', N'Kuiwait', CAST(N'1990-01-01T00:00:00.0000000' AS DateTime2), N'', NULL, N'Kuiwait', N'', N'Male', N'Married', N'2395', 1, NULL, CAST(N'2019-04-09T10:36:51.8700000' AS DateTime2), NULL, NULL)
GO
INSERT [dbo].[Driver] ([Id], [CivilIdNo], [Title], [FirstName], [LastName], [PhoneNumber], [Address], [BirthDate], [Ssn], [LicenseIssuedDate], [LicenseIssuedState], [LicenseNumber], [Gender], [MaritalStatus], [EmployeeId], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (3, N'', N'Mr.', N'WALID ATEF', N'ABDELSABOUR', N'', N'Kuiwait', CAST(N'1990-01-01T00:00:00.0000000' AS DateTime2), N'', NULL, N'Kuiwait', N'', N'Male', N'Married', N'2386', 1, NULL, CAST(N'2019-04-09T10:36:51.8700000' AS DateTime2), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Driver] OFF
