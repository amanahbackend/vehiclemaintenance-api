SET IDENTITY_INSERT [dbo].[VehicleType] ON 
GO
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (1, N'Transit Mixer', 1, NULL, CAST(N'2018-12-24T13:59:02.0433134' AS DateTime2), NULL, NULL)
GO
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (2, N'STP', 1, NULL, CAST(N'2018-12-24T13:59:02.0433134' AS DateTime2), NULL, NULL)
GO
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (3, N'Concrete Pump', 1, NULL, CAST(N'2018-12-24T13:59:02.0433134' AS DateTime2), NULL, NULL)
GO
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (4, N'Silo', 1, NULL, CAST(N'2018-12-30T13:56:49.5862634' AS DateTime2), NULL, CAST(N'2018-12-30T14:05:02.3775124' AS DateTime2))
GO
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (5, N'Trailers', 1, NULL, CAST(N'2018-12-30T14:06:06.3217963' AS DateTime2), NULL, CAST(N'2018-12-30T14:09:49.4541900' AS DateTime2))
GO
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (6, N'Loader', 1, NULL, CAST(N'2019-04-08T00:00:00.0000000' AS DateTime2), NULL, NULL)
GO
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (7, N'Generator', 1, NULL, CAST(N'2019-04-08T00:00:00.0000000' AS DateTime2), NULL, NULL)
GO
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (8, N'Light Vehicles', 1, NULL, CAST(N'2019-04-08T00:00:00.0000000' AS DateTime2), NULL, NULL)
GO
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (9, N'BOB CAT', 1, NULL, CAST(N'2019-04-08T00:00:00.0000000' AS DateTime2), NULL, NULL)
GO
INSERT [dbo].[VehicleType] ([Id], [Name], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (10, N'Other', 1, NULL, CAST(N'2019-04-08T00:00:00.0000000' AS DateTime2), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[VehicleType] OFF
