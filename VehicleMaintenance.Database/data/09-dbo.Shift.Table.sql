SET IDENTITY_INSERT [dbo].[Shift] ON 

INSERT [dbo].[Shift] ([Id], [Name], [StartTime], [EndTime], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (1, N'1st Shift', CAST(N'07:00:00' AS Time), CAST(N'14:59:00' AS Time), 1, NULL, CAST(N'2018-12-24T13:55:51.5614811' AS DateTime2), NULL, NULL)
INSERT [dbo].[Shift] ([Id], [Name], [StartTime], [EndTime], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (2, N'2nd Shift', CAST(N'15:00:00' AS Time), CAST(N'22:59:00' AS Time), 1, NULL, CAST(N'2018-12-24T13:55:51.5614811' AS DateTime2), NULL, NULL)
INSERT [dbo].[Shift] ([Id], [Name], [StartTime], [EndTime], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (3, N'3rd Shift', CAST(N'23:00:00' AS Time), CAST(N'06:59:00' AS Time), 1, NULL, CAST(N'2018-12-24T13:55:51.5614811' AS DateTime2), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Shift] OFF
