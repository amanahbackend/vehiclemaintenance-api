SET IDENTITY_INSERT [dbo].[SparePartStatus] ON 

INSERT [dbo].[SparePartStatus] ([Id], [NameAr], [NameEn], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (1, N'ذاتى', N'Internal', 1, NULL, CAST(N'2019-01-03T11:20:08.5396706' AS DateTime2), NULL, NULL)
INSERT [dbo].[SparePartStatus] ([Id], [NameAr], [NameEn], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (2, N'شراء', N'Purchase', 1, NULL, CAST(N'2019-01-03T11:20:08.5396706' AS DateTime2), NULL, NULL)
INSERT [dbo].[SparePartStatus] ([Id], [NameAr], [NameEn], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (3, N'من المخزن ', N'From Inventory', 1, NULL, CAST(N'2019-01-03T11:20:08.5396706' AS DateTime2), NULL, NULL)
INSERT [dbo].[SparePartStatus] ([Id], [NameAr], [NameEn], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (4, N'تبديل أجزاء', N'Used Transfer', 1, NULL, CAST(N'2019-01-03T11:20:08.5396706' AS DateTime2), NULL, NULL)
SET IDENTITY_INSERT [dbo].[SparePartStatus] OFF
