SET IDENTITY_INSERT [dbo].[JobCardStatus] ON 

INSERT [dbo].[JobCardStatus] ([Id], [NameAr], [NameEn], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (2, N'قيد التنفيذ', N'In progress', 1, NULL, CAST(N'2018-12-24T14:49:57.4528946' AS DateTime2), NULL, NULL)
INSERT [dbo].[JobCardStatus] ([Id], [NameAr], [NameEn], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (3, N'أغلقت', N'Closed', 1, NULL, CAST(N'2018-12-24T14:49:57.4528946' AS DateTime2), NULL, NULL)
INSERT [dbo].[JobCardStatus] ([Id], [NameAr], [NameEn], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (4, N'ألغيت', N'Cancelled', 1, NULL, CAST(N'2018-12-24T14:49:57.4528946' AS DateTime2), NULL, NULL)
SET IDENTITY_INSERT [dbo].[JobCardStatus] OFF
