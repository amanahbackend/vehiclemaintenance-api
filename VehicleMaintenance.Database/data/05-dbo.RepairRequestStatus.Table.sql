SET IDENTITY_INSERT [dbo].[RepairRequestStatus] ON 

INSERT [dbo].[RepairRequestStatus] ([Id], [NameAr], [NameEn], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (1, N'يحتاج إلى الموافقة', N'Need Approval', 1, NULL, CAST(N'2018-12-24T14:52:14.2615967' AS DateTime2), NULL, NULL)
INSERT [dbo].[RepairRequestStatus] ([Id], [NameAr], [NameEn], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (2, N'تحديد الاسعار ', N'Under Quotation', 1, NULL, CAST(N'2018-12-24T14:52:14.2615967' AS DateTime2), NULL, NULL)
INSERT [dbo].[RepairRequestStatus] ([Id], [NameAr], [NameEn], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (4, N'إغلاق', N'Closed', 1, NULL, CAST(N'2018-12-24T14:52:14.2615967' AS DateTime2), NULL, NULL)
INSERT [dbo].[RepairRequestStatus] ([Id], [NameAr], [NameEn], [RowStatusId], [CreatedByUserId], [CreationDate], [ModifiedByUserId], [ModificationDate]) VALUES (5, N'قيد التنفيذ', N'In Progress', 1, NULL, CAST(N'2018-12-24T14:52:14.2615967' AS DateTime2), NULL, NULL)
SET IDENTITY_INSERT [dbo].[RepairRequestStatus] OFF
