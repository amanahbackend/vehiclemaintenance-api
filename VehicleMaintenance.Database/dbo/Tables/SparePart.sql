﻿CREATE TABLE [dbo].[SparePart] (
    [Id]               INT             IDENTITY (1, 1) NOT NULL,
    [SerialNumber]     NVARCHAR (20)   NULL,
    [NameEn]           NVARCHAR (100)  NULL,
    [NameAr]           NVARCHAR (100)  NULL,
    [Category]         NVARCHAR (50)   NULL,
    [NeedApprove]      BIT             NULL,
    [CaseAr]           NVARCHAR (50)   NULL,
    [CaseEn]           NVARCHAR (50)   NULL,
    [Model]            NVARCHAR (50)   NULL,
    [Make]             NVARCHAR (50)   NULL,
    [Description]      NVARCHAR (200)  NULL,
    [Cost]             DECIMAL (18, 4) NULL,
    [Unit]             NVARCHAR (20)   NULL,
    [RowStatusId]      INT             NULL,
    [CreatedByUserId]  NVARCHAR (36)   NULL,
    [CreationDate]     DATETIME2 (7)   NULL,
    [ModifiedByUserId] NVARCHAR (36)   NULL,
    [ModificationDate] DATETIME2 (7)   NULL,
    CONSTRAINT [PK_SparePart] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SparePart_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_SparePart_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);








GO



GO


