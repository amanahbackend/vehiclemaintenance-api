﻿CREATE TABLE [dbo].[RepairRequestMaterial] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [RepairRequestId]   INT             NULL,
    [SparePartId]       INT             NULL,
    [SparePartStatusId] INT             NULL,
    [Quantity]          INT             NULL,
    [Cost]              DECIMAL (18, 4) NULL,
    [TechnicianId]      INT             NULL,
    [RowStatusId]       INT             NULL,
    [CreatedByUserId]   NVARCHAR (36)   NULL,
    [CreationDate]      DATETIME2 (7)   NULL,
    [ModifiedByUserId]  NVARCHAR (36)   NULL,
    [ModificationDate]  DATETIME2 (7)   NULL,
    CONSTRAINT [PK_RepairRequestMaterial] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RepairRequestMaterial_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_RepairRequestMaterial_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_RepairRequestMaterial_RepairRequest_RepairRequestId] FOREIGN KEY ([RepairRequestId]) REFERENCES [dbo].[RepairRequest] ([Id]),
    CONSTRAINT [FK_RepairRequestMaterial_SparePart_SparePartId] FOREIGN KEY ([SparePartId]) REFERENCES [dbo].[SparePart] ([Id]),
    CONSTRAINT [FK_RepairRequestMaterial_SparePartStatus_SparePartStatusId] FOREIGN KEY ([SparePartStatusId]) REFERENCES [dbo].[SparePartStatus] ([Id]),
    CONSTRAINT [FK_RepairRequestMaterial_Technician_TechnicianId] FOREIGN KEY ([TechnicianId]) REFERENCES [dbo].[Technician] ([Id])
);








GO



GO



GO



GO



GO



GO



GO


