﻿CREATE TABLE [dbo].[Cannibalization] (
    [Id]                    INT            IDENTITY (1, 1) NOT NULL,
    [FixingDate]            DATETIME2 (7)  NULL,
    [VehicleFromId]         INT            NULL,
    [VehicleFromRegnNo]     NVARCHAR (50)  NULL,
    [TechnicianRemovedById] INT            NULL,
    [VehicleToId]           INT            NULL,
    [VehicleToRegnNo]       NVARCHAR (50)  NULL,
    [TechnicianFixedById]   INT            NULL,
    [SupervisorComments]    NVARCHAR (250) NULL,
    [RefixingDate]          DATETIME2 (7)  NULL,
    [TechnicianRefixedById] INT            NULL,
    [RowStatusId]           INT            NULL,
    [CreatedByUserId]       NVARCHAR (36)  NULL,
    [CreationDate]          DATETIME2 (7)  NULL,
    [ModifiedByUserId]      NVARCHAR (36)  NULL,
    [ModificationDate]      DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Cannibalization] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Cannibalization_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Cannibalization_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Cannibalization_Technician_TechnicianFixedById] FOREIGN KEY ([TechnicianFixedById]) REFERENCES [dbo].[Technician] ([Id]),
    CONSTRAINT [FK_Cannibalization_Technician_TechnicianRefixedById] FOREIGN KEY ([TechnicianRefixedById]) REFERENCES [dbo].[Technician] ([Id]),
    CONSTRAINT [FK_Cannibalization_Technician_TechnicianRemovedById] FOREIGN KEY ([TechnicianRemovedById]) REFERENCES [dbo].[Technician] ([Id]),
    CONSTRAINT [FK_Cannibalization_Vehicle_VehicleFromId] FOREIGN KEY ([VehicleFromId]) REFERENCES [dbo].[Vehicle] ([Id]),
    CONSTRAINT [FK_Cannibalization_Vehicle_VehicleToId] FOREIGN KEY ([VehicleToId]) REFERENCES [dbo].[Vehicle] ([Id])
);




GO



GO



GO



GO



GO



GO



GO


