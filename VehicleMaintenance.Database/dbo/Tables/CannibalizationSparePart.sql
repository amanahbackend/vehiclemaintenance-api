﻿CREATE TABLE [dbo].[CannibalizationSparePart] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [CannibalizationId] INT           NULL,
    [SparePartId]       INT           NULL,
    [RowStatusId]       INT           NULL,
    [CreatedByUserId]   NVARCHAR (36) NULL,
    [CreationDate]      DATETIME2 (7) NULL,
    [ModifiedByUserId]  NVARCHAR (36) NULL,
    [ModificationDate]  DATETIME2 (7) NULL,
    CONSTRAINT [PK_CannibalizationSparePart] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CannibalizationSparePart_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_CannibalizationSparePart_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_CannibalizationSparePart_Cannibalization_CannibalizationId] FOREIGN KEY ([CannibalizationId]) REFERENCES [dbo].[Cannibalization] ([Id]),
    CONSTRAINT [FK_CannibalizationSparePart_SparePart_SparePartId] FOREIGN KEY ([SparePartId]) REFERENCES [dbo].[SparePart] ([Id])
);




GO



GO



GO



GO


