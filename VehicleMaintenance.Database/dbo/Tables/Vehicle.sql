﻿CREATE TABLE [dbo].[Vehicle] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [PlateNumber]      NVARCHAR (20)  NULL,
    [FleetNumber]      NVARCHAR (20)  NULL,
    [ChassisNumber]    NVARCHAR (30)  NULL,
    [CustomerName]     NVARCHAR (50)  NULL,
    [VehicleTypeId]    INT            NULL,
    [Status]           NVARCHAR (20)  NULL,
    [Odometer]         FLOAT (53)     NULL,
    [Year]             INT            NULL,
    [Make]             NVARCHAR (50)  NULL,
    [Model]            NVARCHAR (50)  NULL,
    [Color]            NVARCHAR (20)  NULL,
    [KiloMeters]       INT            NULL,
    [Vin]              NVARCHAR (20)  NULL,
    [Description]      NVARCHAR (500) NULL,
    [Remarks]          NVARCHAR (500) NULL,
    [PurchaseDate]     DATETIME2 (7)  NULL,
    [FuelConsumption]  INT            NULL,
    [WorkIdleTime]     NVARCHAR (20)  NULL,
    [RowStatusId]      INT            NULL,
    [CreatedByUserId]  NVARCHAR (36)  NULL,
    [CreationDate]     DATETIME2 (7)  NULL,
    [ModifiedByUserId] NVARCHAR (36)  NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Vehicle_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Vehicle_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Vehicle_VehicleType_VehicleTypeId] FOREIGN KEY ([VehicleTypeId]) REFERENCES [dbo].[VehicleType] ([Id])
);






GO



GO



GO


