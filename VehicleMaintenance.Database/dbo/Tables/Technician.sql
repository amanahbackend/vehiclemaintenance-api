﻿CREATE TABLE [dbo].[Technician] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [CivilIdNo]        NVARCHAR (20)  NULL,
    [FirstName]        NVARCHAR (50)  NULL,
    [LastName]         NVARCHAR (50)  NULL,
    [Address]          NVARCHAR (200) NULL,
    [PhoneNumber]      NVARCHAR (20)  NULL,
    [Specialty]        NVARCHAR (50)  NULL,
    [EmployeeId]       NVARCHAR (36)  NULL,
    [HourRating]       FLOAT (53)     NULL,
    [Available]        BIT            NULL,
    [RowStatusId]      INT            NULL,
    [CreatedByUserId]  NVARCHAR (36)  NULL,
    [CreationDate]     DATETIME2 (7)  NULL,
    [ModifiedByUserId] NVARCHAR (36)  NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Technician] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Technician_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Technician_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);








GO



GO


