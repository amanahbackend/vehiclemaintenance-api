﻿CREATE TABLE [dbo].[JobCard] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [DateIn]           DATETIME2 (7)  NULL,
    [DateOut]          DATETIME2 (7)  NULL,
    [DateInShiftId]    INT            NULL,
    [DateOutShiftId]   INT            NULL,
    [Description]      NVARCHAR (200) NULL,
    [VehicleId]        INT            NOT NULL,
    [DriverId]         INT            NULL,
    [Comment]          NVARCHAR (200) NULL,
    [JobCardStatusId]  INT            NULL,
    [Odometer]         FLOAT (53)     NULL,
    [Hourmeter]        FLOAT (53)     NULL,
    [RowStatusId]      INT            NULL,
    [CreatedByUserId]  NVARCHAR (36)  NULL,
    [CreationDate]     DATETIME2 (7)  NULL,
    [ModifiedByUserId] NVARCHAR (36)  NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_JobCard] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_JobCard_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_JobCard_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_JobCard_Driver_DriverId] FOREIGN KEY ([DriverId]) REFERENCES [dbo].[Driver] ([Id]),
    CONSTRAINT [FK_JobCard_JobCardStatus_JobCardStatusId] FOREIGN KEY ([JobCardStatusId]) REFERENCES [dbo].[JobCardStatus] ([Id]),
    CONSTRAINT [FK_JobCard_Vehicle_VehicleId] FOREIGN KEY ([VehicleId]) REFERENCES [dbo].[Vehicle] ([Id]) ON DELETE CASCADE
);








GO



GO



GO



GO



GO



GO



GO


