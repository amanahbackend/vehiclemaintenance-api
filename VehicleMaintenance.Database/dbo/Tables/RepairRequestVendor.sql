﻿CREATE TABLE [dbo].[RepairRequestVendor] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [RepairRequestId]  INT           CONSTRAINT [DF__RepairReq__Repai__4336F4B9] DEFAULT ((0)) NOT NULL,
    [VendorId]         INT           NOT NULL,
    [RowStatusId]      INT           NULL,
    [CreatedByUserId]  NVARCHAR (36) NULL,
    [CreationDate]     DATETIME2 (7) NULL,
    [ModifiedByUserId] NVARCHAR (36) NULL,
    [ModificationDate] DATETIME2 (7) NULL,
    CONSTRAINT [PK_RepairRequestVendor] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RepairRequestVendor_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_RepairRequestVendor_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_RepairRequestVendor_RepairRequest_RepairRequestId] FOREIGN KEY ([RepairRequestId]) REFERENCES [dbo].[RepairRequest] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_RepairRequestVendor_Vendor_VendorId] FOREIGN KEY ([VendorId]) REFERENCES [dbo].[Vendor] ([Id]) ON DELETE CASCADE
);




GO



GO



GO



GO


