﻿CREATE TABLE [dbo].[ServiceType] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [NameAr]           NVARCHAR (100) NULL,
    [NameEn]           NVARCHAR (100) NULL,
    [Period]           INT            NOT NULL,
    [RowStatusId]      INT            NULL,
    [CreatedByUserId]  NVARCHAR (36)  NULL,
    [CreationDate]     DATETIME2 (7)  NULL,
    [ModifiedByUserId] NVARCHAR (36)  NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_ServiceType] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ServiceType_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_ServiceType_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);






GO



GO


