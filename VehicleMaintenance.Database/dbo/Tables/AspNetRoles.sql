﻿CREATE TABLE [dbo].[AspNetRoles] (
    [Id]               NVARCHAR (36)  NOT NULL,
    [Name]             NVARCHAR (256) NULL,
    [NormalizedName]   NVARCHAR (256) NULL,
    [ConcurrencyStamp] NVARCHAR (MAX) NULL,
    [CreatedBy]        NVARCHAR (36)  NULL,
    [CreationDate]     DATETIME2 (7)  NULL,
    [ModifiedBy]       NVARCHAR (36)  NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED ([Id] ASC)
);




GO


