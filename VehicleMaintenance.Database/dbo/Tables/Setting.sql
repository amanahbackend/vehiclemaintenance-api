﻿CREATE TABLE [dbo].[Setting] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [Code]             NVARCHAR (50)  NULL,
    [Name]             NVARCHAR (50)  NULL,
    [Value]            NVARCHAR (100) NULL,
    [Category]         NVARCHAR (50)  NULL,
    [Description]      NVARCHAR (100) NULL,
    [DataType]         INT            NOT NULL,
    [RowStatusId]      INT            NULL,
    [CreatedByUserId]  NVARCHAR (36)  NULL,
    [CreationDate]     DATETIME2 (7)  NULL,
    [ModifiedByUserId] NVARCHAR (36)  NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Setting_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Setting_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);








GO



GO


