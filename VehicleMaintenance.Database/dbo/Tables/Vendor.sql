﻿CREATE TABLE [dbo].[Vendor] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (100) NULL,
    [Address]          NVARCHAR (200) NULL,
    [RowStatusId]      INT            NULL,
    [CreatedByUserId]  NVARCHAR (36)  NULL,
    [CreationDate]     DATETIME2 (7)  NULL,
    [ModifiedByUserId] NVARCHAR (36)  NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Vendor] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Vendor_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Vendor_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);






GO



GO


