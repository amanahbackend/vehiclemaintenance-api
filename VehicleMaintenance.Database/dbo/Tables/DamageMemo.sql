﻿CREATE TABLE [dbo].[DamageMemo] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [DamageMemoRef]    NVARCHAR (50)  NULL,
    [TechnicianId]     INT            NULL,
    [DriverId]         INT            NULL,
    [VehicleId]        INT            NULL,
    [RowStatusId]      INT            NULL,
    [CreatedByUserId]  NVARCHAR (36)  NULL,
    [CreationDate]     DATETIME2 (7)  NULL,
    [ModifiedByUserId] NVARCHAR (36)  NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    [ForemanName]      NVARCHAR (50)  NULL,
    [SparePart1Name]   NVARCHAR (100) NULL,
    [SparePart2Name]   NVARCHAR (100) NULL,
    [SparePart3Name]   NVARCHAR (100) NULL,
    CONSTRAINT [PK_DamageMemo] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DamageMemo_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_DamageMemo_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_DamageMemo_Driver_DriverId] FOREIGN KEY ([DriverId]) REFERENCES [dbo].[Driver] ([Id]),
    CONSTRAINT [FK_DamageMemo_Technician_TechnicianId] FOREIGN KEY ([TechnicianId]) REFERENCES [dbo].[Technician] ([Id]),
    CONSTRAINT [FK_DamageMemo_Vehicle_VehicleId] FOREIGN KEY ([VehicleId]) REFERENCES [dbo].[Vehicle] ([Id])
);






GO



GO



GO



GO



GO


