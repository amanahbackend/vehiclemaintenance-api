﻿CREATE TABLE [dbo].[Shift] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (100) NULL,
    [StartTime]        TIME (7)       NULL,
    [EndTime]          TIME (7)       NULL,
    [RowStatusId]      INT            NULL,
    [CreatedByUserId]  NVARCHAR (36)  NULL,
    [CreationDate]     DATETIME2 (7)  NULL,
    [ModifiedByUserId] NVARCHAR (36)  NULL,
    [ModificationDate] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Shift] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Shift_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Shift_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);






GO



GO


