﻿CREATE TABLE [dbo].[UploadedFile] (
    [Id]                    INT            IDENTITY (1, 1) NOT NULL,
    [FileName]              NVARCHAR (50)  NULL,
    [FileRelativePath]      NVARCHAR (100) NULL,
    [FileType]              NVARCHAR (20)  NULL,
    [JobCardId]             INT            NULL,
    [RepairRequestVendorId] INT            NULL,
    [IsDefault]             BIT            NULL,
    [RowStatusId]           INT            NULL,
    [CreatedByUserId]       NVARCHAR (36)  NULL,
    [CreationDate]          DATETIME2 (7)  NULL,
    [ModifiedByUserId]      NVARCHAR (36)  NULL,
    [ModificationDate]      DATETIME2 (7)  NULL,
    CONSTRAINT [PK_UploadedFile] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UploadedFile_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_UploadedFile_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_UploadedFile_JobCard_JobCardId] FOREIGN KEY ([JobCardId]) REFERENCES [dbo].[JobCard] ([Id]),
    CONSTRAINT [FK_UploadedFile_RepairRequestVendor_RepairRequestVendorId] FOREIGN KEY ([RepairRequestVendorId]) REFERENCES [dbo].[RepairRequestVendor] ([Id])
);












GO



GO



GO



GO



GO



GO


