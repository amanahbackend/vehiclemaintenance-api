﻿CREATE TABLE [dbo].[SparePartDeliveryOrderDetails] (
    [Id]                       INT           IDENTITY (1, 1) NOT NULL,
    [SparePartDeliveryOrderId] INT           NULL,
    [RepairRequestSparePartId] INT           NULL,
    [SparePartId]              INT           NULL,
    [DeliveredQty]             INT           NULL,
    [RowStatusId]              INT           NULL,
    [CreatedByUserId]          NVARCHAR (36) NULL,
    [CreationDate]             DATETIME2 (7) NULL,
    [ModifiedByUserId]         NVARCHAR (36) NULL,
    [ModificationDate]         DATETIME2 (7) NULL,
    CONSTRAINT [PK_SparePartDeliveryOrderDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SparePartDeliveryOrderDetails_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_SparePartDeliveryOrderDetails_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_SparePartDeliveryOrderDetails_RepairRequestSparePart_RepairRequestSparePartId] FOREIGN KEY ([RepairRequestSparePartId]) REFERENCES [dbo].[RepairRequestSparePart] ([Id]),
    CONSTRAINT [FK_SparePartDeliveryOrderDetails_SparePart_SparePartId] FOREIGN KEY ([SparePartId]) REFERENCES [dbo].[SparePart] ([Id]),
    CONSTRAINT [FK_SparePartDeliveryOrderDetails_SparePartDeliveryOrder_SparePartDeliveryOrderId] FOREIGN KEY ([SparePartDeliveryOrderId]) REFERENCES [dbo].[SparePartDeliveryOrder] ([Id])
);






GO



GO



GO



GO



GO


