﻿CREATE TABLE [dbo].[RepairRequestSparePart] (
    [Id]                        INT             IDENTITY (1, 1) NOT NULL,
    [RepairRequestId]           INT             NULL,
    [JobCardId]                 INT             NULL,
    [VehicleId]                 INT             NULL,
    [SparePartId]               INT             NULL,
    [DeliveredQuantity]         INT             NULL,
    [Quantity]                  INT             NULL,
    [Amount]                    DECIMAL (18, 4) NULL,
    [Source]                    NVARCHAR (100)  NULL,
    [KiloMetersChangeSparePart] INT             NULL,
    [TechnicianId]              INT             NULL,
    [TechnicianStartDate]       DATETIME2 (7)   NULL,
    [TechnicianEndDate]         DATETIME2 (7)   NULL,
    [Comments]                  NVARCHAR (200)  NULL,
    [RowStatusId]               INT             NULL,
    [CreatedByUserId]           NVARCHAR (36)   NULL,
    [CreationDate]              DATETIME2 (7)   NULL,
    [ModifiedByUserId]          NVARCHAR (36)   NULL,
    [ModificationDate]          DATETIME2 (7)   NULL,
    CONSTRAINT [PK_RepairRequestSparePart] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RepairRequestSparePart_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_RepairRequestSparePart_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_RepairRequestSparePart_JobCard_JobCardId] FOREIGN KEY ([JobCardId]) REFERENCES [dbo].[JobCard] ([Id]),
    CONSTRAINT [FK_RepairRequestSparePart_RepairRequest_RepairRequestId] FOREIGN KEY ([RepairRequestId]) REFERENCES [dbo].[RepairRequest] ([Id]),
    CONSTRAINT [FK_RepairRequestSparePart_SparePart_SparePartId] FOREIGN KEY ([SparePartId]) REFERENCES [dbo].[SparePart] ([Id]),
    CONSTRAINT [FK_RepairRequestSparePart_Technician_TechnicianId] FOREIGN KEY ([TechnicianId]) REFERENCES [dbo].[Technician] ([Id]),
    CONSTRAINT [FK_RepairRequestSparePart_Vehicle_VehicleId] FOREIGN KEY ([VehicleId]) REFERENCES [dbo].[Vehicle] ([Id])
);












GO



GO



GO



GO



GO



GO



GO


