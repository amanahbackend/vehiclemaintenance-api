﻿CREATE TABLE [dbo].[AspNetUsers] (
    [Id]                   NVARCHAR (36)      NOT NULL,
    [UserName]             NVARCHAR (256)     NULL,
    [NormalizedUserName]   NVARCHAR (256)     NULL,
    [Email]                NVARCHAR (256)     NULL,
    [NormalizedEmail]      NVARCHAR (256)     NULL,
    [EmailConfirmed]       BIT                NOT NULL,
    [PhoneNumber]          NVARCHAR (MAX)     NULL,
    [PhoneNumberConfirmed] BIT                NOT NULL,
    [FirstName]            NVARCHAR (50)      NULL,
    [LastName]             NVARCHAR (50)      NULL,
    [CreatedByUserId]      NVARCHAR (36)      NULL,
    [CreationDate]         DATETIME2 (7)      NULL,
    [ModifiedByUserId]     NVARCHAR (36)      NULL,
    [ModificationDate]     DATETIME2 (7)      NULL,
    [TwoFactorEnabled]     BIT                NOT NULL,
    [LockoutEnd]           DATETIMEOFFSET (7) NULL,
    [LockoutEnabled]       BIT                NOT NULL,
    [AccessFailedCount]    INT                NOT NULL,
    [ConcurrencyStamp]     NVARCHAR (MAX)     NULL,
    [PasswordHash]         NVARCHAR (MAX)     NULL,
    [SecurityStamp]        NVARCHAR (MAX)     NULL,
    CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED ([Id] ASC)
);








GO



GO


