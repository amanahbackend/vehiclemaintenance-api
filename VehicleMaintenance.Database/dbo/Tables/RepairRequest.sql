﻿CREATE TABLE [dbo].[RepairRequest] (
    [Id]                    INT            IDENTITY (1, 1) NOT NULL,
    [JobCardId]             INT            NULL,
    [RepairRequestTypeId]   INT            NULL,
    [RepairRequestStatusId] INT            NULL,
    [ServiceTypeId]         INT            NULL,
    [Duration]              FLOAT (53)     NOT NULL,
    [Cost]                  FLOAT (53)     NOT NULL,
    [External]              BIT            NULL,
    [Comment]               NVARCHAR (200) NULL,
    [RowStatusId]           INT            NULL,
    [CreationDate]          DATETIME2 (7)  NULL,
    [CreatedByUserId]       NVARCHAR (36)  NULL,
    [ModifiedByUserId]      NVARCHAR (36)  NULL,
    [ModificationDate]      DATETIME2 (7)  NULL,
    CONSTRAINT [PK_RepairRequest] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RepairRequest_AspNetUsers_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_RepairRequest_AspNetUsers_ModifiedByUserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_RepairRequest_JobCard_JobCardId] FOREIGN KEY ([JobCardId]) REFERENCES [dbo].[JobCard] ([Id]),
    CONSTRAINT [FK_RepairRequest_RepairRequestStatus_RepairRequestStatusId] FOREIGN KEY ([RepairRequestStatusId]) REFERENCES [dbo].[RepairRequestStatus] ([Id]),
    CONSTRAINT [FK_RepairRequest_RepairRequestType_RepairRequestTypeId] FOREIGN KEY ([RepairRequestTypeId]) REFERENCES [dbo].[RepairRequestType] ([Id]),
    CONSTRAINT [FK_RepairRequest_ServiceType_ServiceTypeId] FOREIGN KEY ([ServiceTypeId]) REFERENCES [dbo].[ServiceType] ([Id])
);






GO



GO



GO



GO



GO



GO


