﻿
-- =============================================
-- Author:		Mohammed Osman
-- Create date: 27/11/2018
-- Description:	
-- EXEC dbo.stp_JobCard_Get1Report @jobCardId = 346;
-- EXEC dbo.stp_JobCard_Get1Report @vehicleId = 660, @jobCardId = 0;
-- =============================================
CREATE PROCEDURE [dbo].[stp_JobCard_Get1Report]
(
    @jobCardId INT = 0,
    @vehicleId INT = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    IF (@jobCardId > 0)
    BEGIN

        SELECT @vehicleId = JobCard.VehicleId
        FROM dbo.JobCard
        WHERE Id = @jobCardId;

    END;

    DECLARE @vehicleChassisNo NVARCHAR(50);
    DECLARE @vehiclePlateNo NVARCHAR(50);
    DECLARE @vehicleFleetNo NVARCHAR(50);
    DECLARE @vehicleType NVARCHAR(50);
    DECLARE @customerName NVARCHAR(50);

    SELECT @vehicleChassisNo = Vehicle.ChassisNumber,
           @vehiclePlateNo = Vehicle.PlateNumber,
           @vehicleFleetNo = Vehicle.FleetNumber,
           @vehicleType = VehicleType.[Name],
           @customerName = Vehicle.CustomerName
    FROM dbo.Vehicle
        INNER JOIN dbo.VehicleType
            ON VehicleType.Id = Vehicle.VehicleTypeId
    WHERE Vehicle.Id = @vehicleId;

    IF (@jobCardId > 0)
    BEGIN

        DECLARE @createdByUserId VARCHAR(36);
        SELECT @createdByUserId = JobCard.CreatedByUserId
        FROM dbo.JobCard
        WHERE JobCard.Id = @jobCardId;

        DECLARE @requestedByUserName NVARCHAR(50);
        SELECT @requestedByUserName = FirstName + N' ' + LastName
        FROM [dbo].[AspNetUsers]
        WHERE Id = @createdByUserId;

        SELECT JobCard.Id AS JobCardId,
               @vehicleId AS VehicleId,
               @vehicleChassisNo AS VehicleChassisNo,
               @vehiclePlateNo AS VehiclePlateNo,
               @vehicleFleetNo AS VehicleFleetNo,
               @vehicleType AS VehicleType,
               JobCard.CreationDate AS CreationDate,
               Vehicle.CustomerName AS CustomerName,
               @requestedByUserName AS RequestedByUserName,
               DateIn AS DateIn,
               DateOut AS DateOut,
               JobCard.Hourmeter,
               JobCard.Odometer,
               JobCardStatus.NameEn AS JobCardStatus
        FROM dbo.JobCard
            INNER JOIN dbo.Vehicle
                ON Vehicle.Id = JobCard.VehicleId
            INNER JOIN dbo.JobCardStatus
                ON JobCardStatus.Id = JobCard.JobCardStatusId
        WHERE JobCard.Id = @jobCardId;

    END;
    ELSE
    BEGIN

        SELECT NULL AS JobCardId,
               @vehicleId AS VehicleId,
               @vehicleChassisNo AS VehicleChassisNo,
               @vehiclePlateNo AS VehiclePlateNo,
               @vehicleFleetNo AS VehicleFleetNo,
               @vehicleType AS VehicleType,
               SYSDATETIME() AS CreationDate,
               @customerName AS CustomerName,
               N'' AS RequestedByUserName,
               SYSDATETIME() AS DateIn,
               SYSDATETIME() AS DateOut,
               NULL AS Hourmeter,
               NULL AS Odometer,
               N'' AS JobCardStatus;

    END;
END;
