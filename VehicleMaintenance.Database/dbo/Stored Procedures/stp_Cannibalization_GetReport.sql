﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 05/03/2019
-- Description:	
-- EXEC dbo.stp_Cannibalization_GetReport @cannId = 1;
-- =============================================
CREATE PROCEDURE [dbo].[stp_Cannibalization_GetReport] 
(
	@cannId INT = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    SELECT Cann.Id AS CannId,
           Cann.FixingDate,
           Cann.VehicleFromId,
           VehicleFrom.FleetNumber AS VehicleFromFleetNo,
           Cann.VehicleFromRegnNo,
           Cann.TechnicianRemovedById,
           TechnicianRemovedBy.FirstName + N' ' + TechnicianRemovedBy.LastName AS TechnicianNameRemovedBy,
           TechnicianRemovedBy.EmployeeId AS TechnicianEmpIdRemovedBy,
           Cann.VehicleToId,
		   VehicleTo.FleetNumber AS VehicleToFleetNo,
           Cann.VehicleToRegnNo,
           Cann.TechnicianFixedById,
           TechnicianFixedBy.FirstName + N' ' + TechnicianFixedBy.LastName AS TechnicianNameFixedBy,
           TechnicianFixedBy.EmployeeId AS TechnicianEmpIdFixedBy,
           Cann.SupervisorComments,
           Cann.RefixingDate,
           Cann.TechnicianRefixedById,
           TechnicianRefixedBy.FirstName + N' ' + TechnicianRefixedBy.LastName AS TechnicianNameRefixedBy,
           TechnicianRefixedBy.EmployeeId AS TechnicianEmpIdRefixedBy
    FROM dbo.Cannibalization AS Cann
        INNER JOIN dbo.Vehicle AS VehicleFrom
            ON VehicleFrom.Id = Cann.VehicleFromId
        INNER JOIN dbo.Vehicle AS VehicleTo
            ON VehicleTo.Id = Cann.VehicleToId
        LEFT JOIN dbo.Technician AS TechnicianRemovedBy
            ON TechnicianRemovedBy.Id = Cann.TechnicianRemovedById
        LEFT JOIN dbo.Technician AS TechnicianFixedBy
            ON TechnicianFixedBy.Id = Cann.TechnicianFixedById
        LEFT JOIN dbo.Technician AS TechnicianRefixedBy
            ON TechnicianRefixedBy.Id = Cann.TechnicianRefixedById
    WHERE Cann.Id = @cannId;

END;