﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 10/03/2019
-- Description:	
-- EXEC dbo.stp_DamageMemo_GetReport @damageMemoId = 2;
-- =============================================
CREATE PROCEDURE [dbo].[stp_DamageMemo_GetReport] (@damageMemoId INT)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @technicianId INT;

    SELECT @technicianId = DamageMemo.TechnicianId
    FROM dbo.DamageMemo
    WHERE Id = @damageMemoId;

    IF (@technicianId IS NOT NULL)
    BEGIN

        SELECT DamageMemo.Id AS DamageMemoId,
               DamageMemo.DamageMemoRef,
               N'Technician' AS EmployeeType,
               Technician.CivilIdNo AS CivilIdNo,
               Technician.FirstName + N' ' + Technician.LastName AS EmployeeName,
               Technician.EmployeeId AS EmployeeId,
               DamageMemo.VehicleId,
               Vehicle.PlateNumber AS VehiclePlateNo,
               Vehicle.FleetNumber AS VehicleFleetNo,
               VehicleType.[Name] AS VehicleType,
               DamageMemo.ForemanName,
               DamageMemo.SparePart1Name,
               DamageMemo.SparePart2Name,
               DamageMemo.SparePart3Name,
               DamageMemo.CreatedByUserId,
               AspNetUsers.FirstName + N' ' + AspNetUsers.LastName AS CreatedByUserName,
               DamageMemo.CreationDate
        FROM dbo.DamageMemo
            INNER JOIN dbo.Vehicle
                ON Vehicle.Id = DamageMemo.VehicleId
            INNER JOIN dbo.VehicleType
                ON VehicleType.Id = Vehicle.VehicleTypeId
            INNER JOIN dbo.AspNetUsers
                ON DamageMemo.CreatedByUserId = AspNetUsers.Id
            INNER JOIN dbo.Technician
                ON DamageMemo.TechnicianId = Technician.Id
        WHERE DamageMemo.Id = @damageMemoId;

    END;
    ELSE
    BEGIN

        SELECT DamageMemo.Id AS DamageMemoId,
               DamageMemo.DamageMemoRef,
               N'Driver' AS EmployeeType,
               Driver.CivilIdNo AS CivilIdNo,
               Driver.FirstName + N' ' + Driver.LastName AS EmployeeName,
               Driver.EmployeeId AS EmployeeId,
               DamageMemo.VehicleId,
               Vehicle.PlateNumber AS VehiclePlateNo,
               Vehicle.FleetNumber AS VehicleFleetNo,
               VehicleType.[Name] AS VehicleType,
               DamageMemo.ForemanName,
               DamageMemo.SparePart1Name,
               DamageMemo.SparePart2Name,
               DamageMemo.SparePart3Name,
               DamageMemo.CreatedByUserId,
               AspNetUsers.FirstName + N' ' + AspNetUsers.LastName AS CreatedByUserName,
               DamageMemo.CreationDate
        FROM dbo.DamageMemo
            INNER JOIN dbo.Vehicle
                ON Vehicle.Id = DamageMemo.VehicleId
            INNER JOIN dbo.VehicleType
                ON VehicleType.Id = Vehicle.VehicleTypeId
            INNER JOIN dbo.AspNetUsers
                ON DamageMemo.CreatedByUserId = AspNetUsers.Id
            INNER JOIN dbo.Driver
                ON DamageMemo.DriverId = Driver.Id
        WHERE DamageMemo.Id = @damageMemoId;

    END;
END;