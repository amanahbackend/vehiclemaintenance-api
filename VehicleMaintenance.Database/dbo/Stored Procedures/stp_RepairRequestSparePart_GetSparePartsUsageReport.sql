﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 23/01/2019
-- Description:	
-- EXEC [dbo].[stp_RepairRequestSparePart_GetSparePartsUsageReport];
-- EXEC [dbo].[stp_RepairRequestSparePart_GetSparePartsUsageReport] @vehiclePlateNo = N'Plate662';
-- EXEC [dbo].[stp_RepairRequestSparePart_GetSparePartsUsageReport] @vehicleFleetNo = N'fleet662';
-- EXEC [dbo].[stp_RepairRequestSparePart_GetSparePartsUsageReport] @jobCardId = 15;
-- =============================================
CREATE PROCEDURE [dbo].[stp_RepairRequestSparePart_GetSparePartsUsageReport]
(
    @startDate DATETIME = NULL,
    @endDate DATETIME = NULL,
    @vehiclePlateNo NVARCHAR(50) = NULL,
    @vehicleFleetNo NVARCHAR(50) = NULL,
    @sparePartId INT = NULL,
    @jobCardId INT = NULL,      -- Null or 0
    @jobCardStatusId INT = NULL -- Null or 0
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @vehicleId INT = NULL;

    IF (@vehiclePlateNo IS NOT NULL)
    BEGIN

        SELECT TOP (1)
               @vehicleId = Vehicle.Id,
               @vehicleFleetNo = Vehicle.FleetNumber
        FROM dbo.Vehicle
        WHERE PlateNumber = @vehiclePlateNo
        ORDER BY Id DESC;

    END;
    ELSE IF (@vehicleFleetNo IS NOT NULL)
    BEGIN
        SELECT TOP (1)
               @vehicleId = Vehicle.Id,
               @vehiclePlateNo = Vehicle.PlateNumber
        FROM dbo.Vehicle
        WHERE FleetNumber = @vehicleFleetNo
        ORDER BY Id DESC;
    END;

    SET @jobCardId = NULLIF(@jobCardId, 0);
    SET @jobCardStatusId = NULLIF(@jobCardStatusId, 0);

    -- Adjust filters.................... {Start}

    DECLARE @startDateParameter NVARCHAR(100) = NULL;

    IF (@startDate IS NOT NULL)
    BEGIN
        SET @startDateParameter = N'Start Date: ' + CONVERT(VARCHAR(10), @startDate, 103);
        SELECT @startDate = [dbo].[GetDateWithFirstTime](@startDate);
    END;

    DECLARE @endDateParameter NVARCHAR(100) = NULL;

    IF (@endDate IS NOT NULL)
    BEGIN
        SET @endDateParameter = N'End Date: ' + CONVERT(VARCHAR(10), @endDate, 103);
        SELECT @endDate = [dbo].[GetDateWithLastTime](@endDate);
    END;

    DECLARE @vehiclePlateNoParameter NVARCHAR(100) = NULL;

    IF (@vehiclePlateNo IS NOT NULL)
    BEGIN
        SET @vehiclePlateNoParameter = N'Vehicle Plate No: ' + CONVERT(NVARCHAR(100), @vehiclePlateNo);
    END;

    DECLARE @vehicleFleetNoParameter NVARCHAR(100) = NULL;

    IF (@vehicleFleetNo IS NOT NULL)
    BEGIN
        SET @vehicleFleetNoParameter = N'Vehicle Fleet No: ' + CONVERT(NVARCHAR(100), @vehicleFleetNo);
    END;

    DECLARE @sparePartIdParameter NVARCHAR(100) = NULL;

    IF (@jobCardStatusId IS NOT NULL)
    BEGIN
        SELECT @sparePartIdParameter = N'Spare Part: ' + NameEn
        FROM dbo.SparePart
        WHERE Id = @sparePartId;
    END;

    DECLARE @jobCardStatusIdParameter NVARCHAR(100) = NULL;

    IF (@jobCardStatusId IS NOT NULL)
    BEGIN
        SELECT @jobCardStatusIdParameter = N'Job Card Status: ' + NameEn
        FROM dbo.JobCardStatus
        WHERE Id = @jobCardStatusId;
    END;

    DECLARE @jobCardIdParameter NVARCHAR(100) = NULL;

    IF (@jobCardId IS NOT NULL)
    BEGIN
        SET @jobCardIdParameter = N'Job Card Id: ' + CONVERT(NVARCHAR(100), @jobCardId);
    END;

    -- Adjust filters.................... {End}

    SELECT RRSP.Id AS RepairRequestSparePartId,
           RepairRequest.JobCardId,
           JobCard.DateIn AS JobCardDateIn,
           Vehicle.Id AS VehicleId,
           Vehicle.PlateNumber AS VehiclePlateNo,
           Vehicle.FleetNumber AS VehicleFleetNo,
           RRSP.RepairRequestId,
           RRSP.TechnicianId,
           Technician.FirstName + ' ' + Technician.LastName AS TechnicianName,
           RRSP.TechnicianStartDate AS TechnicianStartDate,
           RRSP.TechnicianEndDate AS TechnicianEndDate,
           RRSP.[Source],
           RRSP.SparePartId,
           SparePart.SerialNumber AS SparePartSerial,
           SparePart.NameEn AS SparePartName,
           CONVERT(DECIMAL(18, 2), RRSP.Amount) AS Amount,
           RRSP.Quantity,
           RRSP.Comments,
           DATEDIFF(HOUR, RRSP.TechnicianStartDate, RRSP.TechnicianEndDate) AS TechnicianWorkHours,
           @sparePartIdParameter AS SparePartParameter,
           @startDateParameter AS StartDateParameter,
           @endDateParameter AS EndDateParameter,
           @jobCardStatusIdParameter AS JobCardStatusParameter,
           @vehiclePlateNoParameter AS VehiclePlateNoParameter,
           @vehicleFleetNoParameter AS VehicleFleetNoParameter,
           @jobCardIdParameter AS JobCardParameter
    FROM dbo.RepairRequestSparePart AS RRSP
        INNER JOIN dbo.RepairRequest
            ON RepairRequest.Id = RRSP.RepairRequestId
        INNER JOIN dbo.JobCard
            ON JobCard.Id = RepairRequest.JobCardId
        INNER JOIN dbo.SparePart
            ON SparePart.Id = RRSP.SparePartId
        INNER JOIN dbo.Technician
            ON Technician.Id = RRSP.TechnicianId
        INNER JOIN dbo.Vehicle
            ON Vehicle.Id = JobCard.VehicleId
    WHERE RRSP.RowStatusId = 1
          AND RepairRequest.RowStatusId = 1
          AND JobCard.JobCardStatusId IN ( 2, 3 ) -- In Progress and Closed
          AND
          (
              RRSP.SparePartId = @sparePartId
              OR @sparePartId IS NULL
          )
          AND
          (
              JobCard.Id = @jobCardId
              OR @jobCardId IS NULL
          )
          AND
          (
              JobCard.VehicleId = @vehicleId
              OR @vehicleId IS NULL
          )
          AND
          (
              JobCard.DateIn >= @startDate
              OR @startDate IS NULL
          )
          AND
          (
              JobCard.DateIn <= @endDate
              OR @endDate IS NULL
          )
          AND
          (
              JobCard.JobCardStatusId = @jobCardStatusId
              OR @jobCardStatusId IS NULL
          )
    ORDER BY RRSP.Id DESC;

END;