﻿
-- =============================================
-- Author:		Mohammed Osman
-- Create date: 05/03/2019
-- Description:	
-- EXEC dbo.stp_CannibalizationSparePart_GetReport @cannid = 1;
-- =============================================
CREATE PROCEDURE [dbo].[stp_CannibalizationSparePart_GetReport] (@cannId INT = NULL)
AS
BEGIN

    SET NOCOUNT ON;

    SELECT CannSparePart.SparePartId,
           SparePart.SerialNumber AS SparePartSerialNo,
           SparePart.NameEn AS SparePartName
    FROM dbo.CannibalizationSparePart AS CannSparePart
        INNER JOIN dbo.SparePart
            ON SparePart.Id = CannSparePart.SparePartId
    WHERE CannSparePart.CannibalizationId = @cannId;

END;