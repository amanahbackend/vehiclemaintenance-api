﻿
-- =============================================
-- Author:		Mohammed Osman
-- Create date: 12/12/2018
-- Description:	
-- EXEC dbo.stp_JobCards_GetRepairRequests @jobCardId = 73;
-- EXEC dbo.stp_JobCards_GetRepairRequests @jobCardId = 0;
-- =============================================
CREATE PROCEDURE [dbo].[stp_JobCards_GetRepairRequests]
(
	@jobCardId INT -- Pass 0 to get empty data for the blank job card report.
)
AS
BEGIN

    SET NOCOUNT ON;

    IF (@jobCardId <> 0)
    BEGIN

        SELECT ROW_NUMBER() OVER (ORDER BY RepairRequest.Id) AS RowNumber,
               RepairRequest.Id AS RepairRequestId,
               RepairRequest.JobCardId AS JobCardId,
               ServiceType.NameEn AS ServiceTypeName,
               CASE
                   WHEN RepairRequest.[External] = 1 THEN
                       N'OSP'
                   ELSE
                       N'Internal'
               END AS Place,
               RepairRequestType.NameEn AS RequestType,
               RepairRequestStatus.NameEn AS RequestStatus,
               RepairRequest.Duration,
               RepairRequest.Cost
        FROM dbo.RepairRequest
            INNER JOIN dbo.ServiceType
                ON ServiceType.Id = RepairRequest.ServiceTypeId
            INNER JOIN dbo.RepairRequestType
                ON RepairRequestType.Id = RepairRequest.RepairRequestTypeId
            INNER JOIN dbo.RepairRequestStatus
                ON RepairRequestStatus.Id = RepairRequest.RepairRequestStatusId
        WHERE RepairRequest.JobCardId = @jobCardId
              AND RepairRequest.RowStatusId <> -1 -- Not deleted
              AND RepairRequest.RepairRequestStatusId IN ( 4, 5 ); -- InProgress and Closed.
    END;
    ELSE
    BEGIN

        DECLARE @rowsCount INT = 5;

        DECLARE @returnedTable TABLE
        (
            RowNumber INT NOT NULL,
            RepairRequestId INT NULL,
            JobCardId INT NULL,
            ServiceName NVARCHAR(50) NULL,
            Place NVARCHAR(50) NULL,
            RequestType NVARCHAR(50) NULL,
            RequestStatus NVARCHAR(50) NULL,
            Duration DECIMAL(18, 2) NULL,
            Cost DECIMAL(18, 2) NULL
        );

        DECLARE @index INT = 1;

        WHILE (@index <= @rowsCount)
        BEGIN

            INSERT INTO @returnedTable
            (
                RowNumber,
                RepairRequestId,
                JobCardId,
                ServiceName,
                Place,
                RequestType,
                RequestStatus,
                Duration,
                Cost
            )
            VALUES
            (@index, NULL, NULL, N'', N'', N'', N'', NULL, NULL);

            SET @index += 1;

        END;

        SELECT RowNumber,
               RepairRequestId,
               JobCardId,
               ServiceName,
               Place,
               RequestType,
               RequestStatus,
               Duration,
               Cost
        FROM @returnedTable;

    END;

END;
