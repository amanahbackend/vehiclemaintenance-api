﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 27/01/2019
-- Description:	
-- EXEC dbo.stp_SparePartDeliveryOrder_GetMaterialRequisitionReport;
-- EXEC dbo.stp_SparePartDeliveryOrder_GetMaterialRequisitionReport @deliveryOrderId = 35;
-- =============================================
CREATE PROCEDURE [dbo].[stp_SparePartDeliveryOrder_GetMaterialRequisitionReport] (@deliveryOrderId INT = 0)
AS
BEGIN

    SET NOCOUNT ON;

    IF (@deliveryOrderId > 0)
    BEGIN

        SELECT ROW_NUMBER() OVER (ORDER BY SparePartDeliveryOrderDetails.Id ASC) AS RowNumber,
               SparePartDeliveryOrderDetails.Id AS DeliveryOrderDetailId,
               SparePartDeliveryOrderDetails.SparePartDeliveryOrderId,
               SparePartDeliveryOrder.RequestedBy,
               SparePartDeliveryOrderDetails.RepairRequestSparePartId,
               SparePartDeliveryOrderDetails.SparePartId,
               SparePart.SerialNumber AS SparePartSerialNumber,
               SparePart.NameEn AS SparePartDescription,
               SparePartDeliveryOrderDetails.DeliveredQty,
               SparePart.Cost,
               AspNetUsers.FirstName + N' ' + AspNetUsers.LastName AS CreatedByFullName,
               SparePartDeliveryOrder.CreationDate
        FROM dbo.SparePartDeliveryOrderDetails
            INNER JOIN dbo.SparePartDeliveryOrder
                ON SparePartDeliveryOrder.Id = SparePartDeliveryOrderDetails.SparePartDeliveryOrderId
            INNER JOIN dbo.SparePart
                ON SparePart.Id = SparePartDeliveryOrderDetails.SparePartId
            INNER JOIN dbo.AspNetUsers
                ON AspNetUsers.Id = SparePartDeliveryOrder.CreatedByUserId
        WHERE SparePartDeliveryOrderDetails.SparePartDeliveryOrderId = @deliveryOrderId;

    END;
    ELSE
    BEGIN

        DECLARE @rowsCount INT = 20;

        DECLARE @returnedTable TABLE
        (
            RowNumber INT NOT NULL,
            DeliveryOrderDetailId INT NULL,
            SparePartDeliveryOrderId INT NULL,
            RequestedBy NVARCHAR(50) NULL,
            RepairRequestSparePartId INT NULL,
            SparePartId INT NULL,
            SparePartSerialNumber NVARCHAR(50) NULL,
            SparePartDescription NVARCHAR(50) NULL,
            DeliveredQty INT NULL,
            Cost DECIMAL(18, 2) NULL,
            CreatedByFullName NVARCHAR(50) NULL,
            CreationDate DATETIME NULL
        );

        DECLARE @index INT = 1;

        WHILE (@index <= @rowsCount)
        BEGIN

            INSERT INTO @returnedTable
            (
                RowNumber,
                DeliveryOrderDetailId,
                SparePartDeliveryOrderId,
                RequestedBy,
                RepairRequestSparePartId,
                SparePartId,
                SparePartSerialNumber,
                SparePartDescription,
                DeliveredQty,
                Cost,
                CreatedByFullName,
                CreationDate
            )
            VALUES
            (   @index,       -- RowNumber - int
                NULL,         -- DeliveryOrderDetailId - int
                NULL,         -- SparePartDeliveryOrderId - int
                N'',          -- RequestedBy - nvarchar(50)
                NULL,         -- RepairRequestSparePartId - int
                NULL,         -- SparePartId - int
                N'',          -- SparePartSerialNumber - nvarchar(50)
                N'',          -- SparePartDescription - nvarchar(50)
                NULL,         -- DeliveredQty - int
                NULL,         -- Cost - decimal(18, 2)
                N'',          -- CreatedByFullName - nvarchar(50)
                SYSDATETIME() -- CreationDate - datetime
                );

            SET @index += 1;

        END;

        SELECT RowNumber,
               DeliveryOrderDetailId,
               SparePartDeliveryOrderId,
               RequestedBy,
               RepairRequestSparePartId,
               SparePartId,
               SparePartSerialNumber,
               SparePartDescription,
               DeliveredQty,
               Cost,
               CreatedByFullName,
               CreationDate
        FROM @returnedTable;

    END;
END;