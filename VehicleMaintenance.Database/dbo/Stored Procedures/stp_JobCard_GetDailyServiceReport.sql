﻿
-- =============================================
-- Author:		Mohammed Osman
-- Create date: 28/11/2018
-- Description:	
-- EXEC dbo.stp_JobCard_GetDailyServiceReport;
-- EXEC dbo.stp_JobCard_GetDailyServiceReport @vehiclePlateNo=N'4-20657';
-- EXEC dbo.stp_JobCard_GetDailyServiceReport @startDate = '12/9/2018';
-- EXEC dbo.stp_JobCard_GetDailyServiceReport @startDate = '12/9/2018', @endDate = '12/9/2019';
-- =============================================
CREATE PROCEDURE [dbo].[stp_JobCard_GetDailyServiceReport]
(
    @startDate DATETIME = NULL,
    @endDate DATETIME = NULL,
    @vehiclePlateNo NVARCHAR(50) = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    IF (@startDate IS NOT NULL)
    BEGIN
        SELECT @startDate = [dbo].[GetDateWithFirstTime](@startDate);
    END;

    IF (@endDate IS NOT NULL)
    BEGIN
        SELECT @endDate = [dbo].[GetDateWithLastTime](@endDate);
    END;

    DECLARE @vehicleId INT = NULL;

    IF (@vehiclePlateNo IS NOT NULL)
    BEGIN

        SELECT @vehicleId = Vehicle.Id
        FROM dbo.Vehicle
        WHERE PlateNumber = @vehiclePlateNo
              AND RowStatusId <> -1; -- Not deleted vehicle.

    END;

    SELECT ROW_NUMBER() OVER (PARTITION BY JobCardsTable.ShiftName
                              ORDER BY JobCardsTable.DateInShiftId ASC
                             ) AS RowNumber,
           JobCardsTable.ShiftName,
           JobCardsTable.JobCardId,
           JobCardsTable.CreationDate,
           JobCardsTable.VehicleId,
           JobCardsTable.VehiclePlateNo,
           JobCardsTable.VehicleFleetNo,
           JobCardsTable.CustomerName,
           JobCardsTable.DateIn,
           JobCardsTable.DateOut,
           JobCardsTable.DateInShiftId,
           JobCardsTable.DateOutShiftId,
           JobCardsTable.RepairRequestId,
           JobCardsTable.ServiceTypeId,
           JobCardsTable.ServiceType,
           JobCardsTable.JobCardDescription,
           JobCardsTable.Comment,
           @startDate AS StartDateParameter,
           @endDate AS EndDateParameter
    FROM
    (
        SELECT [Shift].[Name] AS ShiftName,
               JobCard.Id AS JobCardId,
               JobCard.CreationDate AS CreationDate,
               Vehicle.Id AS VehicleId,
               Vehicle.PlateNumber AS VehiclePlateNo,
               Vehicle.FleetNumber AS VehicleFleetNo,
               Vehicle.CustomerName AS CustomerName,
               JobCard.DateIn AS DateIn,
               JobCard.DateOut AS DateOut,
               JobCard.DateInShiftId,
               JobCard.DateOutShiftId,
               RepairRequest.Id AS RepairRequestId,
               RepairRequest.ServiceTypeId,
               ServiceType.NameEn AS ServiceType,
               JobCard.[Description] AS JobCardDescription,
               JobCard.Comment
        FROM dbo.JobCard
            INNER JOIN dbo.Vehicle
                ON Vehicle.Id = JobCard.VehicleId
            INNER JOIN dbo.[Shift]
                ON [Shift].Id = JobCard.DateInShiftId
            INNER JOIN dbo.RepairRequest
                ON RepairRequest.JobCardId = JobCard.Id
            INNER JOIN dbo.ServiceType
                ON ServiceType.Id = RepairRequest.ServiceTypeId
        WHERE (
                  Vehicle.Id = @vehicleId
                  OR @vehicleId IS NULL
              )
              AND
              (
                  JobCard.DateIn >= @startDate
                  OR @startDate IS NULL
              )
              AND
              (
                  JobCard.DateIn <= @endDate
                  OR @endDate IS NULL
              )
			  AND JobCard.JobCardStatusId IN (2, 3) -- In Progress and Closed.
			  AND RepairRequest.RepairRequestStatusId IN (5, 4) -- In Progress and Closed.
    ) AS JobCardsTable;

END;
