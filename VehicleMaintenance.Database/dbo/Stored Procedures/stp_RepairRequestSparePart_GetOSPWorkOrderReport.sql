﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 24/02/2019
-- Description:	
-- EXEC dbo.stp_RepairRequestSparePart_GetOSPWorkOrderReport @repairRequestId = 6;
-- =============================================
CREATE PROCEDURE dbo.stp_RepairRequestSparePart_GetOSPWorkOrderReport (@repairRequestId INT)
AS
BEGIN

    SET NOCOUNT ON;

    SELECT ROW_NUMBER() OVER (ORDER BY RRSP.Id) AS RowNumber,
           RRSP.Id,
           RRSP.RepairRequestId,
           RRSP.VehicleId,
           Vehicle.PlateNumber AS VehiclePlateNumber,
           Vehicle.FleetNumber AS VehicleFleetNumber,
           RRSP.SparePartId,
           SparePart.SerialNumber AS SparePartSerialNo,
           SparePart.NameEn AS SparePartName,
           RRSP.Quantity,
           CONVERT(DECIMAL(18, 2), RRSP.Amount) AS TotalCost
    FROM dbo.RepairRequestSparePart AS RRSP
        INNER JOIN dbo.Vehicle
            ON Vehicle.Id = RRSP.VehicleId
        INNER JOIN dbo.SparePart
            ON SparePart.Id = RRSP.SparePartId
    WHERE RRSP.RepairRequestId = @repairRequestId;
END;