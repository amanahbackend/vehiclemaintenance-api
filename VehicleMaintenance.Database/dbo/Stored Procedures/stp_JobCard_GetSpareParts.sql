﻿-- =============================================
-- Author:		Mohammed Osman
-- Create date: 27/11/2018
-- Description:	
-- EXEC dbo.stp_JobCard_GetSpareParts @jobCardId = 73;
-- EXEC dbo.stp_JobCard_GetSpareParts @jobCardId = 0;
-- =============================================
CREATE PROCEDURE [dbo].[stp_JobCard_GetSpareParts] (@jobCardId INT)
AS
BEGIN

    SET NOCOUNT ON;

    IF (@jobCardId <> 0)
    BEGIN

        SELECT RepairRequestSparePart.Id AS RequestSparePartId,
               RepairRequestSparePart.RepairRequestId,
               SparePart.SerialNumber,
               SparePart.NameEn AS SparePartName,
               RepairRequestSparePart.Quantity,
               CONVERT(DECIMAL(18, 2), RepairRequestSparePart.Amount) AS TotalCost,
               RepairRequestSparePart.TechnicianId,
               Technician.FirstName + N' ' + Technician.LastName AS TechnicianName,
               Technician.EmployeeId AS TechnicianEmployeeId,
               Technician.Specialty AS TechnicianSpeciality,
               RepairRequestSparePart.TechnicianStartDate,
               RepairRequestSparePart.TechnicianEndDate
        FROM dbo.RepairRequestSparePart
            INNER JOIN dbo.RepairRequest AS RepairRequest
                ON RepairRequest.Id = RepairRequestSparePart.RepairRequestId
            INNER JOIN dbo.SparePart
                ON SparePart.Id = RepairRequestSparePart.SparePartId
            INNER JOIN dbo.Technician
                ON Technician.Id = RepairRequestSparePart.TechnicianId
        WHERE RepairRequest.JobCardId = @jobCardId
              AND RepairRequestSparePart.RowStatusId = 1;

    END;
    ELSE
    BEGIN

        DECLARE @rowsCount INT = 7;

        DECLARE @returnedTable TABLE
        (
            RequestSparePartId INT NULL,
            RepairRequestId INT NULL,
            SerialNumber NVARCHAR(50) NULL,
            SparePartName NVARCHAR(50) NULL,
            Quantity INT NULL,
            TotalCost DECIMAL(18, 2) NULL,
            TechnicianId INT NULL,
            TechnicianName NVARCHAR(50) NULL,
            TechnicianEmployeeId INT NULL,
            TechnicianSpeciality NVARCHAR(50) NULL,
            TechnicianStartDate DATETIME NULL,
            TechnicianEndDate DATETIME NULL
        );

        DECLARE @index INT = 0;

        WHILE (@index <= @rowsCount)
        BEGIN

            INSERT INTO @returnedTable
            (
                RequestSparePartId,
                RepairRequestId,
                SerialNumber,
                SparePartName,
                Quantity,
                TotalCost,
                TechnicianId,
                TechnicianName,
                TechnicianEmployeeId,
                TechnicianSpeciality,
                TechnicianStartDate,
                TechnicianEndDate
            )
            VALUES
            (NULL, NULL, N'', N'', NULL, NULL, NULL, N'', NULL, N'', NULL, NULL);

            SET @index += 1;

        END;

        SELECT RequestSparePartId,
               RepairRequestId,
               SerialNumber,
               SparePartName,
               Quantity,
               TotalCost,
               TechnicianId,
               TechnicianName,
               TechnicianEmployeeId,
               TechnicianSpeciality,
               TechnicianStartDate,
               TechnicianEndDate
        FROM @returnedTable;

    END;
END;
