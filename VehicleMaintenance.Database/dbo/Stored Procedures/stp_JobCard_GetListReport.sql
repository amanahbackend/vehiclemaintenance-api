﻿
-- =============================================
-- Author:		Mohammed Osman
-- Create date: 25/11/2018
-- Description:	
-- EXEC [dbo].[stp_JobCard_GetListReport];
-- EXEC [dbo].[stp_JobCard_GetListReport] @serviceTypeId = 5;
-- =============================================
CREATE PROCEDURE [dbo].[stp_JobCard_GetListReport]
(
    @customerName NVARCHAR(50) = NULL,
    @startDate DATETIME = NULL,
    @endDate DATETIME = NULL,
    @serviceTypeId INT = NULL,
    @jobCardStatusId INT = NULL,
    @vehiclePlateNo NVARCHAR(50) = NULL,
    @vehicleFleetNo NVARCHAR(50) = NULL,
    @jobCardId INT = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @vehicleId INT = NULL;

    IF (@vehiclePlateNo IS NOT NULL)
    BEGIN

        SELECT TOP (1)
               @vehicleId = Vehicle.Id
        FROM dbo.Vehicle
        WHERE PlateNumber = @vehiclePlateNo
        ORDER BY Id DESC;

    END;

    IF (@vehicleFleetNo IS NOT NULL)
    BEGIN
        SELECT TOP (1)
               @vehicleId = Vehicle.Id
        FROM dbo.Vehicle
        WHERE @vehicleFleetNo = @vehicleFleetNo
        ORDER BY Id DESC;
    END;

    SELECT JobCard.Id AS JobCardId,
           JobCard.[Description] AS JobCardDescription,
           JobCard.DateIn AS DateIn,
           JobCard.DateOut AS DateOut,
           Vehicle.Id AS VehicleId,
           Vehicle.CustomerName AS CustomerName,
           Vehicle.PlateNumber AS VehiclePlateNo,
           Vehicle.FleetNumber AS VehicleFleetNo,
           Driver.FirstName + ' ' + Driver.LastName AS DriverFullName,
           JobCardStatus.NameEn AS JobCardStatus
    FROM dbo.JobCard
        INNER JOIN dbo.Vehicle
            ON Vehicle.Id = JobCard.VehicleId
        INNER JOIN dbo.Driver
            ON JobCard.DriverId = Driver.Id
        INNER JOIN dbo.JobCardStatus
            ON JobCardStatus.Id = JobCard.JobCardStatusId
    WHERE (
              JobCard.VehicleId = @vehicleId
              OR @vehicleId IS NULL
          )
          AND
          (
              Vehicle.CustomerName = @customerName
              OR @customerName IS NULL
          )
          AND
          (
              JobCard.DateIn >= @startDate
              OR @startDate IS NULL
          )
          AND
          (
              JobCard.DateOut <= @endDate
              OR @endDate IS NULL
          )
          AND
          (
              @serviceTypeId IS NULL
              OR EXISTS
				(
					SELECT 1
					FROM dbo.RepairRequest
					WHERE RepairRequest.JobCardId = JobCard.Id
						  AND ServiceTypeId = @serviceTypeId
				)
          )
          AND
          (
              JobCard.JobCardStatusId = @jobCardStatusId
              OR @jobCardStatusId IS NULL
          )
          AND
          (
              JobCard.Id = @jobCardId
              OR @jobCardId IS NULL
          );

END;
